<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style>
    input.input-search {
        background: #00a3cc;
        color: #ffffff;
    }

    input.input-search::placeholder {
        color: #ffffff;
        font-weight: 600;
    }

    #searchResultBox {
        position: absolute;
        top: 40px;
        /*background-color: #d5e3f3;*/
    }

    #searchParent {
        position: relative;
    }

    hr {
        margin: 3px 0 3px 0;
        color: white;
    }
</style>
<script>
    $(document).ready(function () {
        var searchResultBox = $('#searchResultBox');
        searchResultBox.css({
            'width': ($("#searchParent").width() + 'px')
        });
        searchResultBox.hide();
    });

    function liveSearch() {
        var query = $('#searchInput').val();
        $.ajax({
            url: "search_live",
            type: "post",
            data: {query: query},
            success: function (result) {
                var searchResultBox = $('#searchResultBox');
                if (query.length > 0 && result.length > 0) {
                    searchResultBox.show();
                    var output = '';
                    for (var i = 0; i < result.length; i++) {
                        var account = result[i];
                        output += '<li><a href="account?id=' + account.id + '" style="display: block; text-decoration: none!important;">';
                        output += account.name + ' ' + account.surname;
                        output += '</a></li>';
                    }
                    var end = result.length === 5 ? '<li class="w3-padding-small w3-center"><a href="search?q=' + query + '" style="display: block; text-decoration: none!important;">Show more...</a></li>' : '';
                    searchResultBox.html(output + end);
                } else if (query.length > 0 && result.length === 0) {
                    searchResultBox.html('<li>Found nothing...</li>');
                } else {
                    searchResultBox.html('');
                    searchResultBox.hide();
                }
            }
        });
    }
</script>
<sec:authentication var="currentAccount" property="principal.account" scope="request"/>
<sec:authentication var="authorities" property="principal.authorities"/>
<nav class="navbar navbar-default navbar-fixed-top navbar-light" style="background-color: #1ad1ff;">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="account?id=${currentAccount.id}"><i class="fa"
                                                                              style="font-size:30px">&#xf015</i></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <c:set var="currentUrl" scope="page" value="${requestScope['javax.servlet.forward.request_uri']}"/>

                <c:choose>
                    <c:when test="${empty lastVisitedGroup}">
                        <c:set var="groupUrl" scope="session" value="groups"/>
                    </c:when>
                    <c:otherwise>
                        <c:set var="groupUrl" scope="session" value="group?id=${lastVisitedGroup}"/>
                    </c:otherwise>
                </c:choose>

                <c:choose>

                    <c:when test="${currentUrl == '/account'}">
                        <li class="active"><a href="account?id=${lastVisitedAccount}"><i style="font-size:24px"
                                                                                         class="fa">&#xf007;</i> <span
                                class="sr-only">(current)</span></a></li>
                        <li><a href="${groupUrl}"><i style="font-size:24px" class="fa">&#xf0c0;</i></a></li>
                        <li><a href="pm"><i style="font-size:24px" class="fa">&#xf0e0;</i></a></li>
                    </c:when>

                    <c:when test="${currentUrl == '/group'}">
                        <li><a href="account?id=${lastVisitedAccount}"><i style="font-size:24px"
                                                                          class="fa">&#xf007;</i></a></li>
                        <li class="active"><a href="${groupUrl}"><i style="font-size:24px" class="fa">&#xf0c0;</i> <span
                                class="sr-only">(current)</span></a></li>
                        <li><a href="pm"><i style="font-size:24px" class="fa">&#xf0e0;</i></a></li>
                    </c:when>

                    <c:when test="${currentUrl == '/pm'}">
                        <li><a href="account?id=${lastVisitedAccount}"><i style="font-size:24px"
                                                                          class="fa">&#xf007;</i></a></li>
                        <li><a href="${groupUrl}"><i style="font-size:24px" class="fa">&#xf0c0;</i></a></li>
                        <li class="active"><a href="pm"><i style="font-size:24px" class="fa">&#xf0e0;</i> <span
                                class="sr-only">(current)</span></a></li>
                    </c:when>

                    <c:otherwise>
                        <li><a href="account?id=${lastVisitedAccount}"><i style="font-size:24px"
                                                                          class="fa">&#xf007;</i></a></li>
                        <li><a href="${groupUrl}"><i style="font-size:24px" class="fa">&#xf0c0;</i></a></li>
                        <li><a href="pm"><i style="font-size:24px" class="fa">&#xf0e0;</i></a></li>
                    </c:otherwise>
                </c:choose>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Navigation <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="account?id=${currentAccount.id}">My page</a></li>
                        <li><a href="groups">My groups</a></li>
                        <li><a href="friend_requests">My friend requests</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="group_create">Create group</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" action="search" method="get">
                <div class="form-group" id="searchParent">
                    <input type="text" onkeyup="liveSearch()" class="form-control input-search" placeholder="Search"
                           name="q" id="searchInput">
                    <ul class="w3-ul w3-border w3-pale-blue w3-hoverable" id="searchResultBox">
                    </ul>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            <ul class="nav navbar-nav navbar-right">

                <c:if test="${fn:contains(authorities, 'ROLE_ADMIN')}">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Admin tools <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="edit_account?id=${param.id}">Edit this account</a></li>

                            <c:if test="${currentUrl == '/account'}">
                                <li><a href="" data-toggle="modal" data-target="#deleteModal">Delete this account</a>
                                </li>
                                <script>
                                    function deleteAccount() {
                                        $.ajax({
                                            url: "delete_account",
                                            type: "post",
                                            data: {id: ${param.id}},
                                            success: function (result) {
                                                if (result === 'OK') {
                                                    alert('Account deleted successfully.');
                                                    window.location = '/account';
                                                } else {
                                                    alert(result);
                                                }
                                            }
                                        });
                                    }
                                </script>
                            </c:if>

                            <li role="separator" class="divider"></li>
                            <li><a href="admin_list">Admin list</a></li>
                        </ul>
                    </li>
                </c:if>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Options <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="edit_account">Edit my account</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>