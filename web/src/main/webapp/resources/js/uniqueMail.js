$.fn.bootstrapValidator.validators.emailUnique = {
    validate: function (validator, $field, options) {
        var value = $field.val();

        function ajaxWrapper() {
            var jjj = $.ajax({
                url: "check_mail",
                type: "POST",
                async: false,
                data: {email: value}
            });
            return jjj.responseText;
        }

        return ajaxWrapper() === 'true';
    }
};
