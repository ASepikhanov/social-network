function avatarSelected(filename) {
    $('#upload-file-info').html(filename);
    var removeAvatarButton = $('#remove-avatar-button');
    removeAvatarButton.prop("disabled", false);
    removeAvatarButton.html('Remove avatar');
    $('#hidden-avatar-status').val('true');

}

function avatarRemoved() {
    var removeAvatarButton = $('#remove-avatar-button');
    removeAvatarButton.html('Avatar removed');
    removeAvatarButton.prop("disabled", true);
    $('#hidden-avatar-status').val('false');
    $('#upload-file-info').html('');
}

function imageSelected(filename) {
    var result = filename + '<a href="javascript:void(0);" onclick="deselectImage();" style="font-size: 14px"> X </a>';
    $('#upload-file-info').html(result);
}

function deselectImage() {
    var file = document.getElementById("my-file-selector");
    file.value = file.defaultValue;
    $('#upload-file-info').html('');
}