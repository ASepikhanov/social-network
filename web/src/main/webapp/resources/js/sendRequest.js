function sendRequest(container_id, to, status, acceptOrReject) {
    $.ajax({
        url: "RelationController",
        type: "post",
        data: {
            id_to: to,
            relationStatus: status,
            accept_or_reject: acceptOrReject
        },
        success: function (result) {
            var arr = result.split(":");
            var fullResult = '<button  onclick="sendRequest( \x27' + container_id + '\x27, ' +
                +arr[1]
                + ', '
                + arr[2]
                + ' )" type="button" class="btn btn-default btn-block"><i class="fa fa-coffee"></i> '
                + arr[0]
                + ' </button>';
            document.getElementById(container_id).innerHTML = fullResult;
        }
    });
}