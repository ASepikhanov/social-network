function groupStatusRequest(container_id, group_id, acc_id, role_code, adm_action) {
    $.ajax({
        url: "GroupRoleController",
        type: "post",
        data: {
            group_id: group_id,
            account_id: acc_id,
            role_code: role_code,
            downgrade_from_moderator: adm_action
        },
        success: function (result) {
            var arr = result.split(":");
            if (result.length > 0) {
                var fullResult = '<button  onclick="groupStatusRequest( \x27' + container_id + '\x27, ' +
                    +arr[1]
                    + ', '
                    + arr[2]
                    + ', '
                    + arr[3];
                if (adm_action === 'a') {
                    fullResult = fullResult + ', \x27a\x27';
                }
                fullResult = fullResult
                    + ' )" type="button" class="btn btn-primary"> '
                    + arr[0]
                    + ' </button>';
                if (adm_action === 'a') {
                    fullResult = '<div class="btn-group btn-block" role="group">'
                        + fullResult +
                        '<button  onclick="groupStatusRequest( \x27' + container_id + '\x27, ' +
                        +arr[1]
                        + ', '
                        + arr[2]
                        + ', '
                        + 4
                        + ' )" type="button" class="btn btn-danger"> '
                        + 'X'
                        + ' </button>'
                        + '</div>';
                }
                document.getElementById(container_id).innerHTML = fullResult;
            } else {
                $('#' + container_id + 'elem').remove();
            }
        }
    });
}