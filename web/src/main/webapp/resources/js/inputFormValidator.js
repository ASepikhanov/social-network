$(document).ready(function () {
    $('#profileForm').bootstrapValidator({
        container: 'tooltip',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },

        trigger: 'blur',

        fields: {
            name: {

                validators: {
                    stringLength: {
                        min: 2
                    },
                    notEmpty: {
                        message: 'Please supply your first name'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Name can consist of alphabetical characters and spaces only'
                    }
                }
            },

            surname: {
                validators: {
                    stringLength: {
                        min: 2
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Name can consist of alphabetical characters and spaces only'
                    }
                }
            },

            middleName: {
                validators: {
                    stringLength: {
                        min: 2
                    },
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'Name can consist of alphabetical characters and spaces only'
                    }
                }
            },

            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    },
                    emailUnique: {
                        message: 'User with this email is already registered'
                    }
                }
            },

            icq: {
                validators: {
                    integer: {
                        message: 'Only numeric input'
                    },
                    stringLength: {
                        min: 6,
                        max: 15,
                        message: 'ICQ can have from 6 to 15 digits'
                    }
                }
            },

            password: {
                validators: {
                    stringLength: {
                        min: 6,
                        message: 'Minimum 6 characters'
                    },
                    identical: {
                        field: 'confirmPassword',
                        message: 'Confirm your password  - type same password'
                    },
                    notEmpty: {
                        message: 'Please enter password'
                    }
                }
            },

            confirmPassword: {
                validators: {
                    stringLength: {
                        min: 6
                    },
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    },
                    notEmpty: {
                        message: 'Please enter password'
                    }
                }
            }

        }
    })

});
