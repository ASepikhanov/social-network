function deleteMessage(containerId, subjectMessage) {
    var feedElement = $('#' + containerId);
    $.ajax({
        url: "delete_message",
        type: "post",
        data: {subject_message: subjectMessage},
        success: function (result) {
            if (result === 'OK') {
                feedElement.remove();
            }
        }
    });

}