<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <title>Group list</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/groupStatusRequest.js"/>"></script>


</head>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Groups
                    of ${accountName}</strong></div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <div class="feed-activity-list">


                            <%--<c:set var="roleCode" value="${groupList[currentAccount.id].code}"/>--%>
                            <c:forEach items="${groups}" var="item">
                                <div class="feed-element">
                                    <a href="group?id=${item.groupId}" class="pull-left">
                                        <img alt="image" class="img-circle"
                                             src="<c:choose>
                                                    <c:when test="${item.hasAvatar()}">
                                                        group_avatar?id=${item.groupId}
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:url value="/resources/img/default-group.jpg"/>
                                                    </c:otherwise>
                                        </c:choose>">
                                    </a>

                                    <div class="media-body ">
                                        <c:if test="${currentAccount.id == param.id}">
                                            <small class="pull-right" id="apply${item.groupId}">
                                                <div class="btn-group btn-block" role="group">

                                                    <c:set var="codeInGroup"
                                                           value="${item.subscriberRoles[currentAccount.id].code}"/>
                                                    <c:choose>
                                                        <c:when test="${codeInGroup == 0 || codeInGroup == 1 || codeInGroup == 2}">
                                                            <button onclick="groupStatusRequest('apply${item.groupId}', ${item.groupId}, ${currentAccount.id}, 4)"
                                                                    type="button" class="btn btn-danger">Leave
                                                            </button>
                                                        </c:when>
                                                        <c:when test="${codeInGroup == 3}">
                                                            <button onclick="groupStatusRequest('apply${item.groupId}', ${item.groupId}, ${currentAccount.id}, 4)"
                                                                    type="button" class="btn btn-primary">Recall apply
                                                            </button>
                                                        </c:when>
                                                    </c:choose>
                                                </div>
                                            </small>
                                        </c:if>
                                        <a href="group?id=${item.groupId}" style="color: #333333">
                                            <strong>${item.name}</strong>
                                        </a>
                                        <br>
                                        <small class="text-muted">
                                            Subscribers: ${fn:length(item.approovedSubscribers)}</small>
                                    </div>
                                </div>
                                <!-- feed-element-->
                            </c:forEach>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
