<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Friend requests</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/sendRequest.js"/>"></script>
</head>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Friend
                    requests</strong></div>
                <div class="panel-body">

                    <div class="ibox-content">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#in_content">In requests</a></li>
                            <li><a data-toggle="pill" href="#out_content">Out requests</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="in_content" class="tab-pane fade in active">
                                <div class="feed-activity-list">
                                    <c:forEach items="${inRequestList}" var="item">
                                        <div class="feed-element">
                                            <a href="account?id=${item.id}" class="pull-left">
                                                <img alt="image" class="img-circle"
                                                     src="  <c:choose>
                                                                <c:when test="${item.hasAvatar()}">
                                                                    avatar?id=${item.id}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:url value="/resources/img/default-avatar.jpg"/>
                                                                </c:otherwise>
                                                            </c:choose>">
                                            </a>

                                            <div class="media-body ">
                                                <small class="pull-right" id="in_form${item.id}">
                                                    <div class="btn-group btn-block" role="group">
                                                        <button onclick="sendRequest('in_form${item.id}',  ${item.id}, 2, 'accept')"
                                                                type="button" class="btn btn-info ">Accept request
                                                        </button>
                                                        <button onclick="sendRequest('in_form${item.id}',  ${item.id}, 2)"
                                                                type="button" class="btn btn-danger">X
                                                        </button>
                                                    </div>
                                                </small>
                                                <a href="account?id=${item.id}">
                                                    <strong>${item.name} ${item.surname}</strong> <br>
                                                </a>
                                                <br>
                                                <a class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i>
                                                    Message</a>
                                            </div>
                                        </div>
                                        <!-- feed-element-->
                                    </c:forEach>
                                </div>
                            </div>
                            <div id="out_content" class="tab-pane fade">
                                <div class="feed-activity-list">
                                    <c:forEach items="${outRequestList}" var="item">
                                        <div class="feed-element">
                                            <a href="account?id=${item.id}" class="pull-left">
                                                <img alt="image" class="img-circle"
                                                     src="  <c:choose>
                                                                <c:when test="${item.hasAvatar()}">
                                                                    avatar?id=${item.id}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:url value="/resources/img/default-avatar.jpg"/>
                                                                </c:otherwise>
                                                            </c:choose>">
                                            </a>

                                            <div class="media-body ">
                                                <small class="pull-right" id="out_form${item.id}">
                                                    <button onclick="sendRequest('out_form${item.id}',  ${item.id}, 1)"
                                                            type="button" class="btn btn-default btn-block">
                                                        <i class="fa fa-coffee"></i> Recall request
                                                    </button>
                                                </small>
                                                <a href="account?id=${item.id}">
                                                    <strong>${item.name} ${item.surname}</strong> <br>
                                                </a>
                                                <br>
                                                <a href="pm?id=${item.id}" class="btn btn-xs btn-primary"><i
                                                        class="fa fa-pencil"></i> Message</a>
                                            </div>
                                        </div>
                                        <!-- feed-element-->
                                    </c:forEach>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>
</body>
</html>
