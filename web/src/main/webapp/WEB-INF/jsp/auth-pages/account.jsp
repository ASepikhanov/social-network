<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
    <title>Account page</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/sendRequest.js"/>"></script>
    <script src="<c:url value="/resources/js/imageButtonLogic.js"/>"></script>
    <script src="<c:url value="/resources/js/deleteMessage.js"/>"></script>

</head>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong
                        style="color:white;">Details</strong></div>
                <div class="panel-body no-padding">
                    <div class="ibox-content no-padding border-left-right">
                        <img alt="image" class="img-responsive"
                             src="  <c:choose>
                                        <c:when test="${account.hasAvatar()}">
                                            avatar?id=${account.id}
                                        </c:when>
                                        <c:otherwise>
                                            <c:url value="/resources/img/default-avatar.jpg"/>
                                        </c:otherwise>
                                    </c:choose>"
                             width="180" height="180">
                    </div>
                    <div class="ibox-content profile-content">
                        <h3><strong>${account.name} ${account.surname}</strong></h3>
                        <c:if test="${account.country.name != null || account.city != null}">
                            <p><i style="font-size:16px" class="fa">&#xf041;</i>
                                    ${account.country.name}${account.country.name != null && account.city != null ? ", " : ""}
                                    ${account.city}
                            </p>
                        </c:if>

                        <c:if test="${account.birthDate != null}">
                            <p><i style="font-size:16px" class="fa">&#xf1fd;</i>
                                    ${account.birthDate}
                            </p>
                        </c:if>

                        <c:if test="${account.skype != null}">
                            <p><i class="fa fa-skype" style="font-size:16px"></i>
                                    ${account.skype}
                            </p>
                        </c:if>

                        <c:if test="${fn:length(account.phones) > 0}">
                            <button class="btn btn-link" style="color: #333333; padding: 0; outline: none !important;"
                                    data-toggle="modal" data-target="#phoneModal"><i
                                    class="glyphicon glyphicon-earphone"></i>
                                Phones
                            </button>
                            <div class="modal fade" id="phoneModal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Phones of ${account.name}</h4>
                                        </div>
                                        <div class="modal-body">

                                            <script>
                                                $(document).ready(function () {
                                                    $('[data-toggle="tooltip"]').tooltip();
                                                });
                                            </script>
                                            <c:forEach items="${account.phones}" var="item">
                                                <c:choose>
                                                    <c:when test="${item.type.code == 0}">
                                                        <i data-toggle="tooltip" data-placement="top"
                                                           title="Personal phone"
                                                           class="glyphicon glyphicon-phone"></i>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <i data-toggle="tooltip" data-placement="top"
                                                           title="Working phone"
                                                           class="glyphicon glyphicon-phone-alt"></i>
                                                    </c:otherwise>
                                                </c:choose>
                                                <span> ${item.phoneNum}</span>
                                                <hr>
                                            </c:forEach>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </c:if>

                        <c:if test="${account.additionalInfo != null}">
                            <h5><strong>About me</strong></h5>
                            <p>${account.additionalInfo}.</p>
                        </c:if>


                        <div class="row">
                            <div class="stats">
                                <div class="col-sm-4">
                                    <a href="friends?id=${account.id}" style="color: #333333">
                                        <div class="statis">
                                            <span class="glyphicon glyphicon-user"></span>
                                            <h5><strong>${fn:length(account.friends)}</strong><br>Friends</h5>
                                        </div><!-- /statis -->
                                    </a>
                                </div>

                                <div class="col-sm-4">
                                    <a href="groups?id=${account.id}" style="color: #333333">
                                        <div class="statis">
                                            <span class="glyphicon glyphicon-th-list"></span>
                                            <h5><strong>${fn:length(account.groups)}</strong><br>Groups</h5>
                                        </div> <!-- /statis -->
                                    </a>
                                </div>

                                <div class="col-sm-4">
                                    <a href="#" style="color: #333333">
                                        <div class="statis">
                                            <i style="font-size:16px" class="fa">&#xf292;</i>
                                            <h5><strong>${posts}</strong><br>Posts</h5>
                                        </div> <!-- /statis -->
                                    </a>
                                </div>
                            </div>
                        </div><!-- /row m-t-lg -->

                        <div class="user-button">
                            <c:if test="${currentAccount.id != account.id}">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="pm?id=${account.id}" class="btn btn-primary btn-block">
                                            <i class="fa fa-envelope"></i> Send Message
                                        </a>
                                    </div>


                                    <c:choose>

                                        <c:when test="${relationStatus == 0}">
                                            <div class="col-md-6" id="button_container">
                                                <button onclick="sendRequest('button_container', ${param.id}, ${requestScope.relationStatus})"
                                                        type="button" class="btn btn-default btn-block">
                                                    <i class="fa fa-coffee"></i> Add to friends
                                                </button>
                                            </div>
                                        </c:when>

                                        <c:when test="${relationStatus == 1}">
                                            <div class="col-md-6" id="button_container">
                                                <button onclick="sendRequest('button_container', ${param.id}, ${requestScope.relationStatus})"
                                                        type="button" class="btn btn-default btn-block">
                                                    <i class="fa fa-coffee"></i> Recall request
                                                </button>
                                            </div>
                                        </c:when>

                                        <c:when test="${relationStatus == 2}">
                                            <div class="col-md-6" id="button_container">
                                                <div class="btn-group btn-block" role="group">
                                                    <button onclick="sendRequest('button_container', ${param.id}, ${requestScope.relationStatus}, 'accept')"
                                                            type="button" class="btn btn-info ">Accept request
                                                    </button>
                                                    <button onclick="sendRequest('button_container', ${param.id}, ${requestScope.relationStatus})"
                                                            type="button" class="btn btn-danger">X
                                                    </button>
                                                </div>
                                            </div>
                                        </c:when>

                                        <c:otherwise>
                                            <div class="col-md-6" id="button_container">
                                                <button onclick="sendRequest('button_container', ${param.id}, ${requestScope.relationStatus})"
                                                        type="button" class="btn btn-default btn-block">
                                                    <i class="fa fa-coffee"></i> Delete from friends
                                                </button>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:if>
                        </div>
                        <br>
                        <a href="get_xml?id=${account.id}" style="text-decoration: none"
                           class="btn btn-block btn-primary small">
                            <i style="font-size:20px" class="fa">&#xf0ab;</i>
                            Export to xml</a>
                    </div><!-- /profile-content -->
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong
                        style="color:white;"> ${account.name} wall</strong></div>
                <div class="panel-body">

                    <div class="ibox-content">

                        <div class="feed-activity-list">
                            <div class="feed-element">
                                <img alt="image" class="img-circle pull-left"
                                     src="  <c:choose>
                                                <c:when test="${currentAccount.hasAvatar()}">
                                                    avatar?id=${currentAccount.id}
                                                </c:when>
                                                <c:otherwise>
                                                    <c:url value="/resources/img/default-avatar.jpg"/>
                                                </c:otherwise>
                                            </c:choose>">
                                <div class="media-body ">
                                    <strong>Write message</strong>
                                    <br>
                                    <form:form modelAttribute="newWallMessage" enctype="multipart/form-data"
                                               id="messageForm" role="form"
                                               action="send_message_wall" method="post">
                                        <form:input type="hidden" path="destinationId"/>
                                        <form:input type="hidden" path="destinationEntity"/>
                                        <form:textarea path="text" class="form-control" rows="2"/>
                                        <br>
                                        <label class="btn btn-primary" for="my-file-selector">
                                            <input name="attachedImage" id="my-file-selector" type="file" accept=".jpg"
                                                   style="display:none"
                                                   onchange="imageSelected(this.files[0].name)">
                                            Attach image
                                        </label>
                                        <span class='label label-info' id="upload-file-info"
                                              style="font-size: 14px"></span>
                                        <div class="pull-right">
                                            <button class="btn btn-info" type="submit">Send</button>
                                        </div>
                                    </form:form>
                                </div>
                            </div><!-- feed-element-->

                            <c:forEach items="${accountWall}" var="wallItem">
                                <div class="feed-element feed-element-static" id="feed-element-${wallItem.id}">
                                    <a href="account?id=${wallItem.author.id}" class="pull-left">
                                        <img alt="image" class="img-circle"
                                             src="  <c:choose>
                                                        <c:when test="${wallItem.author.hasAvatar()}">
                                                            avatar?id=${wallItem.author.id}
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:url value="/resources/img/default-avatar.jpg"/>
                                                        </c:otherwise>
                                                    </c:choose>">
                                    </a>

                                    <div class="media-body ">
                                        <c:if test="${currentAccount.id == param.id || currentAccount.id == wallItem.author.id }">
                                            <small class="pull-right" id="remove-button-glyph" style="display: none">
                                                <button class="btn btn-xs btn-link glyphicon glyphicon-remove"
                                                        onclick="deleteMessage('feed-element-${wallItem.id}', ${wallItem.id})"></button>
                                            </small>
                                        </c:if>
                                        <a href="account?id=${wallItem.author.id}"><strong>${wallItem.author.name} ${wallItem.author.surname}</strong></a>
                                        <br>
                                        <small class="text-muted">${wallItem.writeTime}</small>
                                        <br>
                                        <c:if test="${!(empty wallItem.text)}">
                                            <div class="well">
                                                <c:out value="${wallItem.text}"/>
                                            </div>
                                        </c:if>
                                        <c:if test="${wallItem.image == 'true'}">
                                            <div class="photos">
                                                <a target="_blank" href="message_image?id=${wallItem.id}">
                                                    <img alt="image" class="feed-photo"
                                                         src="message_image?id=${wallItem.id}"></a>
                                            </div>
                                        </c:if>
                                        <div class="pull-right">
                                            <a href="pm?id=${wallItem.author.id}" class="btn btn-xs btn-primary"><i
                                                    class="fa fa-pencil"></i> Message</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- feed-element-->
                            </c:forEach>

                            <script>
                                $(".feed-element-static").hover(
                                    function () {
                                        $(this).find('#remove-button-glyph').fadeIn();
                                    }, function () {
                                        $(this).find('#remove-button-glyph').fadeOut();
                                    }
                                );
                            </script>

                        </div><!--feed-activity-list-->
                    </div><!-- feed-activity-list -->
                </div><!--ibox-content -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Confirm delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure want to delete this account?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" onclick="deleteAccount()" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
