<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit account</title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <jsp:include page="/common/meta.jsp"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-select.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-formhelpers.min.css"/>">
    <script src="<c:url value="/resources/js/bootstrap-select.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrapvalidator.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-formhelpers.min.js"/>"></script>

    <script src="<c:url value="/resources/js/countrypicker.js"/>"></script>

    <script src="<c:url value="/resources/js/inputFormValidator.js"/>"></script>
    <script src="<c:url value="/resources/js/futureDateRestrict.js"/>"></script>
    <script src="<c:url value="/resources/js/imageButtonLogic.js"/>"></script>

    <script>
        $(document).ready(function () {
            $('.bfh-selectbox').on('change.bfhselectbox', function () {
                var country = $(this).val();
                $("#hiddenField").val(country);
            });

        });

        function isPhoneCorrect(phoneRecord) {
            var record = $('#' + phoneRecord);
            var textField = record.find(".form-control");
            if (textField.val().match("^[0-9]*$") && textField.val().length > 0 && textField.val().length < 12) {
                record.find(".btn-commit").prop("disabled", false);
            } else {
                record.find(".btn-commit").prop("disabled", true);
            }
        }

        function removePhone(phoneRecord) {
            $('#' + phoneRecord).remove();
        }

        function editPhone(phoneRecord) {
            var record = $('#' + phoneRecord);
            record.find(".form-control").prop("readonly", false);
            record.find(".dropdown-toggle").prop("disabled", false);
            record.find(".btn-edit").remove();
            record.find(".right-group").prepend("<button class=\x22btn btn-default btn-undo\x22 type=\x22button\x22 onclick=\x22undo(\x27" + phoneRecord + " \x27)\x22>Undo</button>");
            record.find(".right-group").prepend("<button class=\x22btn btn-default btn-commit\x22 type=\x22button\x22 onclick=\x22commitChange(\x27" + phoneRecord + "\x27)\x22>Save</button>");
        }

        function commitChange(phoneRecord) {
            var record = $('#' + phoneRecord);
            record.find(".form-control").prop("readonly", true);
            record.find(".dropdown-toggle").prop("disabled", true);
            record.find(".btn-commit").remove();
            record.find(".btn-undo").remove();
            record.find(".right-group").prepend("<button class=\x22btn btn-default btn-edit\x22 type=\x22button\x22 onclick=\x22editPhone(\x27" + phoneRecord + "\x27)\x22>Edit</button>");
        }

        function undo(phoneRecord) {
            var record = $('#' + phoneRecord);
            record.find(".form-control").val(phoneRecord.substring(5, phoneRecord.length).trim());
            record.find(".form-control").prop("readonly", true);
            var defaultType = record.attr("data-phonetype");
            if (defaultType === '0') {
                record.find(".dropdown-toggle").val("0");
            } else if (defaultType === '1') {
                record.find(".dropdown-toggle").val("1");
            } else {
                alert("something go wrong")
            }
            record.find(".dropdown-toggle").prop("disabled", true);
            record.find(".btn-commit").remove();
            record.find(".btn-undo").remove();
            record.find(".right-group").prepend("<button class=\x22btn btn-default btn-edit\x22 type=\x22button\x22 onclick=\x22editPhone(\x27" + phoneRecord + "\x27)\x22>Edit</button>");
        }

        function createRecord(newRecord) {
            var record = $('#' + newRecord);
            var inputPhone = record.find(".form-control").val().trim();
            var phonesList = $("#form-phones");
            var selected = record.find(".dropdown-toggle").find(":selected").val();
            var selector;
            if (selected === '1') {
                selector = "                                    <div class=\x22input-group-btn\x22>\n" +
                    "                                        <select class=\x22btn btn-default dropdown-toggle\x22 name=\"type" + inputPhone + "\" style=\x22height: 34px\x22 disabled>\n" +
                    "                                            <option selected=\x22selected\x22 value=\"1\">&#xe183;</option>\n" +
                    "                                            <option value=\"0\x22>&#xe145;</option>\n" +
                    "                                        </select>\n" +
                    "                                    </div>";
            } else {
                selector = "                                    <div class=\x22input-group-btn\x22>\n" +
                    "                                        <select class=\x22btn btn-default dropdown-toggle\x22 name=\"type" + inputPhone + "\"style=\x22height: 34px\x22 disabled>\n" +
                    "                                            <option value=\"1\">&#xe183;</option>\n" +
                    "                                            <option selected=\x22selected\x22 value=\"0\">&#xe145;</option>\n" +
                    "                                        </select>\n" +
                    "                                    </div>";
            }

            phonesList.append("                                    <div class=\x22input-group\x22 id=\x22phone" + inputPhone + "\x22>" + selector +
                "                                        <input type=\x22text\x22 class=\x22form-control\x22 onkeyup=\x22isPhoneCorrect(\x27phone" + inputPhone + "\x27)\x22 readonly value=\x22" + inputPhone + "\x22 name=\"phone" + inputPhone + "\">\n" +
                "                                        <span class=\x22input-group-btn right-group\x22>\n" +
                "                                            <button class=\x22btn btn-default btn-edit\x22 type=\x22button\x22 onclick=\x22editPhone(\x27phone" + inputPhone + "\x27)\x22>Edit</button>\n" +
                "                                            <button class=\x22btn btn-danger\x22 type=\x22button\x22 onclick=\x22removePhone(\x27phone" + inputPhone + "\x27)\x22>X</button>\n" +
                "                                        </span>\n" +
                "                                    </div>");
        }

        function unblockAll() {
            var allDropDowns = $(".dropdown-toggle");
            allDropDowns.prop("disabled", false);
        }
    </script>
    <style>
        [hidden] {
            display: none !important;
        }

        select {
            font-family: "Glyphicons Halflings";
        }
    </style>
</head>
<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-3">
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Edit
                    account</strong>
                    <small class="pull-right">
                        <a href="get_xml?id=${updatingAccount.id}" style="color:white; font-size: 16px;">
                            Download xml <i style="font-size:24px" class="fa">&#xf0ab;</i>
                        </a>
                        |
                        <form method="post" action="process_xml" style="display: none" enctype="multipart/form-data">
                            <input id="uploadXml" name="xmlFile" style="display: none" type="file"/>
                            <button id="sendXmlButton" style="display: none" type="submit"></button>
                        </form>

                        <a href="" id="upload_xml" style="color:white; font-size: 16px;">
                            Upload xml <i style="font-size:24px" class="fa">&#xf0aa;</i>
                        </a>
                        <script>
                            $(function () {
                                $("#upload_xml").on('click', function (e) {
                                    e.preventDefault();
                                    $("#uploadXml:hidden").trigger('click');
                                });
                            });
                            $("#uploadXml").change(function () {
                                $('#sendXmlButton:hidden').trigger('click');
                            });
                        </script>
                    </small>
                </div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <form:form modelAttribute="updatingAccount" id="profileForm" enctype="multipart/form-data"
                                   role="form" action="edit_account"
                                   method="post">
                            <input type="hidden" value="${updatingAccount.country.alpha2}" id="hiddenField"
                                   name="country"/>

                            <form:input path="id" type="hidden"/>
                            <div class="form-group">
                                <label>First name</label>
                                <form:input path="name" class="form-control" type="text"/>
                            </div>
                            <div class="form-group">
                                <label>Last name</label>
                                <form:input path="surname" class="form-control" type="text"/>
                            </div>
                            <div class="form-group">
                                <label for="middleName">Middle name</label>
                                <form:input path="middleName" class="form-control" type="text"/>
                            </div>
                            <div class="form-group">
                                <label>Birth date</label>
                                <input name="birthDateString" value="${updatingAccount.birthDate}" class="form-control"
                                       type="date" id="datepicker"/>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <form:input path="email" class="form-control" type="text"/>
                            </div>
                            <div class="form-group">
                                <label>ICQ</label>
                                <form:input path="icq" class="form-control" type="text"/>
                            </div>
                            <div class="form-group">
                                <label>Skype</label>
                                <form:input path="skype" class="form-control" type="text"/>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <div class="form-group">
                                    <div class="bfh-selectbox bfh-countries"
                                         data-country="${updatingAccount.country.alpha2}" data-flags="true"
                                         data-filter="true">
                                        <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox"
                                           href="#">
                                            <span class="bfh-selectbox-option input-medium" data-option=""></span>
                                            <b class="caret"></b>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <form:input path="city" class="form-control" type="text"/>
                            </div>
                            <div class="form-group">
                                <label>About me</label>
                                <form:textarea path="additionalInfo" class="form-control" rows="3"/>
                            </div>
                            <label>Phones</label>
                            <div class="form-group" id="form-phones">
                                <c:forEach items="${updatingAccount.phones}" var="item">
                                    <div class="input-group" id="phone${item.phoneNum}"
                                         data-phonetype="${item.type.code}">
                                        <div class="input-group-btn">
                                            <select class="btn btn-default dropdown-toggle" name="type${item.phoneNum}"
                                                    style="height: 34px" disabled>
                                                <c:choose>
                                                    <c:when test="${item.type.code == 0}">
                                                        <option value="1">&#xe183;</option>
                                                        <option selected="selected" value="0">&#xe145;</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option selected="selected" value="1">&#xe183;</option>
                                                        <option value="0">&#xe145;</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </select>
                                        </div>
                                        <input type="text" class="form-control"
                                               onkeyup="isPhoneCorrect('phone${item.phoneNum}')" readonly
                                               value="${item.phoneNum}" name="phone${item.phoneNum}">
                                        <span class="input-group-btn right-group">
                                            <button class="btn btn-default btn-edit" type="button"
                                                    onclick="editPhone('phone${item.phoneNum}')">Edit</button>
                                            <button class="btn btn-danger" type="button"
                                                    onclick="removePhone('phone${item.phoneNum}')">X</button>
                                        </span>
                                    </div>
                                </c:forEach>

                            </div>
                            <div class="form-group">
                                <div class="input-group" id="phone-new">
                                    <div class="input-group-btn">
                                        <select class="btn btn-default dropdown-toggle" style="height: 34px">
                                            <option value="1">&#xe183;</option>
                                            <option selected="selected" value="0">&#xe145;</option>
                                        </select>
                                    </div>
                                    <input type="text" class="form-control" onkeyup="isPhoneCorrect('phone-new')">
                                    <span class="input-group-btn">
                                            <button class="btn btn-default btn-commit" disabled type="button"
                                                    onclick="createRecord('phone-new')">Save and add</button>
                                    </span>
                                </div>
                            </div>
                            <label> Avatar </label>
                            <div class="form-group">
                                <label class="btn btn-primary" for="my-file-selector">
                                    <input name="avatarFile" id="my-file-selector" type="file" accept=".jpg"
                                           style="display:none"
                                           onchange="avatarSelected(this.files[0].name)">
                                    Update avatar
                                </label>
                                <span class='label label-info' id="upload-file-info"></span>
                                <c:choose>
                                    <c:when test="${updatingAccount.avatar == 'true'}">
                                        <button class="btn btn-danger pull-right" id="remove-avatar-button"
                                                type="button" onclick="avatarRemoved()">Remove avatar
                                        </button>
                                    </c:when>
                                    <c:otherwise>
                                        <button class="btn btn-danger pull-right" disabled id="remove-avatar-button"
                                                type="button" onclick="avatarRemoved()">You don't have avatar
                                        </button>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <input type="hidden" value="${updatingAccount.avatar}" id="hidden-avatar-status"
                                   name="avatar">
                            <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                    data-target="#confirmModal">
                                Send
                            </button>

                            <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog"
                                 aria-labelledby="confirmModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Confirmation</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            Are you sure want to edit account?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                            </button>
                                            <button type="submit" class="btn btn-info" onclick="unblockAll()">Confirm
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </form:form>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
        </div>
    </div>
</div>
</body>
</html>
