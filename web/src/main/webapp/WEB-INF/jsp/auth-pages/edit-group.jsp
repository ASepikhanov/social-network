<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit group</title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/bootstrapvalidator.min.js"/>"></script>


    <script src="<c:url value="/resources/js/inputFormValidator.js"/>"></script>
    <script src="<c:url value="/resources/js/imageButtonLogic.js"/>"></script>

    <style>
        [hidden] {
            display: none !important;
        }
    </style>
</head>
<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-3">
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Edit
                    group</strong></div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <form:form id="groupForm" modelAttribute="updatingGroup" enctype="multipart/form-data"
                                   role="form"
                                   action="edit_group" method="post">
                            <form:input type="hidden" path="groupId" name="groupId"/>
                            <div class="form-group">
                                <form:label path="name">Group name</form:label>
                                <form:input path="name" class="form-control"/>
                            </div>


                            <div class="form-group">
                                <form:label path="description">Group description</form:label>
                                <form:textarea path="description" class="form-control" rows="3"/>
                            </div>

                            <label> Group avatar </label>
                            <div class="form-group">
                                <label class="btn btn-primary" for="my-file-selector">
                                    <input name="avatarFile" id="my-file-selector" type="file" accept=".jpg"
                                           style="display:none"
                                           onchange="avatarSelected(this.files[0].name)">
                                    Update avatar
                                </label>
                                <span class='label label-info' id="upload-file-info"></span>
                                <c:choose>
                                    <c:when test="${updatingGroup.avatar == 'true'}">
                                        <button class="btn btn-danger pull-right" id="remove-avatar-button"
                                                type="button" onclick="avatarRemoved()">Remove avatar
                                        </button>
                                    </c:when>
                                    <c:otherwise>
                                        <button class="btn btn-danger pull-right" disabled id="remove-avatar-button"
                                                type="button" onclick="avatarRemoved()">Group don't have avatar
                                        </button>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                            <form:input path="avatar" type="hidden" id="hidden-avatar-status"/>
                            <input type="submit" class="btn btn-info btn-block" value="Send"/>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
        </div>
    </div>
</div>
</body>
</html>
