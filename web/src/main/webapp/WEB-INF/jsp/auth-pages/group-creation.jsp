<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <title>Create group</title>
    <jsp:include page="/common/meta.jsp"/>

    <script src="<c:url value="/resources/js/bootstrapvalidator.min.js"/>"></script>


    <script>
        $(document).ready(function () {
            $('#groupForm').bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                trigger: 'blur',
                fields: {
                    name: {

                        validators: {
                            notEmpty: {
                                message: 'Please enter group name'
                            }
                        }
                    }

                }
            })
        });

    </script>
</head>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-3">
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Creating new
                    group</strong></div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <form:form modelAttribute="groupToCreate" enctype="multipart/form-data" id="groupForm"
                                   role="form" action="group_create"
                                   method="post">
                            <div class="form-group">
                                <form:label path="name">Group name</form:label>
                                <form:input path="name" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <form:label path="description">Group description</form:label>
                                <form:textarea path="description" class="form-control" rows="3"/>
                            </div>
                            <div class="form-group">
                                <label>Group avatar</label>
                                <div class="form-group">
                                    <input name="groupAvatar" type="file" accept=".jpg">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-info btn-block">Create group</button>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
        </div>
    </div>
</div>

</body>
</html>
