<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <title>Group page</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/groupStatusRequest.js"/>"></script>
    <script src="<c:url value="/resources/js/imageButtonLogic.js"/>"></script>
    <script src="<c:url value="/resources/js/deleteMessage.js"/>"></script>

</head>
<body>

<jsp:include page="/common/header.jsp"/>


<div class="content">
    <c:set var="viewerRole" value="${group.subscriberRoles[currentAccount.id].code}"/>
    <div id="pad-wrapper">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Wall
                    of ${group.name}</strong></div>
                <div class="panel-body">

                    <div class="ibox-content">

                        <div class="feed-activity-list">

                            <c:if test="${viewerRole == 0 || viewerRole == 1 || viewerRole == 2}">
                                <div class="feed-element">
                                    <img alt="image" class="img-circle pull-left"
                                         src="avatar?id=${currentAccount.id}">
                                    <div class="media-body ">
                                        <strong>Write message</strong>
                                        <br>
                                        <form:form modelAttribute="newWallMessage" enctype="multipart/form-data"
                                                   id="messageForm" role="form"
                                                   action="send_message_group" method="post">
                                            <form:input path="destinationId" type="hidden"/>
                                            <form:input type="hidden" path="destinationEntity"/>
                                            <form:textarea path="text" class="form-control" rows="2"/>
                                            <br>
                                            <label class="btn btn-primary" for="my-file-selector">
                                                <input name="attachedImage" id="my-file-selector" type="file"
                                                       accept=".jpg"
                                                       style="display:none"
                                                       onchange="imageSelected(this.files[0].name)">
                                                Attach image
                                            </label>
                                            <span class='label label-info' id="upload-file-info"
                                                  style="font-size: 14px"></span>
                                            <div class="pull-right">
                                                <button class="btn btn-info" type="submit">Send</button>
                                            </div>
                                        </form:form>
                                    </div>
                                </div>
                            </c:if>


                            <c:forEach items="${groupWall}" var="wallItem">
                                <div class="feed-element feed-element-static" id="feed-element-${wallItem.id}">
                                    <a href="account?id=${wallItem.author.id}" class="pull-left">
                                        <img alt="image" class="img-circle" src="avatar?id=${wallItem.author.id}">
                                    </a>

                                    <div class="media-body ">

                                        <c:if test="${viewerRole == 1 || viewerRole == 2 || currentAccount.id == wallItem.author.id }">
                                            <small class="pull-right" id="remove-button-glyph" style="display: none">
                                                <button class="btn btn-xs btn-link glyphicon glyphicon-remove"
                                                        onclick="deleteMessage('feed-element-${wallItem.id}', ${wallItem.id})"></button>
                                            </small>
                                        </c:if>
                                        <a href="account?id=${wallItem.author.id}"><strong>${wallItem.author.name} ${wallItem.author.surname}</strong></a>
                                        <br>
                                        <small class="text-muted">${wallItem.writeTime}</small>
                                        <br>
                                        <c:if test="${!(empty wallItem.text)}">
                                            <div class="well">
                                                <c:out value="${wallItem.text}"/>
                                            </div>
                                        </c:if>
                                        <c:if test="${wallItem.image == 'true'}">
                                            <div class="photos">
                                                <a target="_blank" href="message_image?id=${wallItem.id}">
                                                    <img alt="image" class="feed-photo"
                                                         src="message_image?id=${wallItem.id}"></a>
                                            </div>
                                        </c:if>
                                        <div class="pull-right">
                                            <a href="pm?id=${wallItem.author.id}" class="btn btn-xs btn-primary"><i
                                                    class="fa fa-pencil"></i> Message</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- feed-element-->
                            </c:forEach>

                            <script>
                                $(".feed-element-static").hover(
                                    function () {
                                        $(this).find('#remove-button-glyph').fadeIn();
                                    }, function () {
                                        $(this).find('#remove-button-glyph').fadeOut();
                                    }
                                );
                            </script>
                        </div><!--feed-activity-list-->

                    </div><!-- feed-activity-list -->
                </div><!--ibox-content -->
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong
                        style="color:white;">Details</strong>
                    <c:if test="${viewerRole == 1 || viewerRole == 2}">
                        <small class="pull-right">
                            <a href="edit_group?id=${group.groupId}" style="color:white;">
                                <i style="font-size:24px" class="fa">&#xf013;</i>
                            </a>
                        </small>
                    </c:if>
                </div>
                <div class="panel-body no-padding">
                    <div class="ibox-content profile-content">
                        <div class="ibox-content no-padding border-left-right center-block">
                            <img alt="image" class="img-responsive"
                                 src="<c:choose>
                                            <c:when test="${group.hasAvatar()}">
                                                group_avatar?id=${group.groupId}
                                            </c:when>
                                            <c:otherwise>
                                                <c:url value="/resources/img/default-group.jpg"/>
                                            </c:otherwise>
                                        </c:choose>"
                                 width="180"
                                 height="180">
                        </div>
                        <h3><strong>${group.name}</strong></h3>
                        <small class="text-muted">Creation date: ${group.creationDate}</small>
                        <br>
                        <c:if test="${group.description != null}">
                            <h5><strong>Description</strong></h5>
                            <p>${group.description}.</p>
                        </c:if>
                        <c:set var="roleCode" value="${subscriberMap[currentAccount.id].code}"/>
                        <div class="row">
                            <div class="stats">
                                <div class="col-sm-6">
                                    <a href="subscribers?id=${group.groupId}" style="color: #333333">
                                        <div class="statis">
                                            <i style="font-size:16px" class="fa">&#xf0c0;</i>
                                            <h5><strong>${subscriberCount}</strong><br>Subscribers</h5>
                                        </div><!-- /statis -->
                                    </a>
                                </div>

                                <div class="col-sm-6">
                                    <a href="#" style="color: #333333">
                                        <div class="statis">
                                            <i style="font-size:16px" class="fa">&#xf292;</i>
                                            <h5><strong>${groupWall.size()}</strong><br>Posts</h5>
                                        </div> <!-- /statis -->
                                    </a>
                                </div>
                                <c:if test="${roleCode == 1 || roleCode == 2}">
                                    <div class="col-sm-12">
                                        <a href="group_mod?id=${group.groupId}" style="color: #333333">
                                            <div class="statis">
                                                <i style="font-size:16px" class="fa">&#xf013;</i>
                                                <h5>Moderation</h5>
                                            </div><!-- /statis -->
                                        </a>
                                    </div>
                                </c:if>

                                <div class="user-button">
                                    <c:choose>
                                        <c:when test="${roleCode == 0 || roleCode == 1 || roleCode == 2}">
                                            <div class="row" id="groupActionButton">
                                                <button type="button" class="btn btn-primary btn-block"
                                                        onclick="groupStatusRequest('groupActionButton', ${group.groupId}, ${currentAccount.id}, 4)">
                                                    Leave group
                                                </button>
                                            </div>
                                        </c:when>
                                        <c:when test="${subscriberMap[currentAccount.id].code == 3}">
                                            <div class="row" id="groupActionButton">
                                                <button type="button" class="btn btn-primary btn-block"
                                                        onclick="groupStatusRequest('groupActionButton', ${group.groupId}, ${currentAccount.id}, 4)">
                                                    Recall apply
                                                </button>
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="row" id="groupActionButton">
                                                <button type="button" class="btn btn-primary btn-block"
                                                        onclick="groupStatusRequest('groupActionButton', ${group.groupId}, ${currentAccount.id}, 3)">
                                                    Apply
                                                </button>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>

                                </div>

                            </div>
                        </div> <!-- /row m-t-lg -->

                    </div><!-- /profile-content -->
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>
