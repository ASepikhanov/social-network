<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Group pending request list</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/groupStatusRequest.js"/>"></script>

</head>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Requests to
                    group ${group.name}</strong>
                </div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <div class="feed-activity-list">

                            <c:forEach items="${group.pendingRequests}" var="item">
                                <div class="feed-element" id="apply${item.id}elem">
                                    <a href="account?id=${item.id}" class="pull-left">
                                        <img alt="image" class="img-circle"
                                             src="<c:choose>
                                                        <c:when test="${item.hasAvatar()}">
                                                            avatar?id=${item.id}
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:url value="/resources/img/default-avatar.jpg"/>
                                                        </c:otherwise>
                                                    </c:choose>">
                                    </a>

                                    <div class="media-body ">
                                        <small class="pull-right" id="apply${item.id}">
                                            <div class="btn-group btn-block" role="group">
                                                <button onclick="groupStatusRequest('apply${item.id}', ${group.groupId},  ${item.id}, 0)"
                                                        type="button" class="btn btn-info ">Accept apply
                                                </button>
                                                <button onclick="groupStatusRequest('apply${item.id}', ${group.groupId}, ${item.id}, 4)"
                                                        type="button" class="btn btn-danger">X
                                                </button>
                                            </div>
                                        </small>
                                        <a href="account?id=${item.id}">
                                            <strong>${item.name} ${item.surname}</strong> <br>
                                        </a>
                                        <br>
                                        <a class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> Message</a>
                                    </div>
                                </div>
                                <!-- feed-element-->
                            </c:forEach>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
