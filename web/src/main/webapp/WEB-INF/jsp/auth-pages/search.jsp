<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <title>Search results</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/sendRequest.js"/>"></script>
    <style>
        .feed-element:hover {
            background-color: lightcyan;
        }

        .feed-element {
            padding-right: 35px;
            display: block;
            text-decoration: none !important;
        }
    </style>
    <script type="text/javascript">
        var urlParams = new URLSearchParams(window.location.search);
        var accPagePointer = 0;
        var accListSize;
        var groupPagePointer = 0;
        var groupListSize;
        $(function () {
            refreshSearchAccs(urlParams.get('page'), function () {
            });
            refreshSearchGroups(urlParams.get('page'), function () {
            });
            $('#prevPageAccButton').hide();
            $('#prevPageGroupButton').hide();


            $('#groupPager').hide();
        });

        function refreshSearchAccs(page, callback) {
            $.ajax({
                type: "GET",
                url: "search_accounts",
                data: {
                    q: urlParams.get('q'),
                    page: page
                },
                success: function (response) {
                    $("#acc_content").html(response);
                    accListSize = $('#accListSize').data("value");
                    if (accListSize <= 5) {
                        $('#nextPageAccButton').hide();
                    }
                    callback();
                }
            });
        }

        function refreshSearchGroups(page, callback) {
            $.ajax({
                type: "GET",
                url: "search_groups",
                data: {
                    q: urlParams.get('q'),
                    page: page
                },
                success: function (response) {
                    $("#group_content").html(response);
                    groupListSize = $('#groupListSize').data("value");
                    if (groupListSize <= 5) {
                        $('#nextPageGroupButton').hide();
                    }
                    callback();
                }
            });
        }

        function nextPageAcc() {
            refreshSearchAccs(accPagePointer + 1, function () {
                accPagePointer++;
                if ((accPagePointer + 1) * 5 >= accListSize) {
                    $('#nextPageAccButton').hide();
                }

                if (accPagePointer > 0) {
                    $('#prevPageAccButton').show();
                }
            });
        }

        function prevPageAcc() {
            refreshSearchAccs(accPagePointer - 1, function () {
                accPagePointer--;
                if (accPagePointer === 0) {
                    $('#prevPageAccButton').hide();
                }

                if ((accPagePointer + 1) * 5 < accListSize) {
                    $('#nextPageAccButton').show();
                }
            });

        }

        function nextPageGroup() {
            refreshSearchGroups(groupPagePointer + 1, function () {
                groupPagePointer++;
                if ((groupPagePointer + 1) * 5 >= groupListSize) {
                    $('#nextPageGroupButton').hide();
                }

                if (groupPagePointer > 0) {
                    $('#prevPageGroupButton').show();
                }
            });
        }

        function prevPageGroup() {
            refreshSearchGroups(groupPagePointer - 1, function () {
                groupPagePointer--;
                if (groupPagePointer === 0) {
                    $('#prevPageGroupButton').hide();
                }

                if ((groupPagePointer + 1) * 5 < groupListSize) {
                    $('#nextPageGroupButton').show();
                }
            });
        }

        function selectAccountList() {
            $('#groupPager').hide();
            $('#accountPager').show();
        }

        function selectGroupList() {
            $('#groupPager').show();
            $('#accountPager').hide();
        }

    </script>
</head>

<body>

<jsp:include page="/common/header.jsp"/>
<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Search
                    results</strong></div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <div class="feed-activity-list">

                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#acc_content"
                                                      onclick="selectAccountList()">Accounts</a></li>
                                <li><a data-toggle="pill" href="#group_content" onclick="selectGroupList()">Groups</a>
                                </li>
                            </ul>


                            <div class="tab-content">
                                <div id="acc_content" class="tab-pane fade in active">

                                </div>
                                <ul class="pager" id="accountPager">
                                    <li class="previous" id="prevPageAccButton"><a href="#" onclick="prevPageAcc()">Previous</a>
                                    </li>
                                    <li class="next" id="nextPageAccButton"><a href="#" onclick="nextPageAcc()">Next</a>
                                    </li>
                                </ul>
                                <div id="group_content" class="tab-pane fade">

                                </div>
                                <ul class="pager" id="groupPager">
                                    <li class="previous"><a href="#" onclick="prevPageGroup()" id="prevPageGroupButton">Previous</a>
                                    </li>
                                    <li class="next"><a href="#" onclick="nextPageGroup()"
                                                        id="nextPageGroupButton">Next</a></li>
                                </ul>

                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
