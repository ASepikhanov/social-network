<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:forEach items="${groups}" var="item">
    <div class="feed-element">
        <a href="group?id=${item.groupId}" class="pull-left">
            <img alt="image" class="img-circle"
                 src="<c:choose>
                        <c:when test="${item.hasAvatar()}">
                            group_avatar?id=${item.groupId}
                        </c:when>
                        <c:otherwise>
                            <c:url value="/resources/img/default-group.jpg"/>
                        </c:otherwise>
                    </c:choose>">
        </a>

        <div class="media-body ">
            <a href="group?id=${item.groupId}" style="color: #333333">
                <strong>${item.name}</strong>
            </a>
            <br>
            <small class="text-muted">
                Subscribers: ${fn:length(item.approovedSubscribers)}</small>
        </div>
    </div>
    <!-- feed-element-->
</c:forEach>
<div id="groupListSize" data-value="${listSize}"></div>