<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Friend list</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/sendRequest.js"/>"></script>
</head>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Friends
                    of ${accountName}</strong></div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <div class="feed-activity-list">


                            <c:forEach items="${friendList}" var="item">
                                <div class="feed-element">
                                    <a href="account?id=${item.id}" class="pull-left">
                                        <img alt="image" class="img-circle"
                                             src="  <c:choose>
                                                        <c:when test="${item.hasAvatar()}">
                                                            avatar?id=${item.id}
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:url value="/resources/img/default-avatar.jpg"/>
                                                        </c:otherwise>
                                                    </c:choose>">
                                    </a>

                                    <div class="media-body ">
                                        <c:if test="${currentAccount.id == param.id}">
                                            <small class="pull-right" id="button_container${item.id}">
                                                <button onclick="sendRequest('button_container${item.id}',  ${item.id}, 3)"
                                                        type="button" class="btn btn-default btn-block">
                                                    <i class="fa fa-coffee"></i> Delete from friends
                                                </button>
                                            </small>
                                        </c:if>
                                        <a href="account?id=${item.id}">
                                            <strong>${item.name} ${item.surname}</strong> <br>
                                        </a>
                                        <br>
                                        <a href="pm?id=${item.id}" class="btn btn-xs btn-primary"><i
                                                class="fa fa-pencil"></i> Message</a>
                                    </div>
                                </div>
                                <!-- feed-element-->
                            </c:forEach>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
