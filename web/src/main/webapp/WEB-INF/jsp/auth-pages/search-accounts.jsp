<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:forEach items="${accounts}" var="item">
    <div class="feed-element">
        <a href="account?id=${item.id}" class="pull-left">
            <img alt="image" class="img-circle"
                 src="  <c:choose>
                            <c:when test="${item.hasAvatar()}">
                                avatar?id=${item.id}
                            </c:when>
                            <c:otherwise>
                                <c:url value="/resources/img/default-avatar.jpg"/>
                            </c:otherwise>
                        </c:choose>">
        </a>

        <div class="media-body ">
            <a href="account?id=${item.id}">
                <strong>${item.name} ${item.surname}</strong> <br>
            </a>
            <br>
            <a href="pm?id=${item.id}" class="btn btn-xs btn-primary"><i
                    class="fa fa-pencil"></i> Message</a>
        </div>
    </div>
    <!-- feed-element-->
</c:forEach>
<div id="accListSize" data-value="${listSize}"></div>