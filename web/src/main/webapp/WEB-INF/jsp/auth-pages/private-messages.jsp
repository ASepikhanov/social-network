<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Conversation</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/sendRequest.js"/>"></script>
    <script src="<c:url value="/resources/js/imageButtonLogic.js"/>"></script>
    <script src="<c:url value="/resources/js/sockjs.min.js"/>"></script>
    <script src="<c:url value="/resources/js/stomp.min.js"/>"></script>

    <style>
        div.feed-activity-list {
            width: 100%;
            height: 450px;
            overflow-y: scroll;
        }
    </style>
</head>

<body>

<jsp:include page="/common/header.jsp"/>

<script type="text/javascript">
    $(document).ready(function () {
        connect();
        var objDiv = document.getElementById("fal");
        objDiv.scrollTop = objDiv.scrollHeight;
    });

    function sendMessage(authorId, destinationId, name, surname) {
        var fd = new FormData();
        fd.append('imageFile', $('#my-file-selector')[0].files[0]);
        fd.append('text', $('#message_text').val());
        fd.append('destinationId', destinationId);

        $.ajax({
            url: "send_message_private",
            type: "post",
            processData: false,
            contentType: false,
            data: fd,
            success: function (resp) {
                var main = "<div class=\"feed-element\"  style=\"padding-right: 35px\">\n" +
                    "    <div class=\"pull-right\">\n" +
                    "    <img alt=\"image\" class=\"img-circle\" src=\"" + $('#currentAccountAvatarLink').data("value") + "\">\n" +
                    "    </div>\n" +
                    "    <div class=\"media-body\">\n" +
                    "    <strong class=\"pull-right\">" + name + ' ' + surname + "</strong>  <br>\n" +
                    "    <br>\n";
                var well = '';
                if (!(resp.text === null)) {
                    well = "<div class=\"well\" style='font-size: 14px'>\n" +
                        resp.text +
                        "    </div>\n";
                }

                var photo = '';
                if (!(resp.image === false)) {
                    photo = "<div class=\"photos\">\n" +
                        "       <a target=\"_blank\" href=\"message_image?id=" + resp.id + "\">\n" +
                        "       <img alt=\"image\" class=\"feed-photo\" src=\"message_image?id=" + resp.id + "\"></a>\n" +
                        "       </div>";
                }
                var end =
                    "    </div>\n" +
                    "    </div>";

                var newHtml = main + well + photo + end;
                $('.feed-activity-list').append(newHtml);
                var objDiv = document.getElementById("fal");
                objDiv.scrollTop = objDiv.scrollHeight;
                deselectImage();
                $('#message_text').val('');
                sendMessageSocks(resp.text, resp.image, resp.id);
            }
        });
    }

    //========================================WebSocket============================
    var stompClient = null;
    var urlParams = new URLSearchParams(window.location.search);

    function connect() {
        var socket = new SockJS('/chat');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/conversation/target/${currentAccount.id}/${param.id}', function (messageOutput) {
                var im = JSON.parse(messageOutput.body);
                var avatar = '<div class="feed-element" style="padding-right: 35px">\n' +
                    '<a href="account?id= ' + urlParams.get('id') + '" class="pull-left">\n' +
                    '           <img alt="image" class="img-circle" src="' + $('#conversationAvatarLink').data("value") + '">\n' +
                    '       </a>';
                var link = '<div class="media-body">\n' +
                    '           <a href="account?id=' + urlParams.get('id') + '">\n' +
                    '               <strong>' + $('#convName').data("value") + ' ' + $('#convSurname').data("value") + '</strong> <br>\n' +
                    '           </a>\n' +
                    '        <br>\n';
                var well = '';
                if (im.text != null) {
                    well = "<div class=\"well\" style='font-size: 14px'>\n" +
                        im.text +
                        "    </div>\n";
                }
                var photo = '';
                if (!(im.image === false)) {
                    photo = "<div class=\"photos\">\n" +
                        "       <a target=\"_blank\" href=\"message_image?id=" + im.id + "\">\n" +
                        "       <img alt=\"image\" class=\"feed-photo\" src=\"message_image?id=" + im.id + "\"></a>\n" +
                        "       </div>";
                }
                var end =
                    "    </div>\n" +
                    "    </div>";
                var newHtml = avatar + link + well + photo + end;
                $('.feed-activity-list').append(newHtml);
                var objDiv = document.getElementById("fal");
                objDiv.scrollTop = objDiv.scrollHeight;
            });
        });
    }

    $(window).bind('beforeunload', function () {
        if (stompClient != null) {
            stompClient.disconnect();
        }
        console.log("Disconnected");
    });

    function sendMessageSocks(text, image, messageId) {
        stompClient.send("/app/send/${param.id}/${currentAccount.id}", {},
            JSON.stringify({'text': text, 'image': image, 'id': messageId, 'destinationId': ${param.id}}));
    }
</script>

<div class="content">
    <div id="convName" data-value="${that.name}"></div>
    <div id="convSurname" data-value="${that.surname}"></div>
    <div id="conversationAvatarLink" data-value="  <c:choose>
                                                <c:when test="${that.hasAvatar()}">
                                                    avatar?id=${that.id}
                                                </c:when>
                                                <c:otherwise>
                                                    <c:url value="/resources/img/default-avatar.jpg"/>
                                                </c:otherwise>
                                            </c:choose>"></div>
    <div id="currentAccountAvatarLink" data-value="  <c:choose>
                                                                <c:when test="${currentAccount.hasAvatar()}">
                                                                    avatar?id=${currentAccount.id}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:url value="/resources/img/default-avatar.jpg"/>
                                                                </c:otherwise>
                                                            </c:choose>"></div>
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;">
                    <a href="pm">
                        <div class="pull-left">
                            <i style="font-size:24px; color:white" class="fa">&#xf060;</i>
                        </div>
                    </a>
                    <p style="text-align: right">
                        <strong style="color:white;">Conversation with ${that.name}</strong>
                    </p>
                </div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <div class="feed-activity-list" id="fal">

                            <c:forEach items="${messages}" var="item">
                                <div class="feed-element" style="padding-right: 35px">
                                    <c:choose>
                                        <c:when test="${item.author.id == currentAccount.id}">
                                            <div class="pull-right">
                                                <img alt="image" class="img-circle"
                                                     src="  <c:choose>
                                                                <c:when test="${item.author.hasAvatar()}">
                                                                    avatar?id=${item.author.id}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:url value="/resources/img/default-avatar.jpg"/>
                                                                </c:otherwise>
                                                            </c:choose>">
                                            </div>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="account?id=${item.author.id}" class="pull-left">
                                                <img alt="image" class="img-circle"
                                                     src="  <c:choose>
                                                                <c:when test="${item.author.hasAvatar()}">
                                                                    avatar?id=${item.author.id}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:url value="/resources/img/default-avatar.jpg"/>
                                                                </c:otherwise>
                                                            </c:choose>">
                                            </a>
                                        </c:otherwise>
                                    </c:choose>


                                    <div class="media-body">
                                        <c:choose>
                                            <c:when test="${item.author.id == currentAccount.id}">
                                                <strong class="pull-right">${item.author.name} ${item.author.surname}</strong>
                                                <br>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="account?id=${item.author.id}">
                                                    <strong>${that.name} ${that.surname}</strong> <br>
                                                </a>
                                            </c:otherwise>
                                        </c:choose>

                                        <br>
                                        <c:if test="${!(empty item.text)}">
                                            <div class="well" style="font-size: 14px">
                                                <c:out value="${item.text}"/>
                                            </div>
                                        </c:if>
                                        <c:if test="${item.image == 'true'}">
                                            <div class="photos">
                                                <a target="_blank" href="message_image?id=${item.id}">
                                                    <img alt="image" class="feed-photo"
                                                         src="message_image?id=${item.id}"></a>
                                            </div>
                                        </c:if>
                                    </div>
                                </div>
                                <!-- feed-element-->
                            </c:forEach>


                        </div>
                        <div class="feed-element">
                            <div class="media-body ">
                                <br>
                                <form enctype="multipart/form-data" id="messageForm" role="form"
                                      action="send_message_private" method="post">
                                    <input type="hidden" value="${that.id}" name="destination_id">
                                    <textarea class="form-control" rows="2" name="text" id="message_text"></textarea>
                                    <br>
                                    <label class="btn btn-primary" for="my-file-selector">
                                        <input name="image" id="my-file-selector" type="file" accept=".jpg"
                                               style="display:none"
                                               onchange="imageSelected(this.files[0].name)">
                                        Attach image
                                    </label>
                                    <span class='label label-info' id="upload-file-info" style="font-size: 14px"></span>
                                    <div class="pull-right">
                                        <button class="btn btn-info" type="button"
                                                onclick="sendMessage(${currentAccount.id}, ${that.id}, '${currentAccount.name}', '${currentAccount.surname}')">
                                            Send
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div><!-- feed-element-->
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
