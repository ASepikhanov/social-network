<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Conversation list</title>
    <jsp:include page="/common/meta.jsp"/>
    <script src="<c:url value="/resources/js/sendRequest.js"/>"></script>
</head>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Your
                    conversations</strong></div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <div class="feed-activity-list">

                            <style>
                                .feed-element:hover {
                                    background-color: lightcyan;
                                }

                                .feed-element {
                                    padding-right: 35px;
                                    display: block;
                                    text-decoration: none !important;
                                }
                            </style>

                            <c:forEach items="${conversationList}" var="item">
                                <c:set var="conversationAccount" value="${conversationAccounts[item.id]}"/>

                                <a href="pm?id=${conversationAccount.id}" class="feed-element">
                                    <div class="pull-left">
                                        <img alt="image" class="img-circle"
                                             src="  <c:choose>
                                                        <c:when test="${conversationAccount.hasAvatar()}">
                                                            avatar?id=${conversationAccount.id}
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:url value="/resources/img/default-avatar.jpg"/>
                                                        </c:otherwise>
                                                    </c:choose>">
                                    </div>


                                    <div class="media-body">

                                        <strong> ${conversationAccount.name} ${conversationAccount.surname}</strong>
                                        <br>
                                        <br>
                                        <c:choose>
                                            <c:when test="${currentAccount.id == item.author.id}">
                                                <c:choose>
                                                    <c:when test="${!(empty item.text) && item.image == 'true'}">
                                                        <div class="text-muted" style="color:green; padding-left: 25px">
                                                            You: <span style="color:gray">${item.text}</span>
                                                        </div>
                                                        <div class="text-muted" style="padding-left: 57px">
                                                            Attached image
                                                            <i style="font-size:16px; color:gray"
                                                               class="fa">&#xf03e;</i>
                                                        </div>
                                                    </c:when>
                                                    <c:when test="${!(empty item.text) && !(item.image == 'true')}">
                                                        <div class="text-muted" style="color:green; padding-left: 25px">
                                                            You: <span style="color:gray">${item.text}</span>
                                                        </div>
                                                    </c:when>
                                                    <c:when test="${empty item.text && item.image == 'true'}">
                                                        <div class="text-muted" style="color:green; padding-left: 25px">
                                                            You:
                                                            <span style="color:gray">
                                                            Attached image
                                                            <i style="font-size:16px" class="fa">&#xf03e;</i>
                                                            </span>
                                                        </div>
                                                    </c:when>
                                                </c:choose>
                                            </c:when>
                                            <c:otherwise>
                                                <c:if test="${!(empty item.text)}">
                                                    <div class="text-muted" style="color:gray; padding-left: 25px">
                                                            ${item.text}
                                                    </div>
                                                </c:if>
                                                <c:if test="${item.image == 'true'}">
                                                    <div class="text-muted" style="padding-left: 25px">
                                                        Attached image
                                                        <i style="font-size:16px; color:gray" class="fa">&#xf03e;</i>
                                                    </div>
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>

                                    </div>
                                </a><!-- feed-element-->
                            </c:forEach>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
