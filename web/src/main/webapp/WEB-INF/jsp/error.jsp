<!DOCTYPE html>
<html>
<title>Registration failed</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>


<div class="w3-row">
    <div class="w3-col w3-container" style="width:35%"></div>


    <div class="w3-col w3-container" style="width:30%">
        <div class="w3-container w3-card-4 w3-light-grey">
            <p class="w3-text-red">Error occurred with reason ${message}<br></p>
        </div>
    </div>


    <div class="w3-col w3-container" style="width:32%"></div>
</div>

</body>
</html>
