<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Admin list</title>
    <jsp:include page="/common/meta.jsp"/>
</head>

<script>
    function adminRequest(container_id, id, action) {
        $.ajax({
            url: "admin_list",
            type: "post",
            data: {
                id: id,
                action: action
            },
            success: function (result) {
                var arr = result.split(":");
                var action = '\x27' + arr[2] + '\x27';
                var fullResult = '<button  onclick="adminRequest( \x27' + container_id + '\x27, ' +
                    +arr[1]
                    + ', '
                    + action
                    + ' )" type="button" class="btn btn-default btn-block"> '
                    + arr[0]
                    + ' </button>';
                document.getElementById(container_id).innerHTML = fullResult;
            }
        });
    }

    function isIdCorrect() {
        var record = $('#admin-new');
        var textField = record.find(".form-control");
        if (textField.val().match("^[0-9]*$") && textField.val().length > 0 && textField.val().length < 12) {
            record.find(".btn-commit").prop("disabled", false);
        } else {
            record.find(".btn-commit").prop("disabled", true);
        }
    }

    function createAdmin() {
        var record = $('#admin-new');
        var textField = record.find(".form-control");
        adminRequest('a', textField.val(), 'add');
        location.reload();
    }
</script>

<body>

<jsp:include page="/common/header.jsp"/>

<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Network
                    admins</strong></div>
                <div class="panel-body">
                    <div class="ibox-content">
                        <div class="feed-activity-list">


                            <form id="addAdminForm" role="form" action="admin_list" method="post">
                                <div class="input-group" id="admin-new">
                                    <input type="hidden" value="add" name="action">
                                    <input type="text" class="form-control" onkeyup="isIdCorrect()" name="id">
                                    <span class="input-group-btn right-group">
                                            <button class="btn btn-default btn-commit" disabled type="button"
                                                    onclick="createAdmin()">Save and add</button>
                                    </span>
                                </div>
                            </form>


                            <c:forEach items="${admins}" var="item">
                                <div class="feed-element">
                                    <a href="account?id=${item.id}" class="pull-left">
                                        <img alt="image" class="img-circle"
                                             src="<c:choose>
                                                    <c:when test="${item.hasAvatar()}">
                                                        avatar?id=${item.id}
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:url value="/resources/img/default-avatar.jpg"/>
                                                    </c:otherwise>
                                                </c:choose>">
                                    </a>

                                    <div class="media-body ">
                                        <c:if test="${item.id != currentAccount.id}">
                                            <small class="pull-right" id="button_container${item.id}">
                                                <button onclick="adminRequest('button_container${item.id}',  ${item.id}, 'remove')"
                                                        type="button" class="btn btn-default btn-block">
                                                    Remove from admins
                                                </button>
                                            </small>
                                        </c:if>
                                        <a href="account?id=${item.id}">
                                            <strong>${item.name} ${item.surname}</strong> <br>
                                        </a>
                                        <br>
                                        <a href="pm?id=${item.id}" class="btn btn-xs btn-primary"><i
                                                class="fa fa-pencil"></i> Message</a>
                                    </div>
                                </div>
                                <!-- feed-element-->
                            </c:forEach>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
