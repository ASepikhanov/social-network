<!DOCTYPE html>
<html>
<head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <meta charset="utf-8">
    <title>Registration</title>

    <link rel="stylesheet" href="<c:url value="/resources/css/w3.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-select.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/bootstrap-formhelpers.min.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/resources/css/page.css"/>">

    <script src="<c:url value="/resources/js/jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-select.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrapvalidator.min.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap-formhelpers.min.js"/>"></script>

    <script src="<c:url value="/resources/js/countrypicker.js"/>"></script>

    <script src="<c:url value="/resources/js/inputFormValidator.js"/>"></script>
    <script src="<c:url value="/resources/js/futureDateRestrict.js"/>"></script>
    <script src="<c:url value="/resources/js/uniqueMail.js"/>"></script>

    <script>
        $(document).ready(function () {
            $('.bfh-selectbox').on('change.bfhselectbox', function () {
                var country = $(this).val();
                $("#hiddenField").val(country);
            });

        });
    </script>

    <style>label {
        font-weight: normal !important;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }</style>
</head>

<body>


<div class="content">
    <div id="pad-wrapper">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: #33d6ff;"><strong style="color:white;">Register new
                    account</strong></div>
                <div class="panel-body">

                    <div class="ibox-content">
                        <form:form modelAttribute="registerAccount" enctype="multipart/form-data" role="form"
                                   id="profileForm"
                                   action="register" method="post">
                            <input value="${registerAccount.country.alpha2}" type="hidden" id="hiddenField"
                                   name="country"/>

                            <label>First name</label>
                            <div class="form-group">
                                <form:input path="name" class="form-control" type="text"/>
                            </div>

                            <label>Last name</label>
                            <div class="form-group">
                                <form:input path="surname" class="form-control" type="text"/>
                            </div>

                            <label>Middle name</label>
                            <div class="form-group">
                                <form:input path="middleName" class="form-control" type="text"/>
                            </div>

                            <label>Date of birth</label>
                            <div class="form-group">
                                <form:input path="birthDate" class="form-control" type="date" id="datepicker"/>
                            </div>

                            <label>Email</label>
                            <div class="form-group">
                                <form:input path="email" class="form-control" type="text"/>
                            </div>

                            <label>ICQ</label>
                            <div class="form-group">
                                <form:input path="icq" class="form-control" type="text"/>
                            </div>

                            <label>Skype</label>
                            <div class="form-group">
                                <form:input path="skype" class="form-control" type="text"/>
                            </div>

                            <div class="form-row">

                                <div class="col-md-8">
                                    <label>City</label>
                                    <div class="form-group">
                                        <form:input path="city" class="form-control" type="text"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Country</label>
                                    <div class="form-group">
                                        <div class="bfh-selectbox bfh-countries" data-country="RU" data-flags="true"
                                             data-filter="true">
                                            <a class="bfh-selectbox-toggle" role="button" data-toggle="bfh-selectbox"
                                               href="#">
                                                <span class="bfh-selectbox-option input-medium" data-option=""></span>
                                                <b class="caret"></b>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <label>Password</label>
                            <div class="form-group">
                                <input name="password" class="form-control" type="password" id="password">
                            </div>

                            <label>Confirm password</label>
                            <div class="form-group">
                                <input name="confirmPassword" class="form-control" type="password">
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label>Avatar</label>
                                    <div class="form-group">
                                        <input name="avatarFile" type="file" accept=".jpg">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <br>
                                    <div class="pull-right">
                                        <button class="btn btn-info" type="submit">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-2">
        </div>
    </div>

</div>

</body>
</html>
