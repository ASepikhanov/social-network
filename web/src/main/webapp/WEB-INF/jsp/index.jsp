<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<title>Social network</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<body>


<div class="w3-row">
    <div class="w3-col w3-container" style="width:35%"></div>


    <div class="w3-col w3-container" style="width:30%">
        <form class="w3-container w3-card-4 w3-light-grey" action="login" method="post">
            <h2 class="w3-text-blue">Sign in</h2>
            <p>
                <label class="w3-text-blue"><b>Email</b></label>
                <input class="w3-input w3-border" name="username" type="text"></p>
            <p>
                <label class="w3-text-blue"><b>Password</b></label>
                <input class="w3-input w3-border" name="password" type="password"></p>

            <div class="w3-cell-row" style="padding-bottom: 10px">
                <div class="w3-cell">
                    <button class="w3-btn w3-blue">Login</button>
                    <input id="checkBox" type="checkbox" name="remember-me"> <span> Remember me</span>
                </div>
                <div class="w3-cell w3-right-align w3-text-blue">
                    <p><a href="register">Register</a></p>
                </div>
            </div>
        </form>
    </div>


    <div class="w3-col w3-container" style="width:32%"></div>
</div>

</body>
</html>
