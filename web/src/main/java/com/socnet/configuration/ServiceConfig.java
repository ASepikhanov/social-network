package com.socnet.configuration;

import com.socnet.*;
import com.socnet.validation.AccountValidator;
import com.socnet.validation.GroupValidator;
import com.socnet.validation.PhoneValidator;
import com.socnet.validation.required.ValidateMailUnique;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
public class ServiceConfig {

    @Bean
    public ImageService imageService(ImageDao imageDao) {
        return new ImageService(imageDao);
    }

    @Bean
    public GroupValidator groupValidator() {
        return new GroupValidator();
    }

    @Bean
    public AccountValidator accountValidator(AccountService accountService) {
        return new AccountValidator(accountService);
    }

    @Bean
    public PhoneValidator phoneValidator() {
        return new PhoneValidator();
    }

    @Bean
    public ValidateMailUnique validateMailUnique(AccountService accountService) {
        return new ValidateMailUnique(accountService);
    }

    @Bean
    public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
        return jpaTransactionManager;
    }
}
