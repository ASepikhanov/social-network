package com.socnet.configuration;

import com.socnet.*;
import com.socnet.jpa.AccountDaoJpa;
import com.socnet.jpa.GroupDaoJpa;
import com.socnet.jpa.MessageDaoJpa;
import com.socnet.jpa.PhoneDaoJpa;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;

@Configuration
public class DaoConfig {

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("com.socnet");
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        return em;
    }

    @Bean
    public AccountDao accountDao() {
        return new AccountDaoJpa();
    }

    @Bean
    public PhoneDao phoneDao() {
        return new PhoneDaoJpa();
    }

    @Bean
    public GroupDao groupDao() {
        return new GroupDaoJpa();
    }

    @Bean
    public MessageDao messageDao() {
        return new MessageDaoJpa();
    }

    @Bean
    public ImageDao imageDao() {
        return new ImageDao("GvgdpOiHLuwAAAAAAACY2hMbshiNDB3c1GBxaM__zRluka68CzYav-nvSX8twXaT");
    }
}
