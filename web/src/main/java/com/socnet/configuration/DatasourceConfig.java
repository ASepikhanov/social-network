package com.socnet.configuration;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiObjectFactoryBean;

import javax.naming.NamingException;
import javax.sql.DataSource;

@Configuration
public class DatasourceConfig {
    private static final String DATABASE_SOURCE_VARIABLE = "database_source";
    private static final String DATABASE_SOURCE = System.getenv(DATABASE_SOURCE_VARIABLE);

    @Bean
    public DataSource jndiDataSource() throws IllegalArgumentException, NamingException {
        JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
        bean.setJndiName("java:comp/env/jdbc/" + DATABASE_SOURCE);
        bean.setProxyInterface(DataSource.class);
        bean.setLookupOnStartup(false);
        bean.afterPropertiesSet();
        return (DataSource) bean.getObject();
    }

    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        if ("local_db".equals(DATABASE_SOURCE)) {
            return new LocalTomcatFactory();
        } else {
            return new HerokuTomcatFactory();
        }
    }

    private class NamingEnabledTomcatFactory extends TomcatServletWebServerFactory {
        @Override
        protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
            tomcat.enableNaming();
            return super.getTomcatWebServer(tomcat);
        }
    }

    private class LocalTomcatFactory extends NamingEnabledTomcatFactory {
        @Override
        protected void postProcessContext(Context context) {
            ContextResource resource = new ContextResource();
            resource.setName("jdbc/" + DATABASE_SOURCE);
            resource.setAuth("Container");
            resource.setProperty("maxTotal", "50");
            resource.setProperty("maxIdle", "20");
            resource.setProperty("maxWaitMillis", "10000");
            resource.setProperty("username", "javaapp");
            resource.setProperty("password", "admin");
            resource.setType(DataSource.class.getName());
            resource.setProperty("driverClassName", "com.mysql.jdbc.Driver");
            resource.setProperty("url", "jdbc:mysql://localhost:3307/network?characterEncoding=utf8");

            context.getNamingResources().addResource(resource);
        }
    }

    private class HerokuTomcatFactory extends NamingEnabledTomcatFactory {
        @Override
        protected void postProcessContext(Context context) {
            ContextResource resource = new ContextResource();
            resource.setName("jdbc/" + DATABASE_SOURCE);
            resource.setAuth("Container");
            resource.setProperty("maxTotal", "15");
            resource.setProperty("maxIdle", "12");
            resource.setProperty("maxWaitMillis", "10000");
            resource.setProperty("username", "frdpyojpuabmhj");
            resource.setProperty("password", "a176ea01b55a997b85dbe3face091841465ec030bfc0272db24ec9ed011d5777");
            resource.setType(DataSource.class.getName());
            resource.setProperty("driverClassName", "org.postgresql.Driver");
            resource.setProperty("url",
                    "jdbc:postgresql://ec2-54-243-63-13.compute-1.amazonaws.com:5432/dicos7f5kbo11?sslmode=require&characterEncoding=utf8");
            context.getNamingResources().addResource(resource);
        }
    }
}
