package com.socnet.configuration;

import com.socnet.AccountDao;
import com.socnet.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
@EnableTransactionManagement(proxyTargetClass = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthService authService;

    @Bean
    public UserDetailsService userDetailsService(AccountDao accountDao) {
        return new AuthService(accountDao);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/check_mail").permitAll()
                .antMatchers("/unauthorizedAccess").permitAll()
                .antMatchers("/index").anonymous()
                .antMatchers("/loginFailed*").anonymous()
                .antMatchers("/register*").anonymous()
                .antMatchers("/registrationFailed*").anonymous()
                .antMatchers("/registrationSuccess*").anonymous()
                .antMatchers("/admin_list").hasRole("ADMIN")
                .antMatchers("/**").hasRole("USER")
                    .and()
                .formLogin().loginPage("/index").loginProcessingUrl("/login").defaultSuccessUrl("/account")
                .failureUrl("/loginFailed")
                    .and()
                .logout().logoutSuccessUrl("/index").deleteCookies("JSESSIONID").permitAll()
                .logoutUrl("/logout").invalidateHttpSession(true)
                    .and()
                .rememberMe().rememberMeParameter("remember-me")
                .tokenValiditySeconds(10800).userDetailsService(authService)
                    .and()
                .csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) {
        web
                .ignoring().antMatchers("/resources/**, favicon.ico");
    }

    @Bean
    public DaoAuthenticationProvider authProvider(AuthService authService) {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(authService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(5);
    }
}
