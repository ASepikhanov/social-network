package com.socnet;

import com.socnet.controller.AbstractController;
import com.socnet.message.Message;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ImageServlet extends AbstractController {

    @RequestMapping(value = {"/avatar", "/group_avatar", "/message_image"})
    public void getImage(HttpServletRequest request, HttpServletResponse response,
                         @RequestParam("id") Integer requestId) throws IOException {
        response.setContentType("image/jpeg");
        String imageType = request.getRequestURI().substring(request.getContextPath().length());
        try (OutputStream out = response.getOutputStream()) {
            switch (imageType) {
                case "/avatar":
                    imageService.writeAccountAvatar(requestId, out);
                    break;
                case "/group_avatar":
                    imageService.writeGroupAvatar(requestId, out);
                    break;
                case "/message_image":
                    Message message = messageService.getMessageById(requestId);
                    Account requester = getCurrentAccount();
                    if (message.hasImage()) {
                        imageService.writeMessageImage(message, out, requester);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
