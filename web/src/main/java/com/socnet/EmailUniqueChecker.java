package com.socnet;

import com.socnet.validation.required.ValidateMailUnique;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EmailUniqueChecker {

    @Autowired
    private ValidateMailUnique mailValidator;

    @RequestMapping(value = "/check_mail", method = RequestMethod.POST)
    @ResponseBody
    public String doCheck(@RequestParam("email") String email) {
        boolean checkResult = mailValidator.doCheck(email);
        return String.valueOf(checkResult);
    }
}
