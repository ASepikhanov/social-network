package com.socnet.controller;

import com.socnet.Account;
import com.socnet.Group;
import com.socnet.GroupRole;
import com.socnet.ServiceException;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class EditGroupController extends AbstractController {

    @RequestMapping(value = "/edit_group", method = RequestMethod.GET)
    public String getEditGroupPage(@RequestParam("id") Integer id,
                                   Model model) {
        Account editor = getCurrentAccount();
        try {
            Group group = groupService.getGroupById(id);
            GroupRole editorRole = group.getSubscriberRoles().get(editor.getId());
            if (editorRole == GroupRole.MODERATOR || editorRole == GroupRole.ADMIN) {
                model.addAttribute("updatingGroup", group);
                return "/auth-pages/edit-group";
            } else {
                return ERROR_PAGE;
            }
        } catch (ServiceException e) {
            return ERROR_PAGE;
        }
    }

    @RequestMapping(value = "/edit_group", method = RequestMethod.POST)
    public ModelAndView updateGroup(@ModelAttribute("updatingGroup") Group formReadGroup,
                                    @RequestParam("avatarFile") MultipartFile file) throws IOException {
        Account editor = getCurrentAccount();
        try {
            groupService.editGroup(formReadGroup, editor, file);
            return new ModelAndView("redirect:/group?id=" + formReadGroup.getGroupId());
        } catch (ServiceException e) {
            return handleServiceException(e);
        }
    }
}
