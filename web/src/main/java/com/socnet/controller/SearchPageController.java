package com.socnet.controller;

import com.socnet.Account;
import com.socnet.Group;
import com.socnet.controller.viewdto.AccountViewDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SearchPageController extends AbstractController {

    private static final int LIVE_SEARCH_THRESHOLD = 5;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search() {
        return "/auth-pages/search";
    }

    @RequestMapping(value = "/search_live", method = RequestMethod.POST)
    @ResponseBody
    public Collection<AccountViewDto> liveSearch(@RequestParam("query") String query) {
        Collection<Account> accounts = accountService.getAccountsByName(query == null ? "" : query)
                .stream().limit(LIVE_SEARCH_THRESHOLD).collect(Collectors.toList());
        Collection<AccountViewDto> viewAccounts = new ArrayList<>(accounts.size());
        for (Account account : accounts) {
            viewAccounts.add(new AccountViewDto(account.getId(), account.getName(), account.getSurname()));
        }
        return viewAccounts;
    }

    @RequestMapping(value = "search_accounts", method = RequestMethod.GET)
    public ModelAndView searchAccounts(@RequestParam(value = "q", required = false) String query,
                                       @RequestParam(value = "page", required = false) Integer page) {
        long startTime = System.currentTimeMillis();
        query = query == null ? "" : query;
        page = page == null ? 0 : page;
        List<Account> foundAccounts = accountService.getAccountsByName(query);
        ModelAndView modelAndView = new ModelAndView("/auth-pages/search-accounts");
        modelAndView.addObject("accounts", foundAccounts
                .subList(page * 5, Math.min((page + 1) * 5, foundAccounts.size())));
        modelAndView.addObject("listSize", foundAccounts.size());
        long elapsedTime = System.currentTimeMillis() - startTime;
        logger.debug("search accounts completed in {} ms", elapsedTime);
        return modelAndView;
    }

    @RequestMapping(value = "search_groups", method = RequestMethod.GET)
    public ModelAndView searchGroups(@RequestParam(value = "q", required = false) String query,
                                     @RequestParam(value = "page", required = false) Integer page) {
        long startTime = System.currentTimeMillis();
        query = query == null ? "" : query;
        page = page == null ? 0 : page;
        List<Group> foundGroups = groupService.getGroupsByName(query);
        ModelAndView modelAndView = new ModelAndView("/auth-pages/search-groups");
        modelAndView.addObject("groups", foundGroups
                .subList(page * 5, Math.min((page + 1) * 5, foundGroups.size())));
        modelAndView.addObject("listSize", foundGroups.size());
        long elapsedTime = System.currentTimeMillis() - startTime;
        logger.debug("search groups completed in {} ms", elapsedTime);
        return modelAndView;
    }
}
