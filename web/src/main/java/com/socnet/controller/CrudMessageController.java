package com.socnet.controller;

import com.socnet.Account;
import com.socnet.ServiceException;
import com.socnet.controller.viewdto.MessageViewDto;
import com.socnet.message.DestinationEntity;
import com.socnet.message.Message;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class CrudMessageController extends AbstractController {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @RequestMapping(value = "send_message_wall", method = RequestMethod.POST)
    public String sendAccountWallMessage(@ModelAttribute("newWallMessage") Message message,
                                         @RequestParam("attachedImage") MultipartFile imageFile) throws IOException {
        Account author = getCurrentAccount();
        message.setAuthor(author);
        message.setWriteTime(LocalDateTime.now());
        InputStream image = imageFile.getInputStream();
        messageService.createWallMessage(message, image);
        return "redirect:/account?id=" + message.getDestinationId();
    }

    @RequestMapping(value = "send_message_group", method = RequestMethod.POST)
    public String sendGroupWallMessage(@ModelAttribute("newWallMessage") Message message,
                                       @RequestParam("attachedImage") MultipartFile imageFile) throws IOException {
        Account author = getCurrentAccount();
        message.setAuthor(author);
        message.setWriteTime(LocalDateTime.now());
        InputStream image = imageFile.getInputStream();
        messageService.createWallMessage(message, image);
        return "redirect:/group?id=" + message.getDestinationId();
    }

    @RequestMapping(value = "send_message_private", method = RequestMethod.POST)
    @ResponseBody
    public MessageViewDto sendPrivateMessage(@ModelAttribute Message message,
                                             @RequestParam("imageFile") Part imageFile) throws IOException {
        Account author = getCurrentAccount();
        message.setAuthor(author);
        message.setDestinationEntity(DestinationEntity.PRIVATE_MESSAGE);
        message.setWriteTime(LocalDateTime.now());
        InputStream image = imageFile.getInputStream();
        Integer generatedId = messageService.createPrivateMessage(message, image);
        message.setId(generatedId);
        return new MessageViewDto(message.getText(), message.getImage(), message.getId());
    }

    @RequestMapping(value = "delete_message", method = RequestMethod.POST)
    @ResponseBody
    public String deleteMessage(@RequestParam("subject_message") Integer subjectMessageId) throws ServiceException {
        Account requester = getCurrentAccount();
        Message messageToDelete = messageService.getMessageById(subjectMessageId);
        switch (messageToDelete.getDestinationEntity()) {
            case ACCOUNT:
                messageService.deleteMessageFromAccountWall(messageToDelete, requester);
                break;
            case GROUP:
                messageService.deleteMessageFromGroupWall(messageToDelete,
                        requester, groupService.getGroupById(messageToDelete.getDestinationId()));
                break;
            case PRIVATE_MESSAGE:
                throw new UnsupportedOperationException();
            default:
                throw new UnsupportedOperationException();
        }
        return "OK";
    }

    @MessageMapping("/send/{idTo}/{idFrom}")
    @PreAuthorize("authentication.principal.account.id == #idTo")
    public void send(@DestinationVariable String idTo, @DestinationVariable String idFrom,
                     @Payload MessageViewDto message) {
        simpMessagingTemplate
                .convertAndSend("/conversation/target/" + idTo + "/" + idFrom, message);
    }
}
