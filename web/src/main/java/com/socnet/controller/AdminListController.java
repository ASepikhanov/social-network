package com.socnet.controller;

import com.socnet.Account;
import com.socnet.ServiceException;

import java.util.Collection;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminListController extends AbstractController {

    @RequestMapping(value = "/admin_list", method = RequestMethod.GET)
    public ModelAndView getAdminList() {
        Collection<Account> admins = accountService.getAdminList();
        ModelAndView modelAndView = new ModelAndView("/admin/admin-list");
        modelAndView.addObject("admins", admins);
        return modelAndView;
    }

    @RequestMapping(value = "/admin_list", method = RequestMethod.POST)
    @ResponseBody
    public String doPost(@RequestParam("id") Integer applyId, @RequestParam("action") String action) {
        Account currentAccount = getCurrentAccount();
        try {
            switch (action) {
                case "remove":
                    accountService.removeAdmin(currentAccount, applyId);
                    logger.info("Account {}, removed admin {}", currentAccount.getId(), applyId);
                    return "Add admin:" + applyId + ':' + "add";
                case "add":
                    accountService.addAdmin(currentAccount, applyId);
                    logger.info("Account {}, added admin {}", +currentAccount.getId(), applyId);
                    return "Remove from admins:" + applyId + ":remove";
                default:
                    logger.info("Account {} tried to perform undefined operation: {}", currentAccount.getId(), action);
                    return "Undefined action";
            }
        } catch (ServiceException e) {
            return "Error occurred with " + e.getMessage();
        }
    }
}
