package com.socnet.controller;

import com.socnet.Account;
import com.socnet.ServiceException;

import java.util.Collection;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FriendListController extends AbstractController {

    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public ModelAndView getFriendList(@RequestParam(value = "id", required = false) Integer id) {
        Account requestAccount = getCurrentAccount();
        if (id != null) {
            try {
                Account account = accountService.getAccountById(id, true);
                Collection<Account> friends = accountService.getAccountListByIdList(account.getFriends());
                ModelAndView modelAndView = new ModelAndView("/auth-pages/friends");
                modelAndView.addObject("friendList", friends);
                modelAndView.addObject("accountName", account.getName());
                return modelAndView;
            } catch (ServiceException e) {
                return handleServiceException(e);
            }
        } else {
            return new ModelAndView("redirect:friends?id=" + requestAccount.getId());
        }
    }

    @RequestMapping(value = "/friend_requests", method = RequestMethod.GET)
    public ModelAndView getFriendRequests() {
        Account account = getCurrentAccount();
        accountService.updateFriendRequests(account);
        Collection<Account> inRequestList = accountService.getAccountListByIdList(account.getInFriendRequests());
        Collection<Account> outRequestList = accountService.getAccountListByIdList(account.getOutFriendRequests());
        ModelAndView modelAndView = new ModelAndView("/auth-pages/friend-requests");
        modelAndView.addObject("inRequestList", inRequestList);
        modelAndView.addObject("outRequestList", outRequestList);
        return modelAndView;
    }
}
