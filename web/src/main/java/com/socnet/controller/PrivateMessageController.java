package com.socnet.controller;

import com.socnet.Account;
import com.socnet.ServiceException;
import com.socnet.message.Message;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PrivateMessageController extends AbstractController {

    @RequestMapping(value = "/pm", method = RequestMethod.GET)
    public ModelAndView getPrivateMessages(@RequestParam(value = "id", required = false) Integer accountId) {
        Account requester = getCurrentAccount();
        if (accountId == null) {
            List<Message> conversationList = messageService.getAccountConversationList(requester);
            Map<Integer, Account> conversationAccounts = new HashMap<>();
            Account otherAccount;
            for (Message message : conversationList) {
                try {
                    if (requester.getId().equals(message.getAuthor().getId())) {
                        otherAccount = accountService.getAccountById(message.getDestinationId());
                    } else {
                        otherAccount = message.getAuthor();
                    }
                    conversationAccounts.put(message.getId(), otherAccount);
                    if (message.getText() != null && message.getText().length() > 80) {
                        message.setText(message.getText().substring(0, 79) + "...");
                    }
                } catch (ServiceException e) {
                    return handleServiceException(e);
                }
            }
            ModelAndView modelAndView = new ModelAndView("/auth-pages/private-message-contacts");
            modelAndView.addObject("conversationList", conversationList);
            modelAndView.addObject("conversationAccounts", conversationAccounts);
            return modelAndView;
        } else {
            try {
                Account conversatingAccount = accountService.getAccountById(accountId);
                List<Message> privateMessages = messageService.getPrivateMessages(conversatingAccount, requester);
                ModelAndView modelAndView = new ModelAndView("/auth-pages/private-messages");
                modelAndView.addObject("messages", privateMessages);
                modelAndView.addObject("that", conversatingAccount);
                return modelAndView;
            } catch (ServiceException e) {
                return handleServiceException(e);
            }
        }
    }
}
