package com.socnet.controller;

import com.neovisionaries.i18n.CountryCode;
import com.socnet.Account;
import com.socnet.Phone;
import com.socnet.ServiceException;
import com.socnet.controller.viewdto.AccountViewDto;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.IOException;
import java.time.LocalDate;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class EditAccountXmlController extends AbstractController {

    private static final XStream xStream = new XStream(new DomDriver());

    static {
        xStream.alias("owner", Account.class);
        xStream.alias("account", AccountViewDto.class);
        xStream.alias("country", CountryCode.class);
        xStream.omitField(Account.class, "inFriendRequests");
        xStream.omitField(Account.class, "outFriendRequests");
        xStream.omitField(Account.class, "friends");
        xStream.omitField(Account.class, "groups");
        xStream.omitField(Account.class, "phones");
        xStream.omitField(Account.class, "registrationDate");
        xStream.setMode(XStream.NO_REFERENCES);

        xStream.alias("phone", Phone.class);
        xStream.registerConverter(new AbstractSingleValueConverter() {
            @Override
            public boolean canConvert(Class type) {
                return LocalDate.class.equals(type);
            }

            @Override
            public Object fromString(String s) {
                return LocalDate.parse(s);
            }

            public String toString(Object source) {
                return source.toString();
            }
        });
    }

    @RequestMapping(value = "process_xml", method = RequestMethod.POST)
    public String processXml(@RequestParam("xmlFile") MultipartFile xmlFile,
                             Model model) throws IOException {
        String xmlString = new String(xmlFile.getBytes());
        AccountViewDto account = (AccountViewDto) xStream.fromXML(xmlString);
        model.addAttribute("updatingAccount", account);
        return "auth-pages/edit-account";

    }

    @RequestMapping(value = "/get_xml", method = RequestMethod.GET)
    public void getXmlFile(@RequestParam(value = "id") Integer requestId,
                           HttpServletResponse response) throws IOException {
        try {
            String serialized = xStream
                    .toXML(AccountViewDto.convertToDto(accountService.getAccountById(requestId)));
            response.setContentType("text/plain");
            response.setHeader("Content-Disposition", "attachment;filename=account" + requestId + ".xml");
            ServletOutputStream out = response.getOutputStream();
            out.println(serialized);
            out.flush();
            out.close();
        } catch (ServiceException e) {
            logger.error(e.getMessage());
        }
    }
}
