package com.socnet.controller;

import com.socnet.Account;
import com.socnet.ServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class RelationController extends AbstractController {

    @RequestMapping(value = "/RelationController", method = RequestMethod.POST)
    @ResponseBody
    public String requestRelation(@RequestParam("id_to") Integer idTo,
                                  @RequestParam(value = "accept_or_reject", required = false) String doubleButtonStatus,
                                  @RequestParam("relationStatus") Integer action) {
        Account from = getCurrentAccount();
        try {
            Account to = accountService.getAccountById(idTo, true);
            switch (RelationStatus.getByStatus(action)) {
                case NO_RELATION:
                    accountService.createFriendRequest(from, to);
                    return "Recall request:" + to.getId() + ':' + RelationStatus.REQUEST_FROM_VIEWPOINT_TO_RENDERING.getStatus();
                case MUTUAL_FRIENDS:
                    accountService.deleteFriend(from, to);
                    return "Add friend:" + to.getId() + ':' + RelationStatus.NO_RELATION.getStatus();
                case REQUEST_FROM_RENDERING_TO_VIEWPOINT:
                    if (doubleButtonStatus != null && doubleButtonStatus.equals("accept")) {
                        accountService.acceptFriendRequest(to, from);
                        return "Delete friend:" + to.getId() + ':' + RelationStatus.MUTUAL_FRIENDS.getStatus();
                    } else {
                        accountService.rejectFriendRequest(to, from);
                        return "Add friend:" + to.getId() + ':' + RelationStatus.NO_RELATION.getStatus();
                    }
                case REQUEST_FROM_VIEWPOINT_TO_RENDERING:
                    accountService.rejectFriendRequest(from, to);
                    return "Add friend:" + to.getId() + ':' + RelationStatus.NO_RELATION.getStatus();
                default:
                    return "Unexpected action";
            }
        } catch (ServiceException e) {
            return "Error occurred: " + e.getMessage();
        }
    }
}
