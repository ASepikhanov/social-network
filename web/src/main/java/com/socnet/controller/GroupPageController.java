package com.socnet.controller;

import com.socnet.Account;
import com.socnet.Group;
import com.socnet.GroupRole;
import com.socnet.ServiceException;
import com.socnet.message.DestinationEntity;
import com.socnet.message.Message;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GroupPageController extends AbstractController {

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public String getGroup(@RequestParam(value = "id", required = false) Integer attributeId,
                           HttpSession session, Model model) {
        Account viewer = getCurrentAccount();
        if (attributeId != null) {
            try {
                Group requestedGroup = groupService.getGroupById(attributeId);
                model.addAttribute("group", requestedGroup);

                long actualSubscriberCount = requestedGroup.getSubscriberRoles().entrySet().stream()
                        .filter(e -> e.getValue() != GroupRole.PENDING).count();
                List<Message> groupWall = messageService.getGroupWall(requestedGroup);

                model.addAttribute("subscriberCount", actualSubscriberCount);
                model.addAttribute("subscriberMap", requestedGroup.getSubscriberRoles());

                GroupRole viewerRole = requestedGroup.getSubscriberRoles().get(viewer.getId());
                if (viewerRole != null && viewerRole != GroupRole.PENDING) {
                    model.addAttribute("groupWall", groupWall);
                }

                Message groupDestinationMessage = new Message();
                groupDestinationMessage.setDestinationId(requestedGroup.getGroupId());
                groupDestinationMessage.setDestinationEntity(DestinationEntity.GROUP);
                model.addAttribute("newWallMessage", groupDestinationMessage);

                session.setAttribute("lastVisitedGroup", requestedGroup.getGroupId());
                return "/auth-pages/group";
            } catch (ServiceException e) {
                model.addAttribute("message", e.getMessage());
                return ERROR_PAGE;
            }
        } else {
            return "redirect:/groups";
        }
    }

    @RequestMapping(value = "/subscribers", method = RequestMethod.GET)
    public ModelAndView getSubscribersList(@RequestParam("id") Integer attributeId) {
        try {
            Group group = groupService.getGroupById(attributeId);
            ModelAndView modelAndView = new ModelAndView("/auth-pages/subscribers");
            modelAndView.addObject("group", group);
            return modelAndView;
        } catch (ServiceException e) {
            return handleServiceException(e);
        }
    }

    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public ModelAndView getGroupList(@RequestParam(value = "id", required = false) Integer attributeId) {
        Account requester = getCurrentAccount();
        if (attributeId != null) {
            try {
                Account account = accountService.getAccountById(attributeId);
                Collection<Group> groups = new ArrayList<>();
                for (Group group : account.getGroups()) {
                    groups.add(groupService.getGroupById(group.getGroupId()));
                }
                ModelAndView modelAndView = new ModelAndView("/auth-pages/groups");
                modelAndView.addObject("accountName", account.getName());
                modelAndView.addObject("groups", groups);
                return modelAndView;
            } catch (ServiceException e) {
                return handleServiceException(e);
            }
        } else {
            return new ModelAndView("redirect:groups?id=" + requester.getId());
        }
    }

    @RequestMapping(value = "/group_mod", method = RequestMethod.GET)
    public ModelAndView getGroupModerationPage(@RequestParam("id") Integer attributeId) {
        try {
            Group group = groupService.getGroupById(attributeId);
            ModelAndView modelAndView = new ModelAndView("/auth-pages/group-mod");
            modelAndView.addObject("group", group);
            return modelAndView;
        } catch (ServiceException e) {
            return handleServiceException(e);
        }
    }

    @RequestMapping(value = "/group_create", method = RequestMethod.GET)
    public String getGroupCreatePage(Model model) {
        Group group = new Group();
        model.addAttribute("groupToCreate", group);
        return "/auth-pages/group-creation";
    }

    @RequestMapping(value = "/group_create", method = RequestMethod.POST)
    public String createGroup(@ModelAttribute("groupToCreate") Group formReadGroup,
                              @RequestParam("groupAvatar") MultipartFile avatarFile,
                              Model model) throws IOException {
        Account creator = getCurrentAccount();
        formReadGroup.setCreationDate(LocalDate.now());
        try {
            Integer generatedId = groupService.save(formReadGroup, creator, avatarFile.getInputStream());
            return "redirect:/group?id=" + generatedId;
        } catch (ServiceException e) {
            model.addAttribute("message", e);
            return ERROR_PAGE;
        }
    }
}
