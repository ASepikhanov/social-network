package com.socnet.controller;

import com.socnet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AbstractController {

    protected static final String ERROR_PAGE = "error";
    protected static final Logger logger = LoggerFactory.getLogger(AbstractController.class);
    private static final SimpleGrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority("ROLE_ADMIN");
    @Autowired
    protected AccountService accountService;
    @Autowired
    protected GroupService groupService;
    @Autowired
    protected ImageService imageService;
    @Autowired
    protected MessageService messageService;

    protected ModelAndView handleServiceException(ServiceException e) {
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("message", e.getMessage());
        return modelAndView;
    }

    protected Account getCurrentAccount() {
        return ((AccountPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getAccount();
    }

    protected boolean isAccountAdmin() {
        return ((AccountPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal()).getAuthorities().contains(ROLE_ADMIN);
    }
}
