package com.socnet.controller;

import com.socnet.Account;
import com.socnet.ServiceException;
import com.socnet.message.DestinationEntity;
import com.socnet.message.Message;

import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AccountPageController extends AbstractController {

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public String getAccountPage(@RequestParam(value = "id", required = false) Integer id,
                                 HttpSession session, Model model) {
        Account viewPointAccount = getCurrentAccount();
        if (id != null) {
            try {
                Account requestAccount;
                if (viewPointAccount.getId().equals(id)) {
                    requestAccount = viewPointAccount;
                } else {
                    requestAccount = accountService.getAccountById(id, true);
                }
                List<Message> accountWall = messageService.getAccountWall(requestAccount);

                model.addAttribute("account", requestAccount);
                model.addAttribute("relationStatus", detectRelation(viewPointAccount, requestAccount));
                model.addAttribute("accountWall", accountWall);
                model.addAttribute("posts", accountWall.size());

                Message accountDestinationMessage = new Message();
                accountDestinationMessage.setDestinationId(requestAccount.getId());
                accountDestinationMessage.setDestinationEntity(DestinationEntity.ACCOUNT);
                model.addAttribute("newWallMessage", accountDestinationMessage);

                session.setAttribute("lastVisitedAccount", requestAccount.getId());
                return "/auth-pages/account";
            } catch (ServiceException e) {
                model.addAttribute("message", e.getMessage());
                return ERROR_PAGE;
            }
        } else {
            return "redirect:/account?id=" + viewPointAccount.getId();
        }
    }

    /**
     * 4 codes of relations are prepared here to be passed to jsp as "relationStatus" attribute of request.
     * 0: relation is not present -> send friend request button to draw
     * 1: current user to user on page request -> recall friend request button
     * 2: user on page to current user request -> reject friend request button
     * 3: mutual friends -> delete friend button
     */
    private int detectRelation(Account viewPointAccount, Account requestAccount) {
        if (viewPointAccount.getFriends().contains(requestAccount.getId())) {
            return RelationStatus.MUTUAL_FRIENDS.getStatus();
        } else if (viewPointAccount.getOutFriendRequests().contains(requestAccount.getId())) {
            return RelationStatus.REQUEST_FROM_VIEWPOINT_TO_RENDERING.getStatus();
        } else if (requestAccount.getOutFriendRequests().contains(viewPointAccount.getId())) {
            return RelationStatus.REQUEST_FROM_RENDERING_TO_VIEWPOINT.getStatus();
        } else {
            return RelationStatus.NO_RELATION.getStatus();
        }
    }
}
