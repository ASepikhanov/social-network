package com.socnet.controller.viewdto;

public class MessageViewDto {

    private String text;
    private boolean image;
    private Integer id;
    private Integer destinationId;

    public MessageViewDto(String text, boolean image, Integer id) {
        this.text = text;
        this.image = image;
        this.id = id;
    }

    public MessageViewDto() {
    }

    public Integer getDestinationId() {
        return destinationId;
    }

    public MessageViewDto setDestinationId(Integer destinationId) {
        this.destinationId = destinationId;
        return this;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isImage() {
        return image;
    }

    public void setImage(boolean image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
