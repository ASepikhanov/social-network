package com.socnet.controller.viewdto;

import com.neovisionaries.i18n.CountryCode;
import com.socnet.Account;
import com.socnet.Phone;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

import com.thoughtworks.xstream.converters.ConversionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccountViewDto {

    private static final Logger logger = LoggerFactory.getLogger(AccountViewDto.class);
    private Integer id;
    private String name;
    private String surname;
    private String middleName;
    private String email;
    private CountryCode country;
    private String city;
    private Boolean avatar;
    private String additionalInfo;
    private Long icq;
    private String skype;
    private LocalDate birthDate;
    private Collection<Phone> phones = new ArrayList<>();

    public AccountViewDto(Integer id, String name, String surname) {
        this.id = id;
        this.name = name;
        this.surname = surname;
    }

    public AccountViewDto() {
    }

    public static Account convertToDomain(AccountViewDto dto) {
        try {
            Account resultAccount = new Account();
            for (Field dtoField : AccountViewDto.class.getDeclaredFields()) {
                String fieldName = dtoField.getName();
                if (!"logger phones".contains(fieldName)) {
                    Field correspondingDomainField = Account.class.getDeclaredField(fieldName);
                    correspondingDomainField.setAccessible(true);
                    Object dtoFieldValue = dtoField.get(dto);
                    correspondingDomainField.set(resultAccount, dtoFieldValue);
                }
            }

            Account owner = new Account(dto.getId());
            Collection<Phone> resultPhones = new ArrayList<>();
            for (Phone domainPhone : dto.phones) {
                resultPhones.add(new Phone(owner, domainPhone.getPhoneNum(), domainPhone.getType()));
            }
            resultAccount.setPhones(resultPhones);
            return resultAccount;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.error(e.getMessage());
            throw new ConversionException("Unable to convert DTO to domain model!", e);
        }
    }

    public static AccountViewDto convertToDto(Account account) {
        try {
            AccountViewDto resultAccount = new AccountViewDto();
            for (Field dtoField : AccountViewDto.class.getDeclaredFields()) {
                String fieldName = dtoField.getName();
                if (!"logger phones".contains(fieldName)) {
                    Field correspondingDomainField = Account.class.getDeclaredField(fieldName);
                    correspondingDomainField.setAccessible(true);
                    Object domainFieldValue = correspondingDomainField.get(account);
                    dtoField.setAccessible(true);
                    dtoField.set(resultAccount, domainFieldValue);
                }
            }

            Account owner = new Account(account.getId());
            for (Phone domainPhone : account.getPhones()) {
                resultAccount.phones.add(new Phone(owner, domainPhone.getPhoneNum(), domainPhone.getType()));
            }
            return resultAccount;
        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.error(e.getMessage());
            throw new ConversionException("Unable to convert account to DTO!", e);
        }
    }

    public Integer getId() {

        return id;
    }

    public AccountViewDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public AccountViewDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public AccountViewDto setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public AccountViewDto setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public AccountViewDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public CountryCode getCountry() {
        return country;
    }

    public AccountViewDto setCountry(CountryCode country) {
        this.country = country;
        return this;
    }

    public String getCity() {
        return city;
    }

    public AccountViewDto setCity(String city) {
        this.city = city;
        return this;
    }

    public Boolean getAvatar() {
        return avatar;
    }

    public AccountViewDto setAvatar(Boolean avatar) {
        this.avatar = avatar;
        return this;
    }

    public Collection<Phone> getPhones() {
        return phones;
    }

    public AccountViewDto setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public AccountViewDto setBirthDate(String birthDate) {
        this.birthDate = birthDate == null ? null : LocalDate.parse(birthDate);
        return this;
    }

    public Long getIcq() {
        return icq;
    }

    public AccountViewDto setIcq(Long icq) {
        this.icq = icq;
        return this;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public AccountViewDto setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }

    public String getSkype() {
        return skype;
    }

    public AccountViewDto setSkype(String skype) {
        this.skype = skype;
        return this;
    }
}
