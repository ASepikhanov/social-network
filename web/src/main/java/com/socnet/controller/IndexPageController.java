package com.socnet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexPageController extends AbstractController {

    @RequestMapping("index")
    public String showIndexPage() {
        return "index";
    }
}
