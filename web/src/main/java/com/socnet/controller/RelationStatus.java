package com.socnet.controller;

public enum RelationStatus {

    NO_RELATION(0),
    REQUEST_FROM_VIEWPOINT_TO_RENDERING(1),
    REQUEST_FROM_RENDERING_TO_VIEWPOINT(2),
    MUTUAL_FRIENDS(3);

    private int status;

    RelationStatus(int status) {
        this.status = status;
    }

    public static RelationStatus getByStatus(int status) {
        switch (status) {
            case 0:
                return NO_RELATION;
            case 1:
                return REQUEST_FROM_VIEWPOINT_TO_RENDERING;
            case 2:
                return REQUEST_FROM_RENDERING_TO_VIEWPOINT;
            case 3:
                return MUTUAL_FRIENDS;
            default:
                throw new IllegalArgumentException();
        }
    }

    public int getStatus() {
        return status;
    }
}
