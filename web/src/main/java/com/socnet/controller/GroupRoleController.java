package com.socnet.controller;

import com.socnet.Account;
import com.socnet.Group;
import com.socnet.GroupRole;
import com.socnet.ServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class GroupRoleController extends AbstractController {

    @RequestMapping(value = "/GroupRoleController", method = RequestMethod.POST)
    @ResponseBody
    public String requestRole(@RequestParam("group_id") Integer groupId,
                              @RequestParam("account_id") Integer subjectAccountId,
                              @RequestParam("role_code") Integer requestCode,
                              @RequestParam(value = "downgrade_from_moderator", required = false) String downgradeFromModerator) {
        Account approver = getCurrentAccount();
        try {
            Group group = groupService.getGroupById(groupId);
            Account subjectAccount = accountService.getAccountById(subjectAccountId);
            GroupRole requestRole = GroupRole.getRole(requestCode);

            switch (requestRole) {
                case PENDING:
                    groupService.addGroupApplyRequest(group, approver);
                    return "Recall apply:" + group.getGroupId() + ':' + approver.getId() + ':'
                            + GroupRole.NOT_IN_GROUP.getCode();
                case SUBSCRIBER:
                    groupService.addSubscriberToGroup(group, subjectAccount, approver);
                    if (downgradeFromModerator == null) {
                        return "Remove subscriber:" + group.getGroupId() + ':' + subjectAccount.getId() + ':'
                                + GroupRole.NOT_IN_GROUP.getCode();
                    } else {
                        return "Make moderator:" + group.getGroupId() + ':' + subjectAccount.getId() + ':'
                                + GroupRole.MODERATOR.getCode();
                    }
                case MODERATOR:
                    groupService.addModeratorToGroup(group, subjectAccount, approver);
                    return "Remove from moderators:" + group.getGroupId() + ':' + subjectAccount.getId() + ':'
                            + GroupRole.SUBSCRIBER.getCode();
                case NOT_IN_GROUP:
                    groupService.removeSubscriber(group, subjectAccount, approver);
                    if (subjectAccount.getId().equals(approver.getId())) {
                        return "Apply:" + group.getGroupId() + ':' + subjectAccount.getId() + ':'
                                + GroupRole.PENDING.getCode();
                    }
                    return "";
                case ADMIN:
                    throw new ServiceException("Group can have only 1 admin");
                default:
                    throw new ServiceException("Undefined requested group role");
            }
        } catch (ServiceException e) {
            return "Error is " + e.getMessage() + "::";
        }
    }
}
