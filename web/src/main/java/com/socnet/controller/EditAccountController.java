package com.socnet.controller;

import com.socnet.*;
import com.socnet.controller.viewdto.AccountViewDto;
import com.socnet.validation.PhoneValidator;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class EditAccountController extends AbstractController {

    @Autowired
    private PhoneValidator phoneValidator;

    @RequestMapping(value = "/edit_account", method = RequestMethod.GET)
    public String showEditAccountForm(@RequestParam(value = "id", required = false) Integer attributeId,
                                      Model model) {
        Account currentAccount = getCurrentAccount();
        if (attributeId == null) {
            AccountViewDto dto = AccountViewDto.convertToDto(currentAccount);
            model.addAttribute("updatingAccount", dto);
            return "/auth-pages/edit-account";
        } else if (isAccountAdmin()) {
            try {
                model.addAttribute("updatingAccount",
                        AccountViewDto.convertToDto(accountService.getAccountById(attributeId)));
                return "/auth-pages/edit-account";
            } catch (ServiceException e) {
                model.addAttribute("message", e.getMessage());
                return ERROR_PAGE;
            }
        } else {
            model.addAttribute("message", "you should be admin to edit other accounts");
            return ERROR_PAGE;
        }
    }

    @RequestMapping(value = "/edit_account", method = RequestMethod.POST)
    public String doUpdate(@ModelAttribute("updatingAccount") AccountViewDto formReadAccount,
                           @RequestParam("avatarFile") MultipartFile avatarFile,
                           @RequestParam(value = "birthDateString", required = false) String birthDate,
                           HttpServletRequest req,
                           Model model) throws IOException {
        formReadAccount.setBirthDate(birthDate);
        Account currentAccount = getCurrentAccount();
        try {
            if (formReadAccount.getId().equals(currentAccount.getId())) {
                Account updated = performUpdate(req, AccountViewDto.convertToDomain(formReadAccount),
                        currentAccount, avatarFile);
                ((AccountPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                        .setAccount(updated);
                return "redirect:/account?id=" + updated.getId();
            } else if (!isAccountAdmin()) {
                model.addAttribute("message", "only admin can edit other accounts");
                return ERROR_PAGE;
            } else {
                Account updated = performUpdate(req, AccountViewDto.convertToDomain(formReadAccount),
                        accountService.getAccountById(formReadAccount.getId(), true), avatarFile);
                logger.info("Admin {} updated account {}", currentAccount.getId(), formReadAccount.getId());
                return "redirect:/account?id=" + updated.getId();
            }
        } catch (ServiceException e) {
            model.addAttribute("message", e.getMessage());
            return ERROR_PAGE;
        }
    }

    private Account performUpdate(HttpServletRequest req, Account formReadAccount, Account currentAccount,
                                  MultipartFile avatarFile)
            throws ServiceException, IOException {
        formReadAccount.setGroups(currentAccount.getGroups());
        List<Phone> readPhones = fillPhones(req, formReadAccount);
        phoneValidator.validate(readPhones);

        if (formReadAccount.hasAvatar()) {
            InputStream avatar = avatarFile.getInputStream();
            if (avatar.available() > 0) {
                imageService.setAccountAvatar(formReadAccount.getId(), avatar);
            }
        }
        Account updatedAccount = accountService.editAccount(formReadAccount, readPhones);
        updatedAccount.setFriends(currentAccount.getFriends());
        updatedAccount.setOutFriendRequests(currentAccount.getOutFriendRequests());
        updatedAccount.setInFriendRequests(currentAccount.getInFriendRequests());
        return updatedAccount;
    }

    private List<Phone> fillPhones(HttpServletRequest req, Account updatingAccount) {
        List<Phone> phones = new ArrayList<>();
        Map<String, PhoneType> types = new HashMap<>();
        Collection<String> phoneNumbers = new ArrayList<>();
        for (Map.Entry entry : req.getParameterMap().entrySet()) {
            String currentRequestKey = (String) entry.getKey();
            if (currentRequestKey.matches("type[0-9]+")) {
                PhoneType currentType = PhoneType.getType(Integer.parseInt(req.getParameter(currentRequestKey)));
                String commonElementIdSuffix = currentRequestKey.substring(4, currentRequestKey.length());
                String typeMapping = req.getParameter("phone" + commonElementIdSuffix);
                types.put(typeMapping, currentType);

            } else if (currentRequestKey.matches("phone[0-9]+")) {
                phoneNumbers.add(req.getParameter(currentRequestKey));
            }
        }
        for (String phoneNumber : phoneNumbers) {
            Phone phone = new Phone(updatingAccount, phoneNumber, types.get(phoneNumber));
            phones.add(phone);
        }
        return phones;
    }
}
