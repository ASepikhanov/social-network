package com.socnet.controller;

import com.socnet.Account;
import com.socnet.ServiceException;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class RegistrationController extends AbstractController {

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String prepareRegisterPage(Model model) {
        Account account = new Account();
        account.setCountry("RU");
        model.addAttribute("registerAccount", account);
        return "registration";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute("registerAccount") Account account,
                           @RequestParam("password") String password,
                           @RequestParam("avatarFile") MultipartFile avatarFile,
                           Model model)
            throws IOException {
        account.setRegistrationDate(LocalDate.now());
        InputStream avatar = avatarFile.getInputStream();
        try {
            accountService.save(account, avatar, password);
            model.addAttribute("username", account.getName());
            return "registrationSuccess";
        } catch (ServiceException e) {
            model.addAttribute("registrationFailMessage", e.getMessage());
            return "registrationFailed";
        }
    }
}
