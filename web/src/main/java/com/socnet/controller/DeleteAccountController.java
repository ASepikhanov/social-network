package com.socnet.controller;

import com.socnet.ServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DeleteAccountController extends AbstractController {

    @RequestMapping(value = "delete_account", method = RequestMethod.POST)
    @ResponseBody
    public String deleteAccount(@RequestParam("id") Integer id) {
        try {
            accountService.deleteAccount(accountService.getAccountById(id));
            return "OK";
        } catch (ServiceException e) {
            return e.getMessage();
        }
    }
}
