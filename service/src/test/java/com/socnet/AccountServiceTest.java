package com.socnet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.junit.Test;

import java.time.LocalDate;
import java.util.*;

public class AccountServiceTest extends ServiceTest {

    @Test
    public void testCreateFriendRequest() {
        Account from = new Account(1);
        Account to = new Account(2);
        accountService.createFriendRequest(from, to);
        assertEquals(from.getOutFriendRequests(), Collections.singletonList(2));
        assertEquals(to.getInFriendRequests(), Collections.singletonList(1));
    }

    @Test
    public void testAcceptFriendRequest() {
        Account from = new Account(1);
        Account to = new Account(2);
        from.addOutFriendRequest(2);
        to.addInFriendRequest(1);
        accountService.acceptFriendRequest(from, to);
        assertEquals(from.getOutFriendRequests(), new ArrayList<>());
        assertEquals(to.getInFriendRequests(), new ArrayList<>());
        assertEquals(from.getFriends(), Collections.singletonList(2));
        assertEquals(to.getFriends(), Collections.singletonList(1));
    }

    @Test
    public void testRejectFriendRequest() {
        Account from = new Account(1);
        Account to = new Account(2);
        from.addOutFriendRequest(2);
        to.addInFriendRequest(1);
        accountService.rejectFriendRequest(from, to);
        assertEquals(from.getOutFriendRequests(), new ArrayList<>());
        assertEquals(to.getInFriendRequests(), new ArrayList<>());
    }

    @Test
    public void testDeleteFriend() {
        Account acc1 = new Account(1);
        Account acc2 = new Account(2);
        acc1.addFriend(2);
        acc2.addFriend(1);
        accountService.deleteFriend(acc1, acc2);
        assertEquals(acc1.getFriends(), new ArrayList<>());
        assertEquals(acc2.getFriends(), new ArrayList<>());
    }

    @Test
    public void testAddAdmin() {
        Account admin = new Account(1);
        Account notAdmin = new Account(2);
        Account target = new Account(3);
        when(accountService.getAdminList()).thenReturn(Collections.singletonList(new Account(1)));
        try {
            accountService.addAdmin(admin, target.getId());
        } catch (ServiceException e) {
            fail();
        }

        try {
            accountService.addAdmin(notAdmin, 10);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Access denied");
        }
    }

    @Test
    public void testRemoveAdmin() {
        Collection<Account> admins = Arrays.asList(new Account(1), new Account(2), new Account(3));
        when(accountService.getAdminList()).thenReturn(admins);
        Account admin1 = new Account(1);
        Account admin2 = new Account(2);
        try {
            accountService.removeAdmin(admin1, admin2.getId());
        } catch (ServiceException e) {
            fail();
        }

        try {
            accountService.removeAdmin(admin1, 100);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Both requester and target should be admins to perform remove");
        }

        try {
            accountService.removeAdmin(admin1, admin1.getId());
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Admin cannot delete himself");
        }
    }

    @Test
    public void testEditAccount() {
        PhoneDao phoneDao = getPhoneDao();
        AccountDao accountDao = getAccountDao();
        Account account = new Account.Builder(1, "a", "b", "mail@mail.com", LocalDate.now()).build();

        when(phoneDao.getPhoneByPhoneNum("")).thenReturn(new Phone(new Account(3), "", 1));
        when(phoneDao.getPhoneByPhoneNum("new")).thenReturn(null);
        when(accountDao.updateAccount(any())).thenReturn(new Account(1));

        try {
            accountService.editAccount(account, Collections.singletonList(new Phone(new Account(1), "", 1)));
            fail();
        } catch (ServiceException e) {
            assertEquals("One of entered phones belongs to somebody else", e.getMessage());
        }
        assertEquals(account.getPhones(), new ArrayList<>());

        try {
            when(accountDao.getAccountById(1, true)).thenReturn(account);
            accountService.editAccount(account, Collections.singletonList(new Phone(new Account(1), "new", 1)));
        } catch (ServiceException e) {
            fail();
        }
    }
}
