package com.socnet;

import static com.socnet.GroupServiceTest.generateCommonGroup;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import com.socnet.message.Message;
import org.junit.Test;

public class MessageServiceTest extends ServiceTest {

    @Test
    public void testCreateGroupMessage() {
        Group group = generateCommonGroup();
        Message message1 = new Message.Builder().author(new Account(1)).build();
        try {
            messageService.createGroupMessage(message1, group);
        } catch (ServiceException e) {
            fail();
        }

        Message message2 = new Message.Builder().author(new Account(9)).build();
        try {
            messageService.createGroupMessage(message2, group);
        } catch (ServiceException e) {
            fail();
        }

        Message message3 = new Message.Builder().author(new Account(100)).build();
        try {
            messageService.createGroupMessage(message3, group);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Only group participants can post messages on group wall");
        }
    }

    @Test
    public void testDeleteMessageFromAccountWall() {
        Message message = new Message.Builder().author(new Account(1)).destinationId(2).build();
        try {
            messageService.deleteMessageFromAccountWall(message, new Account(1));
        } catch (ServiceException e) {
            fail();
        }
        try {
            messageService.deleteMessageFromAccountWall(message, new Account(2));
        } catch (ServiceException e) {
            fail();
        }
        try {
            messageService.deleteMessageFromAccountWall(message, new Account(3));
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "You cannot delete this message - you should be it's author or " +
                    "owner of wall where it was posted");
        }
    }

    @Test
    public void testDeleteMessageFromGroupWall() {
        Group group = generateCommonGroup();
        Message message = new Message.Builder().author(new Account(9)).build();
        try {
            messageService.deleteMessageFromGroupWall(message, new Account(1), group);
        } catch (ServiceException e) {
            fail();
        }
        try {
            messageService.deleteMessageFromGroupWall(message, new Account(9), group);
        } catch (ServiceException e) {
            fail();
        }
        try {
            messageService.deleteMessageFromGroupWall(message, new Account(8), group);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Only group admin, moderators or message author can delete" +
                    " messages from group wall");
        }
    }
}
