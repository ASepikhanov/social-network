package com.socnet;

import org.junit.Test;

import static org.junit.Assert.*;

public class GroupServiceTest extends ServiceTest {

    public static Group generateCommonGroup() {
        Group group = new Group();
        group.setGroupId(0);
        group.addSubscriberRole(1, GroupRole.ADMIN);
        group.addSubscriberRole(2, GroupRole.MODERATOR);
        group.addSubscriberRole(3, GroupRole.SUBSCRIBER);
        group.addSubscriberRole(4, GroupRole.PENDING);
        group.addSubscriberRole(5, GroupRole.PENDING);
        group.addSubscriberRole(6, GroupRole.PENDING);
        group.addSubscriberRole(7, GroupRole.SUBSCRIBER);
        group.addSubscriberRole(8, GroupRole.SUBSCRIBER);
        group.addSubscriberRole(9, GroupRole.SUBSCRIBER);
        return group;
    }

    @Test
    public void testAddSubscriberToGroup() {
        Group group = generateCommonGroup();
        Account admin = new Account(1);
        try {
            assertTrue(groupService.addSubscriberToGroup(group, new Account(4), admin));
            assertTrue(group.getSubscriberRoles().containsKey(4));
        } catch (ServiceException e) {
            fail();
        }

        String firstApplyMsg = "Account should first apply to group before being accepted";
        try {
            groupService.addSubscriberToGroup(group, new Account(4), admin);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), firstApplyMsg);
        }

        try {
            groupService.addSubscriberToGroup(group, new Account(10), admin);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), firstApplyMsg);
        }

        try {
            groupService.addSubscriberToGroup(group, new Account(5), new Account(2));
            assertTrue(group.getSubscriberRoles().containsKey(5));
        } catch (ServiceException e) {
            fail();
        }

        try {
            groupService.addSubscriberToGroup(group, new Account(6), new Account(100));
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Only moderator or admin can approve group apply");
        }
    }

    @Test
    public void testRemoveSubscriber() {
        Account admin = new Account(1);
        Group group = generateCommonGroup();
        try {
            groupService.removeSubscriber(group, new Account(3), admin);
            assertFalse(group.getSubscribers().contains(new Account(3)));
        } catch (ServiceException e) {
            fail();
        }

        try {
            groupService.removeSubscriber(group, new Account(7), new Account(7));
            assertFalse(group.getSubscribers().contains(new Account(7)));
        } catch (ServiceException e) {
            fail();
        }

        String removeSubscriberErrorMsg = "Only admin or moderator can remove others (except admin) from group.";
        try {
            groupService.removeSubscriber(group, admin, admin);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), removeSubscriberErrorMsg);
        }

        try {
            groupService.removeSubscriber(group, new Account(8), new Account(9));
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), removeSubscriberErrorMsg);
        }
    }

    @Test
    public void testAddModerator() {
        Group group = generateCommonGroup();
        Account admin = new Account(1);
        try {
            groupService.addModeratorToGroup(group, new Account(3), admin);
        } catch (ServiceException e) {
            fail();
        }

        try {
            groupService.addModeratorToGroup(group, new Account(4), admin);
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "To become moderator account should be subscriber first");
        }

        try {
            groupService.addModeratorToGroup(group, new Account(4), new Account(2));
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Only group admin can set moderators");
        }
    }

    @Test
    public void testAddGroupApplyRequest() {
        Group group = generateCommonGroup();
        try {
            groupService.addGroupApplyRequest(group, new Account(100));
        } catch (ServiceException e) {
            fail();
        }

        try {
            groupService.addGroupApplyRequest(group, new Account(2));
        } catch (ServiceException e) {
            assertEquals(e.getMessage(), "Account 2 is already in this group");
        }
    }
}
