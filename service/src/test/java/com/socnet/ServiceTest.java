package com.socnet;

import com.socnet.validation.AccountValidator;
import com.socnet.validation.ValidationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Field;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:service-test.xml"})
public class ServiceTest {

    private static final String DAO_FIELD_NAME = "dao";
    private static final Logger logger = LoggerFactory.getLogger(ServiceTest.class);
    @Autowired
    protected AccountService accountService;
    @Autowired
    protected GroupService groupService;
    @Autowired
    protected MessageService messageService;

    @Before
    public void injectMocks() {
        try {
            AccountValidator validator = mock(AccountValidator.class);
            when(validator.validateUpdating(any())).thenReturn(true);
        } catch (ValidationException e) {
            logger.info("Validation didn't pass: ", e);
        }
    }

    @Test
    public void testServiceSetup() {
        assertNotNull(accountService);
        assertNotNull(groupService);
        assertNotNull(messageService);
    }

    protected AccountDao getAccountDao() {
        return getDao(accountService, DAO_FIELD_NAME);
    }

    protected PhoneDao getPhoneDao() {
        return getDao(accountService, "phoneDao");
    }

    @SuppressWarnings("unchecked")
    private <T> T getDao(Object service, String fieldName) {
        Field f;
        try {
            f = service.getClass().getDeclaredField(fieldName);
            f.setAccessible(true);
            return (T) f.get(service);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.info(e.toString());
        }
        return null;
    }
}
