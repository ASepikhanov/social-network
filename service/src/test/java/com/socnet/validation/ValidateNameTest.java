package com.socnet.validation;

import com.socnet.Account;
import com.socnet.validation.required.ValidateName;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ValidateNameTest {

    @Test
    public void testValidateName() {
        Account correct = new Account(1);
        correct.setName("aAaAa   a ada");
        String correctName = "dsadsa dsadsadsa";
        correct.setSurname(correctName);
        assertTrue(new ValidateName().doCheck(correct));

        Account incorrect = new Account(1);
        incorrect.setName("aAaAa1   a ada");
        incorrect.setSurname("b");
        assertFalse(new ValidateName().doCheck(incorrect));

        Account correct2 = new Account(1);
        correct2.setName("aAaAa   a ada");
        correct2.setSurname(correctName);
        correct2.setMiddleName(correctName);
        assertTrue(new ValidateName().doCheck(correct2));
    }
}
