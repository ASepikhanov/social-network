package com.socnet.validation;

import com.socnet.Account;
import com.socnet.validation.optional.ValidateIcq;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ValidateIcqTest {

    @Test
    public void testValidateIcq() {
        Account correct = new Account(1);
        correct.setIcq((long) 12345678);
        assertTrue(new ValidateIcq().doCheck(correct));

        Account incorrect = new Account(1);
        incorrect.setIcq(1234_1234_1234_1234_1L);
        assertFalse(new ValidateIcq().doCheck(incorrect));
    }
}
