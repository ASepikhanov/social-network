package com.socnet.validation;

import com.socnet.Account;
import com.socnet.validation.optional.ValidateDate;
import org.junit.Test;

import java.time.LocalDate;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ValidateDateTest {

    @Test
    public void testValidateDate() {
        Account correct = new Account(1);
        correct.setBirthDate(LocalDate.of(2001, 11, 11));
        assertTrue(new ValidateDate().doCheck(correct));

        Account correct2 = new Account(1);
        correct2.setBirthDate(LocalDate.now());
        assertTrue(new ValidateDate().doCheck(correct2));

        Account incorrect = new Account(1);
        incorrect.setBirthDate(LocalDate.of(2020, 11, 11));
        assertFalse(new ValidateDate().doCheck(incorrect));
    }
}
