package com.socnet.validation;

import com.socnet.Account;
import com.socnet.validation.optional.ValidateSkype;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ValidateSkypeTest {

    @Test
    public void doTest() {
        ValidateSkype validator = new ValidateSkype();
        Account correct = new Account(1);
        correct.setSkype("dasdsadsa");
        assertTrue(validator.doCheck(correct));

        Account incorrect = new Account(1);
        incorrect.setSkype("3dasdasd");
        assertFalse(validator.doCheck(incorrect));
    }
}
