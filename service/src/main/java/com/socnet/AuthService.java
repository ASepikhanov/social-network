package com.socnet;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AuthService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger("main");

    private AccountDao accountDao;

    public AuthService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        logger.debug("username: {}", s);
        Account account = accountDao.getAccountByMail(s);
        if (account == null) {
            logger.debug("Account not found");
            throw new UsernameNotFoundException("Auth error");
        }
        logger.debug(account.getPhones().toString());
        logger.debug(account.getGroups().toString());
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        if (accountDao.isAccountAdmin(account.getId())) {
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }
        return new AccountPrincipal(account, authorities);
    }
}