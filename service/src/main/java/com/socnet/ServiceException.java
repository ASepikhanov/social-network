package com.socnet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceException extends Exception {

    private static final Logger logger = LoggerFactory.getLogger("exception");

    public ServiceException(String message) {
        super(message);
        logger.info(message);
    }
}
