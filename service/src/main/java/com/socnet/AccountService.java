package com.socnet;

import com.socnet.validation.AccountValidator;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountService {

    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_USER = "ROLE_USER";
    private static final String LENGTH_PASS_ERROR = "Password should have 6 characters at least";
    private static final int MIN_PWD_LENGTH = 6;
    private AccountDao dao;
    private PhoneDao phoneDao;
    private AccountValidator accountValidator;
    private ImageService imageService;
    @Autowired
    private PasswordEncoder encoder;

    public AccountService(AccountDao accountDao, PhoneDao phoneDao, ImageService imageService) {
        this.dao = accountDao;
        this.phoneDao = phoneDao;
        this.accountValidator = new AccountValidator(this);
        this.imageService = imageService;
    }

    @Secured(ROLE_USER)
    public Account getAccountById(int id) throws ServiceException {
        return getAccountById(id, false);
    }

    @Secured(ROLE_USER)
    public Account getAccountById(int id, boolean initRelations) throws ServiceException {
        Account account = dao.getAccountById(id, initRelations);
        if (account != null) {
            return account;
        } else {
            throw new ServiceException("Account doesn't exist");
        }
    }

    @Secured(ROLE_USER)
    public void updateFriendRequests(Account account) {
        account.setOutFriendRequests(dao.getOutFriendRequests(account.getId()));
        account.setInFriendRequests(dao.getInFriendRequests(account.getId()));
    }

    @Secured(ROLE_ADMIN)
    public Collection<Account> getAdminList() {
        return dao.getAdminList();
    }

    @Secured(ROLE_ADMIN)
    public boolean addAdmin(Account requester, Integer target) throws ServiceException {
        Collection<Account> currentAdmins = dao.getAdminList();
        Collection<Integer> adminIds = currentAdmins.stream().map(Account::getId).collect(Collectors.toList());
        if (adminIds.contains(requester.getId()) && !adminIds.contains(target)) {
            return dao.addAdmin(target);
        } else {
            throw new ServiceException("Access denied");
        }
    }

    @Secured(ROLE_ADMIN)
    public boolean removeAdmin(Account requester, Integer target) throws ServiceException {
        Collection<Account> currentAdmins = dao.getAdminList();
        Collection<Integer> adminIds = currentAdmins.stream().map(Account::getId).collect(Collectors.toList());
        if (adminIds.contains(requester.getId()) && adminIds.contains(target)) {
            if (!(requester.getId().equals(target))) {
                return dao.removeAdmin(target);
            } else {
                throw new ServiceException("Admin cannot delete himself");
            }
        } else {
            throw new ServiceException("Both requester and target should be admins to perform remove");
        }
    }

    public int save(Account account, InputStream avatar, String password) throws ServiceException, IOException {
        if (password.length() < MIN_PWD_LENGTH) {
            throw new ServiceException(LENGTH_PASS_ERROR);
        }
        accountValidator.validate(account);
        account.setPasswordHash(encoder.encode(password));
        if (avatar.available() > 0) {
            account.setAvatar(true);
            dao.createAccount(account);
            imageService.setAccountAvatar(account.getId(), avatar);
        } else {
            dao.createAccount(account);
        }
        return account.getId();
    }

    @Secured(ROLE_USER)
    public Account editAccount(Account updatingAccount, List<Phone> newPhones) throws ServiceException {
        accountValidator.validateUpdating(updatingAccount);
        for (Phone newPhone : newPhones) {
            Phone phoneFromDatabase = phoneDao.getPhoneByPhoneNum(newPhone.getPhoneNum());
            if (phoneFromDatabase != null
                    && !phoneFromDatabase.getAccount().getId().equals(updatingAccount.getId())) {
                throw new ServiceException("One of entered phones belongs to somebody else");
            }
        }
        updatingAccount.setPhones(newPhones);
        return dao.updateAccount(updatingAccount);
    }

    @Secured(ROLE_ADMIN)
    public void deleteAccount(Account account) {
        dao.deleteAccount(account);
    }

    @Secured(ROLE_USER)
    public boolean createFriendRequest(Account from, Account to) {
        boolean result = dao.createFriendRequest(from.getId(), to.getId());
        from.addOutFriendRequest(to.getId());
        to.addInFriendRequest(from.getId());
        return result;
    }

    @Secured(ROLE_USER)
    public boolean acceptFriendRequest(Account from, Account to) {
        boolean result = dao.acceptFriendRequest(from.getId(), to.getId());
        from.deleteOutFriendRequest(to.getId());
        to.deleteInFriendRequest(from.getId());
        from.addFriend(to.getId());
        to.addFriend(from.getId());
        return result;
    }

    @Secured(ROLE_USER)
    public boolean rejectFriendRequest(Account from, Account to) {
        boolean result = dao.rejectFriendRequest(from.getId(), to.getId());
        from.deleteOutFriendRequest(to.getId());
        to.deleteInFriendRequest(from.getId());
        return result;
    }

    @Secured(ROLE_USER)
    public boolean deleteFriend(Account account1, Account account2) {
        boolean result = dao.deleteFriend(account1.getId(), account2.getId());
        account1.deleteFriend(account2.getId());
        account2.deleteFriend(account1.getId());
        return result;
    }

    @Secured(ROLE_USER)
    public List<Account> getAccountsByName(String query) {
        return dao.getAccountsByName(query);
    }

    @Secured(ROLE_USER)
    public Collection<Account> getAccountListByIdList(Collection<Integer> idList) {
        return dao.getAccountListByIdList(idList);
    }

    public boolean isEmailUnique(String email) {
        return dao.isEmailUnique(email);
    }
}
