package com.socnet;

import com.socnet.message.Message;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MessageService {

    private MessageDao dao;
    private ImageService imageService;

    public MessageService(MessageDao messageDao, ImageService imageService) {
        this.dao = messageDao;
        this.imageService = imageService;
    }

    private List<Message> getAccountWall(Integer account) {
        return dao.getAccountWall(account);
    }

    public List<Message> getAccountWall(Account account) {
        return getAccountWall(account.getId());
    }

    public List<Message> getGroupWall(Group group) {
        return dao.getGroupWall(group.getGroupId());
    }

    public Message getMessageById(Integer id) {
        return dao.getMessageById(id);
    }

    public int createWallMessage(Message message, InputStream image) throws IOException {
        Integer generatedId;
        if (image.available() > 0) {
            message.setImage(true);
            generatedId = dao.createMessage(message);
            message.setId(generatedId);
            imageService.setMessageImage(message, image);
        } else {
            message.setImage(false);
            generatedId = dao.createMessage(message);
        }
        return generatedId;
    }

    public int createGroupMessage(Message message, Group destinationGroup) throws ServiceException {
        GroupRole authorRole = destinationGroup.getSubscriberRoles().get(message.getAuthor().getId());
        if (authorRole == GroupRole.PENDING) {
            throw new ServiceException("Only group participants can post messages on group wall");
        } else {
            return dao.createMessage(message);
        }
    }

    public int createPrivateMessage(Message message, InputStream image) throws IOException {
        Integer generatedId;
        if (image.available() > "undefined".length()) {
            message.setImage(true);
            generatedId = dao.createMessage(message);
            message.setId(generatedId);
            imageService.setMessageImage(message, image);
        } else {
            message.setImage(false);
            generatedId = dao.createMessage(message);
        }
        return generatedId;
    }

    public void deleteMessageFromAccountWall(Message subjectMessage, Account requester) throws ServiceException {
        boolean deletePermit = requester.getId().equals(subjectMessage.getAuthor().getId())
                || requester.getId().equals(subjectMessage.getDestinationId());
        if (deletePermit) {
            dao.deleteMessage(subjectMessage);
        } else {
            throw new ServiceException("You cannot delete this message - you should be it's author or "
                    + "owner of wall where it was posted");
        }
    }

    public List<Message> getPrivateMessages(Account account1, Account account2) {
        return dao.getPrivateMessages(account1.getId(), account2.getId());
    }

    public List<Message> getAccountConversationList(Account account) {
        return dao.getAccountConversationList(account.getId());
    }

    public void deleteMessageFromGroupWall(Message subjectMessage, Account requester, Group targetGroup)
            throws ServiceException {
        GroupRole requesterRole = targetGroup.getSubscriberRoles().get(requester.getId());
        boolean deletePermit = requesterRole == GroupRole.MODERATOR || requesterRole == GroupRole.ADMIN
                || requester.getId().equals(subjectMessage.getAuthor().getId());
        if (deletePermit) {
            dao.deleteMessage(subjectMessage);
        } else {
            throw new ServiceException("Only group admin, moderators or message author can delete messages from"
                    + " group wall");
        }
    }
}
