package com.socnet;

import com.socnet.message.DestinationEntity;
import com.socnet.message.Message;

import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.stereotype.Service;

@Service
public class ImageService {

    private ImageDao dao;

    public ImageService(ImageDao imageDao) {
        this.dao = imageDao;
    }

    public void writeAccountAvatar(Integer accountId, OutputStream out) {
        dao.write(out, "account", accountId);
    }

    public void writeGroupAvatar(Integer groupId, OutputStream out) {
        dao.write(out, "group_avatar", groupId);
    }

    public void writeMessageImage(Message message, OutputStream out, Account requester) {
        if (message.getDestinationEntity() == DestinationEntity.PRIVATE_MESSAGE) {
            boolean writePermit = requester.getId().equals(message.getAuthor().getId())
                    || requester.getId().equals(message.getDestinationId());
            if (writePermit) {
                dao.writeMessageImage(out, message);
            }
        } else {
            dao.writeMessageImage(out, message);
        }
    }

    public void setAccountAvatar(Integer accountId, InputStream in) {
        dao.setAccountAvatar(accountId, in);
    }

    public void setGroupAvatar(Integer groupId, InputStream in) {
        dao.setGroupAvatar(groupId, in);
    }

    public void setMessageImage(Message message, InputStream in) {
        dao.setMessageImage(message, in);
    }
}
