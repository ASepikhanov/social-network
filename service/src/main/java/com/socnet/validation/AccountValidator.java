package com.socnet.validation;

import com.socnet.Account;
import com.socnet.AccountService;
import com.socnet.validation.optional.ValidateDate;
import com.socnet.validation.optional.ValidateIcq;
import com.socnet.validation.optional.ValidatePatronymic;
import com.socnet.validation.optional.ValidateSkype;
import com.socnet.validation.required.ValidateMailFormat;
import com.socnet.validation.required.ValidateMailUnique;
import com.socnet.validation.required.ValidateName;
import com.socnet.validation.required.Validator;

import java.util.HashSet;
import java.util.Set;

public class AccountValidator {

    private Set<Validator> validators = new HashSet<>();

    public AccountValidator(AccountService service) {
        validators.add(new ValidateDate());
        validators.add(new ValidateIcq());
        validators.add(new ValidateMailFormat());
        validators.add(new ValidateMailUnique(service));
        validators.add(new ValidateName());
        validators.add(new ValidatePatronymic());
        validators.add(new ValidateSkype());
    }

    public boolean validate(Account account) throws ValidationException {
        for (Validator validator : validators) {
            if (!validator.doCheck(account)) {
                throw new ValidationException(validator.getFailMessage());
            }
        }
        return true;
    }

    public boolean validateUpdating(Account account) throws ValidationException {
        for (Validator validator : validators) {
            if (!validator.getClass().equals(ValidateMailUnique.class) && !validator.doCheck(account)) {
                throw new ValidationException(validator.getFailMessage());
            }
        }
        return true;
    }
}
