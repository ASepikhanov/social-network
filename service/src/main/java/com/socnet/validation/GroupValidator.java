package com.socnet.validation;

import com.socnet.Group;

public class GroupValidator {

    public boolean validate(Group group) throws ValidationException {
        if (group.getName() == null || group.getName().length() == 0) {
            throw new ValidationException("group name required");
        } else {
            return true;
        }
    }
}
