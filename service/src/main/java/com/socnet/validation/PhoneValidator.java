package com.socnet.validation;

import com.socnet.Phone;

import java.util.Collection;

public class PhoneValidator {

    public boolean validate(Collection<Phone> phones) throws ValidationException {
        for (Phone phone : phones) {
            String phoneNum = phone.getPhoneNum();
            if (phoneNum.length() == 0 || phoneNum.length() > 11 || !phoneNum.matches("[0-9]+")) {
                throw new ValidationException("phone number has incorrect format");
            }
        }
        return true;
    }
}
