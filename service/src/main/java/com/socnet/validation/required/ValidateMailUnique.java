package com.socnet.validation.required;

import com.socnet.Account;
import com.socnet.AccountService;

/**
 * This is the only validator class that depends on database connection,
 * so additional methods provided. Candidate to be moved into special
 * validators group when another database-based validator could possibly
 * be introduced.
 */
public class ValidateMailUnique implements Validator {

    private AccountService service;

    public ValidateMailUnique(AccountService service) {
        this.service = service;
    }

    @Override
    public boolean doCheck(Account account) {
        return doCheck(account.getEmail());
    }

    public boolean doCheck(String email) {
        return service.isEmailUnique(email);
    }

    @Override
    public String getFailMessage() {
        return "User with this email already registered";
    }
}
