package com.socnet.validation.required;

import com.socnet.Account;
import org.apache.commons.validator.routines.EmailValidator;

public class ValidateMailFormat implements Validator {
    @Override
    public boolean doCheck(Account account) {
        return EmailValidator.getInstance().isValid(account.getEmail());
    }

    @Override
    public String getFailMessage() {
        return "Incorrect email format";
    }
}
