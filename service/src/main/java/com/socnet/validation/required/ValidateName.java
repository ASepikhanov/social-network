package com.socnet.validation.required;

import com.socnet.Account;

public class ValidateName implements Validator {

    @Override
    public boolean doCheck(Account account) {
        String regex = "[a-zA-Z\\s]+";
        return account.getName().matches(regex) && account.getSurname().matches(regex);
    }

    @Override
    public String getFailMessage() {
        return "Name can consist of alphabetical characters and spaces only";
    }
}
