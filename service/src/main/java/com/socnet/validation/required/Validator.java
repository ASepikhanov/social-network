package com.socnet.validation.required;

import com.socnet.Account;

public interface Validator {

    boolean doCheck(Account account);

    String getFailMessage();
}
