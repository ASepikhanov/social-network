package com.socnet.validation.optional;

import com.socnet.Account;

public class ValidateSkype implements ValidatorOptional {

    @Override
    public boolean isOptionalFieldNull(Account account) {
        return account.getSkype() == null;
    }

    @Override
    public boolean whenOptionalFieldNonNull(Account account) {
        return account.getSkype().matches("[a-zA-Z][a-zA-Z0-9\\.,\\-_]{5,31}");
    }

    @Override
    public String getFailMessage() {
        return "Incorrect skype ID";
    }
}
