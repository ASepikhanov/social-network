package com.socnet.validation.optional;

import com.socnet.Account;
import com.socnet.validation.required.Validator;

/**
 * Validator for optional fields. Concrete validators should
 * override isOptionalFieldNull() and whenOptionalFieldNonNull() methods.
 */
public interface ValidatorOptional extends Validator {

    default boolean doCheck(Account account) {
        if (isOptionalFieldNull(account)) {
            return true;
        } else {
            return whenOptionalFieldNonNull(account);
        }
    }

    boolean isOptionalFieldNull(Account account);

    boolean whenOptionalFieldNonNull(Account account);
}
