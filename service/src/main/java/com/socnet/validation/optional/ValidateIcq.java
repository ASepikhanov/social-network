package com.socnet.validation.optional;

import com.socnet.Account;

public class ValidateIcq implements ValidatorOptional {

    @Override
    public boolean isOptionalFieldNull(Account account) {
        return account.getIcq() == null;
    }

    @Override
    public boolean whenOptionalFieldNonNull(Account account) {
        return account.getIcq() > 99_999 && account.getIcq() < 99999_99999_99999L;
    }

    @Override
    public String getFailMessage() {
        return "ICQ number should have 5-12 digits";
    }
}
