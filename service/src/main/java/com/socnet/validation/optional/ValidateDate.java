package com.socnet.validation.optional;

import com.socnet.Account;

import java.time.LocalDate;

public class ValidateDate implements ValidatorOptional {

    @Override
    public boolean isOptionalFieldNull(Account account) {
        return account.getBirthDate() == null;
    }

    @Override
    public boolean whenOptionalFieldNonNull(Account account) {
        LocalDate today = LocalDate.now();
        LocalDate birthDate = account.getBirthDate();
        return birthDate.isBefore(today) || birthDate.equals(today);
    }

    @Override
    public String getFailMessage() {
        return "Future dates are not permitted";
    }
}
