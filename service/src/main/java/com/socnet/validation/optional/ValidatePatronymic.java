package com.socnet.validation.optional;

import com.socnet.Account;

public class ValidatePatronymic implements ValidatorOptional {

    @Override
    public boolean isOptionalFieldNull(Account account) {
        return account.getMiddleName() == null;
    }

    @Override
    public boolean whenOptionalFieldNonNull(Account account) {
        return account.getMiddleName().matches("[a-zA-Z\\s]+");
    }

    @Override
    public String getFailMessage() {
        return "Patronymic can consist of alphabetical characters and spaces only";
    }
}
