package com.socnet.validation;

import com.socnet.ServiceException;

public class ValidationException extends ServiceException {

    public ValidationException(String message) {
        super(message);
    }
}
