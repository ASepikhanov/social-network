package com.socnet;

import com.socnet.validation.GroupValidator;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Transactional
@Service
public class GroupService {

    private GroupDao dao;
    private GroupValidator validator;
    private ImageService imageService;

    public GroupService(GroupDao groupDao, GroupValidator groupValidator, ImageService imageService) {
        this.validator = groupValidator;
        this.dao = groupDao;
        this.imageService = imageService;
    }

    public Group getGroupById(int id) throws ServiceException {
        Group group = dao.getGroupById(id);
        if (group != null) {
            return group;
        } else {
            throw new ServiceException("Group doesn't exist");
        }
    }

    public List<Group> getGroupsByName(String query) {
        return dao.getGroupsByName(query);
    }

    public int save(Group group, Account creator, InputStream avatar) throws ServiceException, IOException {
        validator.validate(group);
        Integer generatedId;
        if (avatar.available() > 0) {
            group.setAvatar(true);
            generatedId = dao.createGroup(group, creator);
            imageService.setGroupAvatar(generatedId, avatar);
        } else {
            generatedId = dao.createGroup(group, creator);
        }
        return generatedId;
    }

    public void editGroup(Group formReadGroup, Account editor, MultipartFile file)
            throws ServiceException, IOException {
        Group groupFromDb = getGroupById(formReadGroup.getGroupId());
        GroupRole editorRole = groupFromDb.getSubscriberRoles().get(editor.getId());
        if (editorRole == GroupRole.MODERATOR || editorRole == GroupRole.ADMIN) {
            if (formReadGroup.hasAvatar()) {
                InputStream avatar = file.getInputStream();
                if (avatar.available() > 0) {
                    imageService.setGroupAvatar(formReadGroup.getGroupId(), avatar);
                }
            }
            dao.updateGroup(formReadGroup);
        } else {
            throw new ServiceException("Only administrator or moderator can edit group");
        }
    }

    /**
     * @param group          group to update
     * @param subjectAccount account to be inserted in group as subscriber. This account should
     *                       have PENDING role to be updated to SUBSCRIBER role
     * @param approver       Account to approve update. This account should have MODERATOR or ADMIN
     *                       role to approve subjectAccount become SUBSCRIBER from PENDING
     */
    public boolean addSubscriberToGroup(Group group, Account subjectAccount, Account approver) throws ServiceException {
        Map<Integer, GroupRole> subscribers = group.getSubscriberRoles();
        GroupRole approverRole = subscribers.get(approver.getId());
        if (approverRole == GroupRole.MODERATOR || approverRole == GroupRole.ADMIN) {
            GroupRole subjectRole = subscribers.get(subjectAccount.getId());
            if (subjectRole == GroupRole.PENDING || subjectRole == GroupRole.MODERATOR) {
                dao.updateAccountRole(group, subjectAccount, GroupRole.SUBSCRIBER);
                group.addSubscriberRole(subjectAccount.getId(), GroupRole.SUBSCRIBER);
                return true;
            } else {
                throw new ServiceException("Account should first apply to group before being accepted");
            }
        } else {
            throw new ServiceException("Only moderator or admin can approve group apply");
        }
    }

    public boolean removeSubscriber(Group group, Account subjectAccount, Account approver) throws ServiceException {
        Map<Integer, GroupRole> subscribers = group.getSubscriberRoles();
        GroupRole approverRole = subscribers.get(approver.getId());
        GroupRole subjectAccountRole = subscribers.get(subjectAccount.getId());
        boolean removePermit = subjectAccount.getId().equals(approver.getId())
                || (approverRole == GroupRole.MODERATOR || approverRole == GroupRole.ADMIN)
                && subjectAccountRole != GroupRole.ADMIN;
        if (removePermit) {
            group.removeSubscriber(subjectAccount.getId());
            return dao.deleteAccountFromGroup(group.getGroupId(), subjectAccount.getId());
        } else {
            throw new ServiceException("Only admin or moderator can remove others (except admin) from group.");
        }
    }

    /**
     * @param group          group to update
     * @param subjectAccount account to become moderator. This account should have SUBSCRIBER role
     *                       to become MODERATOR
     * @param approver       only group admin can set moderators
     */
    public boolean addModeratorToGroup(Group group, Account subjectAccount, Account approver) throws ServiceException {
        Map<Integer, GroupRole> subscribers = group.getSubscriberRoles();
        GroupRole approverRole = subscribers.get(approver.getId());
        if (approverRole == GroupRole.ADMIN) {
            if (group.getSubscriberRoles().get(subjectAccount.getId()) == GroupRole.SUBSCRIBER) {
                dao.updateAccountRole(group, subjectAccount, GroupRole.MODERATOR);
                group.addSubscriberRole(subjectAccount.getId(), GroupRole.MODERATOR);
                return true;
            } else {
                throw new ServiceException("To become moderator account should be subscriber first");
            }
        } else {
            throw new ServiceException("Only group admin can set moderators");
        }
    }

    public boolean addGroupApplyRequest(Group group, Account account) throws ServiceException {
        Integer accountId = account.getId();
        if (group.getSubscriberRoles().containsKey(accountId)) {
            throw new ServiceException("Account " + accountId + " is already in this group");
        }
        dao.addMemberToGroup(group.getGroupId(), accountId, GroupRole.PENDING);
        group.addSubscriberRole(accountId, GroupRole.PENDING);
        return true;
    }
}
