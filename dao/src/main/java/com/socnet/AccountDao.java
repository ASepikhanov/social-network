package com.socnet;

import java.util.Collection;
import java.util.List;

public interface AccountDao {
    void createAccount(Account account);

    Collection<Integer> getOutFriendRequests(int id);

    Collection<Integer> getInFriendRequests(int id);

    Account getAccountById(int id, boolean initRelations);

    Account getAccountByMail(String email);

    List<Account> getAccountsByName(String name);

    Account updateAccount(Account accountToUpdate);

    void deleteAccount(Account account);

    boolean createFriendRequest(int idFrom, int idTo);

    boolean acceptFriendRequest(int idFrom, int idTo);

    boolean rejectFriendRequest(int idFrom, int idTo);

    boolean deleteFriend(int id1, int id2);

    boolean isEmailUnique(String email);

    boolean isAccountAdmin(Integer id);

    Collection<Account> getAdminList();

    boolean addAdmin(Integer target);

    boolean removeAdmin(Integer adminId);

    Collection<Account> getAccountListByIdList(Collection<Integer> ids);
}
