package com.socnet.converter;

import com.neovisionaries.i18n.CountryCode;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CountryCodeAttributeConverter implements AttributeConverter<CountryCode, String> {
    @Override
    public String convertToDatabaseColumn(CountryCode countryCode) {
        return countryCode.getAlpha2();
    }

    @Override
    public CountryCode convertToEntityAttribute(String s) {
        return CountryCode.getByCode(s);
    }
}
