package com.socnet;

public interface PhoneDao {
    Phone getPhoneByPhoneNum(String number);
}
