package com.socnet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DaoException extends RuntimeException {

    private static final Logger logger = LoggerFactory.getLogger("exception");
    private final Exception cause;

    public DaoException(Exception cause) {
        logger.info("Dao layer exception, ", cause);
        this.cause = cause;
    }

    @Override
    public Exception getCause() {
        return cause;
    }
}
