package com.socnet;

import com.socnet.message.Message;

import java.util.List;

public interface MessageDao {
    Integer createMessage(Message message);

    Message getMessageById(Integer id);

    List<Message> getAccountWall(Integer accountId);

    List<Message> getGroupWall(Integer groupId);

    List<Message> getPrivateMessages(Integer account1, Integer account2);

    List<Message> getAccountConversationList(Integer accountId);

    void updateMessage(Message messageToUpdate);

    void deleteMessage(Message messageToDelete);
}
