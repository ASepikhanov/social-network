package com.socnet;

public enum Relation {

    LOW_TO_HIGH_REQUEST(0),
    HIGH_TO_LOW_REQUEST(1),
    MUTUAL_FRIENDS(2);

    private int code;

    Relation(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
