package com.socnet.jpa;

import com.socnet.Phone;
import com.socnet.PhoneDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class PhoneDaoJpa implements PhoneDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Phone getPhoneByPhoneNum(String number) {
        return entityManager.find(Phone.class, number);
    }
}
