package com.socnet.jpa;

import com.socnet.Account;
import com.socnet.Group;
import com.socnet.GroupDao;
import com.socnet.GroupRole;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

public class GroupDaoJpa implements GroupDao {

    private static final String GROUP_ID_PARAM = "groupId";
    private static final String ACCOUNT_ID_PARAM = "accountId";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public int createGroup(Group group, Account creator) {
        entityManager.persist(group);
        Integer generatedId = group.getGroupId();
        String sql = "insert into accounts_in_groups values (:createdGroupId, :creatorId, :adminCode)";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("createdGroupId", generatedId);
        nativeQuery.setParameter("creatorId", creator.getId());
        nativeQuery.setParameter("adminCode", GroupRole.ADMIN.getCode());
        nativeQuery.executeUpdate();
        return generatedId;
    }

    @Override
    public Group getGroupById(int id) {
        Group group = entityManager.find(Group.class, id);
        if (group != null) {
            fillSubscribers(group);
        }
        return group;
    }

    @SuppressWarnings("unchecked")
    private void fillSubscribers(Group group) {
        Query nativeQuery = entityManager
                .createNativeQuery("select id_account, role from accounts_in_groups where id_group = :groupId");
        nativeQuery.setParameter(GROUP_ID_PARAM, group.getGroupId());
        List<Object[]> list = nativeQuery.getResultList();
        for (Object[] row : list) {
            Integer participant = ((Number) row[0]).intValue();
            Integer role = ((Number) row[1]).intValue();
            group.addSubscriberRole(participant, GroupRole.getRole(role));
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Group> getGroupsByName(String name) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = criteriaBuilder.createQuery(Group.class);
        Root<Group> root = criteriaQuery.from(Group.class);

        Expression<String> path = root.get("name");
        Predicate likePredicate = criteriaBuilder.like(path, '%' + name + '%');
        criteriaQuery.where(criteriaBuilder.and(likePredicate));
        List<Group> groups = entityManager.createQuery(criteriaQuery.select(root)).getResultList();

        for (Group group : groups) {
            fillSubscribers(group);
        }
        return groups;
    }

    @Override
    public void updateGroup(Group groupToUpdate) {
        entityManager.merge(groupToUpdate);
    }

    @Override
    public void deleteGroup(Group group) {
        entityManager.remove(group);
    }

    @Override
    public boolean addMemberToGroup(int groupId, int accountId, GroupRole role) {
        String sql = "insert into accounts_in_groups values(:groupId, :accountId, :roleCode)";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter(GROUP_ID_PARAM, groupId);
        nativeQuery.setParameter(ACCOUNT_ID_PARAM, accountId);
        nativeQuery.setParameter("roleCode", role.getCode());
        return nativeQuery.executeUpdate() > 0;
    }

    @Override
    public boolean updateAccountRole(Group group, Account account, GroupRole role) {
        String sql = "update accounts_in_groups set role = :role where id_group = :groupId and id_account = :accountId";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("role", role.getCode());
        nativeQuery.setParameter(GROUP_ID_PARAM, group.getGroupId());
        nativeQuery.setParameter(ACCOUNT_ID_PARAM, account.getId());
        return nativeQuery.executeUpdate() > 0;
    }

    @Override
    public boolean deleteAccountFromGroup(int groupId, int accountId) {
        String sql = "delete from accounts_in_groups where id_group = :groupId and id_account = :accountId";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter(GROUP_ID_PARAM, groupId);
        nativeQuery.setParameter(ACCOUNT_ID_PARAM, accountId);
        return nativeQuery.executeUpdate() > 0;
    }
}
