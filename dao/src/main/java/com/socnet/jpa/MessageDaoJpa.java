package com.socnet.jpa;

import com.socnet.MessageDao;
import com.socnet.message.DestinationEntity;
import com.socnet.message.Message;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.*;
import java.util.List;

public class MessageDaoJpa implements MessageDao {

    private static final String DESTINATION_ID_PARAM = "destinationId";
    private static final String AUTHOR_COL_NAME = "author";
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Integer createMessage(Message message) {
        entityManager.persist(message);
        return message.getId();
    }

    @Override
    public Message getMessageById(Integer id) {
        return entityManager.find(Message.class, id);
    }

    @Override
    public List<Message> getAccountWall(Integer accountId) {
        return getEntityWall(accountId, DestinationEntity.ACCOUNT);
    }

    @Override
    public List<Message> getGroupWall(Integer groupId) {
        return getEntityWall(groupId, DestinationEntity.GROUP);
    }

    private List<Message> getEntityWall(Integer entityId, DestinationEntity entity) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> criteriaQuery = criteriaBuilder.createQuery(Message.class);
        Root<Message> root = criteriaQuery.from(Message.class);
        root.fetch(AUTHOR_COL_NAME, JoinType.INNER);

        Expression<String> destinationId = root.get(DESTINATION_ID_PARAM);
        Expression<String> destinationEntity = root.get("destinationEntity");

        Predicate destIdEquals = criteriaBuilder.equal(destinationId, entityId);
        Predicate destEntityEquals = criteriaBuilder.equal(destinationEntity, entity);
        criteriaQuery.where(criteriaBuilder.and(destEntityEquals, destIdEquals));

        Expression<Integer> messageId = root.get("id");
        criteriaQuery.orderBy(criteriaBuilder.desc(messageId));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<Message> getPrivateMessages(Integer account1, Integer account2) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Message> criteriaQuery = criteriaBuilder.createQuery(Message.class);
        Root<Message> root = criteriaQuery.from(Message.class);

        Expression<Integer> authorId = root.get(AUTHOR_COL_NAME);
        root.fetch(AUTHOR_COL_NAME, JoinType.INNER);
        Expression<String> destinationId = root.get(DESTINATION_ID_PARAM);
        Expression<String> destinationEntity = root.get("destinationEntity");

        Predicate leftRestriction = criteriaBuilder
                .and(criteriaBuilder.equal(authorId, account1), criteriaBuilder.equal(destinationId, account2));
        Predicate rightRestriction = criteriaBuilder
                .and(criteriaBuilder.equal(authorId, account2), criteriaBuilder.equal(destinationId, account1));
        Predicate combinationPredicate = criteriaBuilder.or(leftRestriction, rightRestriction);
        criteriaQuery.where(criteriaBuilder
                .and(combinationPredicate, criteriaBuilder.equal(destinationEntity, DestinationEntity.PRIVATE_MESSAGE)));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    /**
     * Returns set of all last private messages of specified account with other accounts and used to
     * prepare conversation list, where all last messages of account shown.
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Message> getAccountConversationList(Integer accountId) {
        String query = "select m2.id, text, destination_entity, destination_id, author, write_time, image "
                + "from messages m2 inner join\n"
                + "    (select max(x.id) as id, (destination_id + author) as ind from messages m\n"
                + "     inner join (select max(id) as id from messages \n"
                + "        where ( (destination_id = :accountId or author = :accountId) and destination_entity = 2 ) \n"
                + "        group by author, destination_id) x\n"
                + "    on x.id = m.id\n"
                + "    group by ind) x2\n"
                + "on m2.id = x2.id order by m2.id desc";
        Query nativeQuery = entityManager.createNativeQuery(query, Message.class);
        nativeQuery.setParameter("accountId", accountId);
        return nativeQuery.getResultList();
    }

    @Override
    public void updateMessage(Message messageToUpdate) {
        entityManager.merge(messageToUpdate);
    }

    @Override
    public void deleteMessage(Message messageToDelete) {
        entityManager.remove(messageToDelete);
    }
}
