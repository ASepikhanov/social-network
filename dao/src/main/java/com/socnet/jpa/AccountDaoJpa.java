package com.socnet.jpa;

import com.socnet.*;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class AccountDaoJpa implements AccountDao {

    private static final String STATUS_COLUMN = "status";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void createAccount(Account account) {
        entityManager.persist(account);
    }

    @Override
    public Account getAccountById(int id, boolean initRelations) {
        Account account = entityManager.find(Account.class, id);
        if (account != null && initRelations) {
            account.setFriends(getFriendList(id));
            account.setOutFriendRequests(getOutFriendRequests(id));
            account.setInFriendRequests(getInFriendRequests(id));
        }
        return account;
    }

    public Account getAccountById(int id) {
        return getAccountById(id, true);
    }

    @SuppressWarnings("unchecked")
    private Collection<Integer> getFriendList(int id) {
        String sql = "select account_1 from relations where status = 2 and account_2 = :id"
                + " union "
                + "select account_2 from relations where status = 2 and account_1 = :id";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("id", id);
        return nativeQuery.getResultList();
    }

    @SuppressWarnings("unchecked")
    public Collection<Integer> getOutFriendRequests(int id) {
        String outRequestsQuery = "select account_2 from relations where status = 0 and account_1 = :id "
                + "union "
                + "select account_1 from relations where status = 1 and account_2 = :id";
        Query nativeQuery = entityManager.createNativeQuery(outRequestsQuery);
        nativeQuery.setParameter("id", id);
        return nativeQuery.getResultList();
    }

    @SuppressWarnings("unchecked")
    public Collection<Integer> getInFriendRequests(int id) {
        String inRequestsQuery = "select account_2 from relations where status = 1 and account_1 = :id "
                + "union "
                + "select account_1 from relations where status = 0 and account_2 = :id";
        Query nativeQuery = entityManager.createNativeQuery(inRequestsQuery);
        nativeQuery.setParameter("id", id);
        return nativeQuery.getResultList();
    }


    @Override
    public Account getAccountByMail(String email) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> accountCriteria = cb.createQuery(Account.class);
        Root<Account> accountRoot = accountCriteria.from(Account.class);
        accountCriteria.select(accountRoot);
        accountCriteria.where(cb.equal(accountRoot.get("email"), email));
        List<Account> resultList = entityManager.createQuery(accountCriteria).getResultList();
        if (!resultList.isEmpty()) {
            Account account = resultList.get(0);
            Integer id = account.getId();
            account.setFriends(getFriendList(id));
            account.setOutFriendRequests(getOutFriendRequests(id));
            account.setInFriendRequests(getInFriendRequests(id));
            return account;
        } else {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Account> getAccountsByName(String name) {
        String query = "select * from ("
                + "SELECT *, concat(name, ' ', surname) a, concat(surname, ' ', name) b FROM accounts "
                + ") x where a like :name or b like :name";
        Query nativeQuery = entityManager.createNativeQuery(query, Account.class);
        nativeQuery.setParameter("name", '%' + name + '%');
        return nativeQuery.getResultList();
    }

    @Override
    public Account updateAccount(Account accountToUpdate) {
        return entityManager.merge(accountToUpdate);
    }

    @Override
    public void deleteAccount(Account account) {
        entityManager.remove(account);
        Query deletePrivateMessages = entityManager
                .createNativeQuery("delete from messages "
                        + "where (destination_id = :id or author = :id) and destination_entity = 2");
        deletePrivateMessages.setParameter("id", account.getId());
        deletePrivateMessages.executeUpdate();

        Query deleteWallMessagesQuery = entityManager
                .createNativeQuery("delete  from messages where author = :id and destination_entity in (0, 1)");
        deleteWallMessagesQuery.setParameter("id", account.getId());
        deleteWallMessagesQuery.executeUpdate();
    }

    @Override
    public boolean createFriendRequest(int idFrom, int idTo) {
        if (isRelationPresent(idFrom, idTo)) {
            throw new DaoException(new IllegalArgumentException("relation between " + idFrom + " and "
                    + idTo + " already exists"));
        }
        String query = "insert into relations values(:id1, :id2, :status)";
        Query nativeQuery = entityManager.createNativeQuery(query);
        nativeQuery.setParameter("id1", Math.min(idFrom, idTo));
        nativeQuery.setParameter("id2", Math.max(idFrom, idTo));

        if (idFrom < idTo) {
            nativeQuery.setParameter(STATUS_COLUMN, Relation.LOW_TO_HIGH_REQUEST.getCode());
        } else {
            nativeQuery.setParameter(STATUS_COLUMN, Relation.HIGH_TO_LOW_REQUEST.getCode());
        }

        return nativeQuery.executeUpdate() > 0;
    }

    @Override
    public boolean acceptFriendRequest(int idFrom, int idTo) {
        if (friendRequestExists(idFrom, idTo)) {
            String sql = "update relations set status = " + Relation.MUTUAL_FRIENDS.getCode()
                    + " where account_1 = :id1 and account_2 = :id2";
            Query nativeQuery = entityManager.createNativeQuery(sql);
            nativeQuery.setParameter("id1", Math.min(idFrom, idTo));
            nativeQuery.setParameter("id2", Math.max(idFrom, idTo));
            return nativeQuery.executeUpdate() > 0;
        } else {
            throw new DaoException(new IllegalArgumentException("friend request from " + idFrom + " to "
                    + idTo + " does not exist: can't accept"));
        }
    }

    @Override
    public boolean rejectFriendRequest(int idFrom, int idTo) {
        if (friendRequestExists(idFrom, idTo)) {
            String sql = "delete from relations where account_1 = :id1 and account_2 = :id2";
            Query nativeQuery = entityManager.createNativeQuery(sql);
            nativeQuery.setParameter("id1", Math.min(idFrom, idTo));
            nativeQuery.setParameter("id2", Math.max(idFrom, idTo));
            return nativeQuery.executeUpdate() > 0;
        } else {
            throw new DaoException(new IllegalArgumentException("friend request from " + idFrom + " to "
                    + idTo + " does not exist: can't reject"));
        }
    }

    @Override
    public boolean deleteFriend(int id1, int id2) {
        String sql = "delete from relations where account_1 = :id1 and account_2 = :id2 and status = "
                + Relation.MUTUAL_FRIENDS.getCode();
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("id1", Math.min(id1, id2));
        nativeQuery.setParameter("id2", Math.max(id1, id2));
        return nativeQuery.executeUpdate() > 0;
    }

    @Override
    public boolean isEmailUnique(String email) {
        return getAccountByMail(email) == null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean isAccountAdmin(Integer id) {
        String sql = "select count(*) as cnt from admins where id = :id";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("id", id);
        BigInteger count = (BigInteger) nativeQuery.getResultList().get(0);
        return count.intValue() == 1;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Account> getAdminList() {
        String sqlAdmins = "select id from admins";
        List<Integer> idAdmins = entityManager.createNativeQuery(sqlAdmins).getResultList();
        String sqlAccounts = "SELECT * FROM accounts a WHERE a.id IN (:ids)";
        Query nativeQuery = entityManager.createNativeQuery(sqlAccounts, Account.class);
        nativeQuery.setParameter("ids", idAdmins);
        return nativeQuery.getResultList();
    }

    @Override
    public boolean addAdmin(Integer target) {
        Query nativeQuery = entityManager.createNativeQuery("insert into admins values (:id)");
        nativeQuery.setParameter("id", target);
        return nativeQuery.executeUpdate() > 0;
    }

    @Override
    public boolean removeAdmin(Integer adminId) {
        Query nativeQuery = entityManager.createNativeQuery("delete from admins where id = :id");
        nativeQuery.setParameter("id", adminId);
        return nativeQuery.executeUpdate() > 0;
    }

    /**
     * Used to validate usage of <code>acceptFriendRequest </code> method
     */
    @SuppressWarnings("unchecked")
    private boolean friendRequestExists(int idFrom, int idTo) {
        String sql = "select count(*) as cnt from relations where account_1 = :id1 and account_2 = :id2 and status = :status";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("id1", Math.min(idFrom, idTo));
        nativeQuery.setParameter("id2", Math.max(idTo, idFrom));
        nativeQuery.setParameter(STATUS_COLUMN, idFrom < idTo
                ? Relation.LOW_TO_HIGH_REQUEST.getCode() : Relation.HIGH_TO_LOW_REQUEST.getCode());
        BigInteger count = (BigInteger) nativeQuery.getResultList().get(0);
        return count.intValue() == 1;
    }

    private boolean isRelationPresent(int idFrom, int idTo) {
        String sql = "select count(*) as cnt from relations  where account_1 = :id1 and account_2 = :id2";
        Query nativeQuery = entityManager.createNativeQuery(sql);
        nativeQuery.setParameter("id1", Math.min(idFrom, idTo));
        nativeQuery.setParameter("id2", Math.max(idTo, idFrom));
        BigInteger count = (BigInteger) nativeQuery.getResultList().get(0);
        return count.intValue() == 1;
    }

    @Override
    public Collection<Account> getAccountListByIdList(Collection<Integer> ids) {
        if (!ids.isEmpty()) {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
            Root<Account> root = criteriaQuery.from(Account.class);
            criteriaQuery.select(root);
            Expression<Integer> accountId = root.get("id");
            criteriaQuery.where(accountId.in(ids));
            return entityManager.createQuery(criteriaQuery).getResultList();
        } else {
            return new ArrayList<>();
        }
    }
}
