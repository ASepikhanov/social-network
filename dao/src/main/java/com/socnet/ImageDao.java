package com.socnet;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.WriteMode;
import com.socnet.message.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class ImageDao {

    private static final String JPG_EXTENSION = ".jpg";
    private DbxClientV2 client;

    public ImageDao(String accessToken) {
        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/socnet").withUserLocale("en_US")
                .build();
        client = new DbxClientV2(config, accessToken);
    }

    public boolean setAccountAvatar(Integer accountId, InputStream in) {
        try {
            client.files().uploadBuilder("/account" + accountId + JPG_EXTENSION)
                    .withMode(WriteMode.OVERWRITE)
                    .uploadAndFinish(in);
            return true;
        } catch (DbxException e) {
            throw new DaoException(e);
        } catch (IOException e) {
            return false;
        }
    }

    public boolean setMessageImage(Message message, InputStream in) {
        try {
            client.files().uploadBuilder(getMessageImagePath(message))
                    .withMode(WriteMode.OVERWRITE)
                    .uploadAndFinish(in);
            return true;
        } catch (DbxException e) {
            throw new DaoException(e);
        } catch (IOException e) {
            return false;
        }
    }

    public boolean writeMessageImage(OutputStream out, Message message) {
        try {
            client.files().downloadBuilder(getMessageImagePath(message))
                    .download(out);
            return true;
        } catch (DbxException e) {
            throw new DaoException(e);
        } catch (IOException e) {
            return false;
        }
    }

    private String getMessageImagePath(Message message) {
        return "/" + message.getDestinationEntity()
                + '_' + message.getDestinationId()
                + '_' + message.getAuthor().getId()
                + '_' + message.getId() + ".jpg";
    }

    public boolean setGroupAvatar(Integer groupId, InputStream in) {
        try {
            client.files().uploadBuilder("/group_avatar" + groupId + JPG_EXTENSION)
                    .withMode(WriteMode.OVERWRITE)
                    .uploadAndFinish(in);
            return true;
        } catch (DbxException e) {
            throw new DaoException(e);
        } catch (IOException e) {
            return false;
        }
    }

    public boolean write(OutputStream out, String entityType, Integer entityId) {
        try {
            client.files().downloadBuilder('/' + entityType + entityId + JPG_EXTENSION).download(out);
            return true;
        } catch (DbxException e) {
            throw new DaoException(e);
        } catch (IOException e) {
            return false;
        }
    }
}
