package com.socnet;

import java.util.List;

public interface GroupDao {
    int createGroup(Group group, Account creator);

    Group getGroupById(int id);

    List<Group> getGroupsByName(String name);

    void updateGroup(Group groupToUpdate);

    void deleteGroup(Group group);

    boolean addMemberToGroup(int groupId, int accountId, GroupRole role);

    boolean updateAccountRole(Group group, Account account, GroupRole role);

    boolean deleteAccountFromGroup(int groupId, int accountId);
}
