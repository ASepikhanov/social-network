package com.socnet.springdata;

import com.socnet.Phone;
import com.socnet.PhoneDao;
import org.springframework.data.repository.CrudRepository;

public interface PhoneRepository extends CrudRepository<Phone, String>, PhoneDao {
}