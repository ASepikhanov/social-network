package com.socnet;

import com.socnet.springdata.PhoneRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.Assert.*;

@Transactional
public class PhoneDaoSpringDataTest extends DatabaseAwareTest {

    @Autowired
    private PhoneRepository phoneRepository;

    @Test
    public void testSetup() {
        assertNotNull(phoneRepository);
    }

    @Test
    public void testGetPhoneByNum() {
        Optional<Phone> phone = phoneRepository.findById("11");
        if (phone.isPresent()) {
            assertEquals(phone.get().getPhoneNum(), "11");
            assertEquals(phone.get().getType(), PhoneType.PERSONAL);
            assertEquals(1, (int) phone.get().getAccount().getId());
            Phone phone1 = phoneRepository.getPhoneByPhoneNum("11");
            assertEquals(phone.get(), phone1);
        } else {
            fail();
        }
    }
}