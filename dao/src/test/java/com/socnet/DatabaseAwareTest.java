package com.socnet;

import com.socnet.jpa.AccountDaoJpa;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-test.xml"})
public class DatabaseAwareTest {

    private static final String RELATIONS_CREATE = "CREATE TABLE relations (\n" +
            "  account_1 int(11) NOT NULL,\n" +
            "  account_2 int(11) NOT NULL,\n" +
            "  status tinyint(1) DEFAULT NULL,\n" +
            "  PRIMARY KEY (account_1,account_2),\n" +
            "  CONSTRAINT idacc FOREIGN KEY (account_1) REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE NO ACTION,\n" +
            "  CONSTRAINT idacc2 FOREIGN KEY (account_2) REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE NO ACTION\n" +
            ")";
    private static final String ACCOUNTS_CREATE = "CREATE TABLE accounts (\n" +
            "  id int(11) NOT NULL AUTO_INCREMENT,\n" +
            "  name varchar(45) DEFAULT NULL,\n" +
            "  surname varchar(45) DEFAULT NULL,\n" +
            "  middle_name varchar(45) DEFAULT NULL,\n" +
            "  birth_date date DEFAULT NULL,\n" +
            "  email varchar(45) NOT NULL,\n" +
            "  icq int(15) DEFAULT NULL,\n" +
            "  skype varchar(45) DEFAULT NULL,\n" +
            "  additional_info longtext,\n" +
            "  country char(2) DEFAULT NULL," +
            "  password_hash char(64) DEFAULT NULL," +
            "  registration_date date NOT NULL," +
            "  city varchar(45) DEFAULT NULL," +
            "  avatar boolean DEFAULT false," +
            "  PRIMARY KEY (id)," +
            "  UNIQUE KEY email_UNIQUE (email)" +
            ")";
    private static final String GROUPS_CREATE = "CREATE TABLE groups (\n" +
            "  id_group int(11) NOT NULL AUTO_INCREMENT,\n" +
            "  description varchar(45) DEFAULT NULL,\n" +
            "  name varchar(45) NOT NULL,\n" +
            "  creation_date date NOT NULL," +
            "  avatar boolean DEFAULT false," +
            "  PRIMARY KEY (id_group)\n" +
            ")";
    private static final String ACCOUNTS_IN_GROUPS_CREATE = "CREATE TABLE accounts_in_groups (\n" +
            "  id_group int(11) NOT NULL,\n" +
            "  id_account int(11) NOT NULL,\n" +
            "  role int(1) NOT NULL,\n" +
            "  PRIMARY KEY (id_group, id_account),\n" +
            "  CONSTRAINT iacc FOREIGN KEY (id_account) REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE NO ACTION,\n" +
            "  CONSTRAINT id_group FOREIGN KEY (id_group) REFERENCES groups (id_group) ON DELETE CASCADE ON UPDATE NO ACTION\n" +
            ")";
    private static final String PHONES_CREATE = "CREATE TABLE phones (\n" +
            "  id_account int(11) NOT NULL,\n" +
            "  phone varchar(14) NOT NULL,\n" +
            "  phone_type tinyint(1) NOT NULL,\n" +
            "  PRIMARY KEY (phone),\n" +
            "  UNIQUE KEY phone_UNIQUE (phone)," +
            "  CONSTRAINT id_account FOREIGN KEY (id_account) REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE NO ACTION\n" +
            ")";
    private static final String MESSAGES_CREATE = "CREATE TABLE messages (\n" +
            "  id int(11) NOT NULL AUTO_INCREMENT,\n" +
            "  text longtext,\n" +
            "  destination_entity int(1) NOT NULL,\n" +
            "  destination_id int(11) NOT NULL,\n" +
            "  author int(11) NOT NULL,\n" +
            "  write_time datetime NOT NULL,\n" +
            "  image boolean DEFAULT false,\n" +
            "  PRIMARY KEY (id))";
    private static final String ADMINS_CREATE = "CREATE TABLE admins (\n" +
            "  id int(11) NOT NULL, " +
            "  PRIMARY KEY (id),\n" +
            "  CONSTRAINT id_acc FOREIGN KEY (id) REFERENCES accounts (id) ON DELETE CASCADE ON UPDATE NO ACTION\n" +
            ")";
    private static final String MESSAGE_INSERT_QUERY_END = "parsedatetime('17-09-2012 18:47:52', 'dd-MM-yyyy hh:mm:ss'), false)";
    private static final Logger logger = LoggerFactory.getLogger(DatabaseAwareTest.class);
    @Autowired
    protected DataSource dataSource;
    @Autowired
    protected AccountDaoJpa accountDao;
    @Autowired
    protected GroupDao groupDao;
    @Autowired
    protected PhoneDao phoneDao;
    @Autowired
    protected MessageDao messageDao;
    @Autowired
    protected PlatformTransactionManager platformTransactionManager;
    protected TransactionTemplate transactionTemplate;
    private int testCount;

    @Before
    public void initializeDatabase() {
        if (testCount == 0) {
            createAllTables();
            testCount++;
        }
        transactionTemplate = new TransactionTemplate(platformTransactionManager);
    }

    @Test
    public void testSpringSetup() {
        assertNotNull(dataSource);
        assertNotNull(accountDao);
        assertNotNull(groupDao);
        assertNotNull(phoneDao);
        assertNotNull(messageDao);
    }

    private void createAllTables() {
        try (Connection connection = dataSource.getConnection();
             Statement createStatement = connection.createStatement()) {
            createStatement.executeUpdate(ACCOUNTS_CREATE);
            connection.commit();
            createStatement.executeUpdate(GROUPS_CREATE);
            connection.commit();
            createStatement.executeUpdate(ACCOUNTS_IN_GROUPS_CREATE);
            connection.commit();
            createStatement.executeUpdate(PHONES_CREATE);
            connection.commit();
            createStatement.executeUpdate(RELATIONS_CREATE);
            connection.commit();
            createStatement.executeUpdate(MESSAGES_CREATE);
            connection.commit();
            createStatement.executeUpdate(ADMINS_CREATE);
            connection.commit();

            try (Statement insertStatement = connection.createStatement()) {
                fillAccounts(insertStatement);
                fillPhones(insertStatement);
                fillGroups(insertStatement);
                fillAccountsInGroups(insertStatement);
                fillRelations(insertStatement);
                fillAccountWallMessages(insertStatement);
                fillGroupWallMessages(insertStatement);
                fillPrivateMessages(insertStatement);
                fillAdmins(insertStatement);
                connection.commit();
            }
        } catch (SQLException e) {
            logger.info(e.toString());
        }
    }

    private void fillAccounts(Statement insertStatement) throws SQLException {
        for (int i = 0; i < 20; i++) {
            insertStatement.executeUpdate("insert into accounts values (default, 'a', 'b', 'c', "
                    + "'2011-11-11', 'mail" + i + "', 123, 'skype', 'add_info', 'RU', 'pass', '2012-12-12', 'city', default);");
        }
        insertStatement.executeUpdate("insert into accounts values (default, 'some', 'special', 'person', "
                + "'1999-2-3', 'specialMail', 999, 'skype', 'specialInfo', 'US', 32000, '2002-12-12', 'Washington', 'true');");
    }

    private void fillAdmins(Statement insertStatement) throws SQLException {
        insertStatement.executeUpdate("insert into admins values (1);");
        insertStatement.executeUpdate("insert into admins values (2);");
        insertStatement.executeUpdate("insert into admins values (3);");
    }

    private void fillPhones(Statement insertStatement) throws SQLException {
        insertStatement.executeUpdate("insert into phones values (1, '11', 0);");
        insertStatement.executeUpdate("insert into phones values (1, '12', 0);");
        insertStatement.executeUpdate("insert into phones values (1, '13', 0);");
        insertStatement.executeUpdate("insert into phones values (1, '14', 1);");
        insertStatement.executeUpdate("insert into phones values (2, '15', 0);");
        insertStatement.executeUpdate("insert into phones values (2, '16', 1);");
        insertStatement.executeUpdate("insert into phones values (3, '17', 1);");
        insertStatement.executeUpdate("insert into phones values (4, '18', 1);");
    }

    private void fillGroups(Statement insertStatement) throws SQLException {
        insertStatement.executeUpdate("insert into groups values(default, 'first_group', 'a', '2003-03-03', default )");
        insertStatement.executeUpdate("insert into groups values(default, 'second_group', 'a', '2003-03-03', default )");
        insertStatement.executeUpdate("insert into groups values(default, 'third_group', 'a', '2003-03-03', default )");
        insertStatement.executeUpdate("insert into groups values(default, 'another', 'a', '2003-03-03', default )");
        insertStatement.executeUpdate("insert into groups values(default, 'another_one', 'a', '2003-03-03', default )");
        insertStatement.executeUpdate("insert into groups values(default, 'desc3', 'a', '2003-03-03', default )");
        insertStatement.executeUpdate("insert into groups values(default, 'desc4', 'a', '2003-03-03', default )");
        insertStatement.executeUpdate("insert into groups values(default, 'desc8', 'find', '2003-03-03', default )");
    }

    private void fillAccountsInGroups(Statement insertStatement) throws SQLException {
        //adding admins into their groups (id_group, id_account, role)
        insertStatement.executeUpdate("insert into accounts_in_groups values (1, 1, 2);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (2, 1, 2);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (3, 1, 2);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (4, 2, 2);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (5, 2, 2);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (6, 3, 2);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (7, 4, 2);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (8, 4, 2);");

        //adding others to existing groups
        insertStatement.executeUpdate("insert into accounts_in_groups values (1, 3, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (1, 4, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (1, 5, 1);");

        insertStatement.executeUpdate("insert into accounts_in_groups values (2, 3, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (2, 4, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (2, 5, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (2, 6, 0);");

        insertStatement.executeUpdate("insert into accounts_in_groups values (4, 6, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (4, 7, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (4, 8, 0);");
        insertStatement.executeUpdate("insert into accounts_in_groups values (4, 9, 0);");
    }

    private void fillRelations(Statement insertStatement) throws SQLException {
        insertStatement.executeUpdate("insert into relations values (1, 2, 0)");
        insertStatement.executeUpdate("insert into relations values (1, 3, 0)");
        insertStatement.executeUpdate("insert into relations values (1, 4, 0)");
        insertStatement.executeUpdate("insert into relations values (1, 5, 0)");

        insertStatement.executeUpdate("insert into relations values (2, 3, 0)");
        insertStatement.executeUpdate("insert into relations values (2, 4, 1)");
        insertStatement.executeUpdate("insert into relations values (2, 5, 1)");
        insertStatement.executeUpdate("insert into relations values (2, 6, 1)");
        insertStatement.executeUpdate("insert into relations values (2, 7, 0)");

        insertStatement.executeUpdate("insert into relations values (3, 4, 2)");
        insertStatement.executeUpdate("insert into relations values (3, 5, 2)");
        insertStatement.executeUpdate("insert into relations values (3, 6, 0)");
        insertStatement.executeUpdate("insert into relations values (3, 7, 1)");

        insertStatement.executeUpdate("insert into relations values (4, 8, 2)");
    }

    private void fillAccountWallMessages(Statement insertStatement) throws SQLException {
        insertStatement.executeUpdate("insert into messages values (default, 'hi', 0, 2, 1, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'hi', 0, 2, 1, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'hi', 0, 2, 2, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'hi', 0, 3, 2, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'hi', 0, 3, 2, " +
                MESSAGE_INSERT_QUERY_END);
    }

    private void fillGroupWallMessages(Statement insertStatement) throws SQLException {
        insertStatement.executeUpdate("insert into messages values (default, 'group', 1, 2, 1, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'group', 1, 2, 1, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'group', 1, 2, 2, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'group', 1, 3, 2, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'group', 1, 3, 2, " +
                MESSAGE_INSERT_QUERY_END);
    }

    private void fillPrivateMessages(Statement insertStatement) throws SQLException {
        insertStatement.executeUpdate("insert into messages values (default, 'hello 2', 2, 2, 1, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'hello 2 again', 2, 2, 1, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'hello answer', 2, 1, 2, " +
                MESSAGE_INSERT_QUERY_END);
        insertStatement.executeUpdate("insert into messages values (default, 'hello conv', 2, 2, 3, " +
                MESSAGE_INSERT_QUERY_END);
    }
}
