package com.socnet;

import com.socnet.message.DestinationEntity;
import com.socnet.message.Message;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;

@Transactional
@Rollback
public class MessageDaoTest extends DatabaseAwareTest {

    @Test
    public void testCreateMessage() {
        Message createdMessage = generateCommonMessage(null);
        messageDao.createMessage(createdMessage);
        List<Message> actual = messageDao.getAccountWall(2);
        List<Message> expected = new ArrayList<>();
        LocalDateTime time = LocalDateTime.of(2012, 9, 17, 18, 47, 52);
        expected.add(createdMessage);
        Account author1 = accountDao.getAccountById(1);
        Account author2 = accountDao.getAccountById(2);
        expected.add(new Message.Builder().id(3).text("hi").setImage(false)
                .destinationEntity(DestinationEntity.ACCOUNT).destinationId(2).author(author2).writeTime(time).build());
        expected.add(new Message.Builder().id(2).text("hi").setImage(false)
                .destinationEntity(DestinationEntity.ACCOUNT).destinationId(2).author(author1).writeTime(time).build());
        expected.add(new Message.Builder().id(1).text("hi").setImage(false)
                .destinationEntity(DestinationEntity.ACCOUNT).destinationId(2).author(author1).writeTime(time).build());
        assertEquals(expected, actual);
        assertEquals(messageDao.getMessageById(15), createdMessage);
    }

    @Test
    public void testGetGroupWall() {
        List<Message> actual = messageDao.getGroupWall(2);
        List<Message> expected = new ArrayList<>();
        LocalDateTime time = LocalDateTime.of(2012, 9, 17, 18, 47, 52);

        Account author1 = accountDao.getAccountById(1);
        Account author2 = accountDao.getAccountById(2);
        String group = "group";
        expected.add(new Message.Builder().id(8).destinationEntity(DestinationEntity.GROUP).destinationId(2)
                .setImage(false).author(author2).writeTime(time).text(group).build());
        expected.add(new Message.Builder().id(7).destinationEntity(DestinationEntity.GROUP).destinationId(2)
                .setImage(false).author(author1).writeTime(time).text(group).build());
        expected.add(new Message.Builder().id(6).destinationEntity(DestinationEntity.GROUP).destinationId(2)
                .setImage(false).author(author1).writeTime(time).text(group).build());

        assertEquals(expected, actual);
    }

    @Test
    public void testUpdateMessage() {
        Message updating = messageDao.getMessageById(1);
        updating.setText("hello");
        messageDao.updateMessage(updating);
        assertEquals(updating, messageDao.getMessageById(1));
    }

    @Test
    public void testPrivateMessages() {
        List<Message> actual = messageDao.getPrivateMessages(1, 2);
        @SuppressWarnings("unchecked")
        List<Message> expected = new ArrayList<>();
        LocalDateTime time = LocalDateTime.of(2012, 9, 17, 18, 47, 52);

        Account author1 = accountDao.getAccountById(1);
        Account author2 = accountDao.getAccountById(2);
        expected.add(new Message.Builder().id(11).destinationEntity(DestinationEntity.PRIVATE_MESSAGE)
                .text("hello 2").setImage(false).destinationId(2).author(author1).writeTime(time).build());
        expected.add(new Message.Builder().id(12).destinationEntity(DestinationEntity.PRIVATE_MESSAGE)
                .text("hello 2 again").setImage(false).destinationId(2).author(author1).writeTime(time).build());
        expected.add(new Message.Builder().id(13).destinationEntity(DestinationEntity.PRIVATE_MESSAGE)
                .text("hello answer").setImage(false).destinationId(1).author(author2).writeTime(time).build());

        assertEquals(expected, actual);
        assertEquals(expected, messageDao.getPrivateMessages(2, 1));
    }

    @Test
    public void testDeleteMessage() {
        Message message = messageDao.getMessageById(1);
        messageDao.deleteMessage(message);
        assertEquals(2, messageDao.getAccountWall(2).size());
    }

    @Test
    public void testGetConversationList() {
        List<Message> actual = messageDao.getAccountConversationList(2);
        List<Message> expected = new ArrayList<>();
        LocalDateTime time = LocalDateTime.of(2012, 9, 17, 18, 47, 52);
        expected.add(new Message.Builder().id(14).text("hello conv").setImage(false)
                .destinationEntity(DestinationEntity.PRIVATE_MESSAGE).destinationId(2)
                .author(accountDao.getAccountById(3)).writeTime(time).build());
        expected.add(new Message.Builder().id(13).text("hello answer").setImage(false)
                .destinationEntity(DestinationEntity.PRIVATE_MESSAGE).destinationId(1)
                .author(accountDao.getAccountById(2)).writeTime(time).build());
        assertEquals(expected, actual);
    }

    private Message generateCommonMessage(Integer id) {
        Message message = new Message();
        message.setId(id);
        message.setText("hi");
        message.setDestinationEntity(DestinationEntity.ACCOUNT);
        message.setDestinationId(2);
        message.setAuthor(new Account(1));
        message.setWriteTime(LocalDateTime.of(2012, 9, 17, 18, 47, 52));
        message.setImage(false);
        return message;
    }
}
