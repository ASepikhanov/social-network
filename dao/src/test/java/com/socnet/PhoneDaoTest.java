package com.socnet;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Rollback
public class PhoneDaoTest extends DatabaseAwareTest {

    @Test
    public void testGetPhoneByNum() {
        Phone phone = phoneDao.getPhoneByPhoneNum("11");
        assertEquals(phone.getPhoneNum(), "11");
        assertEquals(phone.getAccount().getId(), Integer.valueOf(1));
    }
}
