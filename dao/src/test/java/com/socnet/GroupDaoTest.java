package com.socnet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Transactional
@Rollback
public class GroupDaoTest extends DatabaseAwareTest {

    private static final String DEFAULT = "a";
    private static final String FIRST_GROUP = "first_group";

    @Test
    public void testCreateGroup() {
        groupDao.createGroup(new Group(null, DEFAULT, DEFAULT, LocalDate.parse("2003-03-03")), new Account(5));
        Group actual = groupDao.getGroupById(9);
        Group expected = generateCommonGroup(9);
        expected.addSubscriberRole(5, GroupRole.ADMIN);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetGroupById() {
        Group actual = groupDao.getGroupById(1);
        Group expected = generateCommonGroup(1);
        Map<Integer, GroupRole> groupRoles = new HashMap<>();
        groupRoles.put(1, GroupRole.ADMIN);
        groupRoles.put(3, GroupRole.SUBSCRIBER);
        groupRoles.put(4, GroupRole.SUBSCRIBER);
        groupRoles.put(5, GroupRole.MODERATOR);
        expected.setSubscriberRoles(groupRoles);
        expected.setDescription(FIRST_GROUP);
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getSubscriberRoles(), actual.getSubscriberRoles());
        assertEquals(actual.getSubscribers().size(), groupRoles.size());
    }

    @Test
    public void testUpdateGroup() {
        Group groupToUpdate = generateCommonGroup(3);
        String newDescription = "special";
        groupToUpdate.setDescription(newDescription);
        groupDao.updateGroup(groupToUpdate);
        assertEquals(groupDao.getGroupById(3).getDescription(), newDescription);
    }

    @Test
    public void testDeleteGroup() {
        groupDao.deleteGroup(groupDao.getGroupById(5));
        assertNull(groupDao.getGroupById(5));
    }

    @Test
    public void testAddAccountToGroup() {
        groupDao.addMemberToGroup(7, 1, GroupRole.SUBSCRIBER);
        groupDao.addMemberToGroup(7, 2, GroupRole.SUBSCRIBER);
        groupDao.addMemberToGroup(7, 3, GroupRole.MODERATOR);
        Group actual = groupDao.getGroupById(7);
        Group expected = generateCommonGroup(7);
        expected.addSubscriberRole(4, GroupRole.ADMIN);
        expected.addSubscriberRole(1, GroupRole.SUBSCRIBER);
        expected.addSubscriberRole(2, GroupRole.SUBSCRIBER);
        expected.addSubscriberRole(3, GroupRole.MODERATOR);
        expected.addSubscriber(accountDao.getAccountById(1));
        expected.addSubscriber(accountDao.getAccountById(2));
        expected.addSubscriber(accountDao.getAccountById(3));
        expected.addSubscriber(accountDao.getAccountById(4));
        expected.setAvatar(false);
        expected.setDescription("desc4");
        assertEquals(expected, actual);
    }

    @Test
    public void testDeleteAccountFromGroup() {
        groupDao.deleteAccountFromGroup(1, 3);
        groupDao.deleteAccountFromGroup(1, 4);
        groupDao.deleteAccountFromGroup(1, 5);
        Group actual = groupDao.getGroupById(1);
        Group expected = generateCommonGroup(1);
        expected.addSubscriberRole(1, GroupRole.ADMIN);
        expected.setDescription(FIRST_GROUP);
        expected.addSubscriber(accountDao.getAccountById(1));
        assertEquals(expected, actual);
    }

    @Test
    public void testGetGroupsByName() {
        Collection<Group> actual = groupDao.getGroupsByName("ind");
        Collection<Group> expected = new ArrayList<>();
        Group expectedGroup = generateCommonGroup(8);
        expectedGroup.setDescription("desc8");
        expectedGroup.addSubscriberRole(4, GroupRole.ADMIN);
        expectedGroup.addSubscriber(accountDao.getAccountById(4));
        expectedGroup.setName("find");
        expected.add(expectedGroup);
        assertEquals(expected, actual);
    }

    private Group generateCommonGroup(int idGroup) {
        return new Group(idGroup, DEFAULT, DEFAULT, LocalDate.parse("2003-03-03"));
    }
}
