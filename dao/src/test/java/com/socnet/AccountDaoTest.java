package com.socnet;

import org.junit.*;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.*;

public class AccountDaoTest extends DatabaseAwareTest {

    private static Account generateCommonAccount(int id) {
        String birthDate = "2011-11-11";
        String registrationDate = "2012-12-12";
        int mailIndex = id - 1;
        return new Account.Builder(id, "a", "b", "mail" + mailIndex, LocalDate.parse(registrationDate))
                .icq((long) 123).skype("skype").additionalInfo("add_info").country("RU").city("city").password("pass")
                .birthDate(LocalDate.parse(birthDate)).patronymic("c").build();
    }

    @Test
    @Transactional
    public void testGetAccountById() {
        Account actual1 = accountDao.getAccountById(1);
        Account expected1 = generateCommonAccount(1);

        expected1.addGroup(new Group(1));
        expected1.addGroup(new Group(2));
        expected1.addGroup(new Group(3));

        expected1.addOutFriendRequest(2);
        expected1.addOutFriendRequest(3);
        expected1.addOutFriendRequest(4);
        expected1.addOutFriendRequest(5);
        assertEquals(expected1.getName(), actual1.getName());
        assertEquals(expected1.getInFriendRequests(), actual1.getInFriendRequests());
        assertEquals(expected1.getOutFriendRequests(), actual1.getOutFriendRequests());
        assertEquals(expected1.getFriends(), actual1.getFriends());
        int counterGroups1 = 0;
        List<Group> actual1Groups = (List<Group>) actual1.getGroups();
        for (Group group : expected1.getGroups()) {
            assertEquals(group.getGroupId(), actual1Groups.get(counterGroups1++).getGroupId());
        }

        Account actual2 = accountDao.getAccountById(2);
        Account expected2 = generateCommonAccount(2);

        expected2.addGroup(new Group(4));
        expected2.addGroup(new Group(5));

        expected2.addInFriendRequest(1);
        expected2.addOutFriendRequest(3);
        expected2.addInFriendRequest(4);
        expected2.addInFriendRequest(5);
        expected2.addInFriendRequest(6);
        expected2.addOutFriendRequest(7);
        assertEquals(expected2.getName(), actual2.getName());
        assertEquals(expected2.getInFriendRequests(), actual2.getInFriendRequests());
        assertEquals(expected2.getOutFriendRequests(), actual2.getOutFriendRequests());
        assertEquals(expected2.getFriends(), actual2.getFriends());
        int counterGroups2 = 0;
        List<Group> actual2Groups = (List<Group>) actual2.getGroups();
        for (Group group : expected2.getGroups()) {
            assertEquals(group.getGroupId(), actual2Groups.get(counterGroups2++).getGroupId());
        }

        Account actual3 = accountDao.getAccountById(3);
        Account expected3 = generateCommonAccount(3);

        expected3.addGroup(new Group(6));
        expected3.addGroup(new Group(1));
        expected3.addGroup(new Group(2));

        expected3.addInFriendRequest(1);
        expected3.addInFriendRequest(2);
        expected3.addFriend(4);
        expected3.addFriend(5);
        expected3.addOutFriendRequest(6);
        expected3.addInFriendRequest(7);
        assertEquals(expected3.getName(), actual3.getName());
        assertEquals(expected3.getInFriendRequests(), actual3.getInFriendRequests());
        assertEquals(expected3.getOutFriendRequests(), actual3.getOutFriendRequests());
        assertEquals(expected3.getFriends(), actual3.getFriends());
        int counterGroups3 = 0;
        List<Group> actual3Groups = (List<Group>) actual3.getGroups();
        for (Group group : expected3.getGroups()) {
            assertEquals(group.getGroupId(), actual3Groups.get(counterGroups3++).getGroupId());
        }
        Account actual4 = accountDao.getAccountById(4);
        Account expected4 = generateCommonAccount(4);

        expected4.addGroup(new Group(7));
        expected4.addGroup(new Group(8));
        expected4.addGroup(new Group(1));
        expected4.addGroup(new Group(2));

        expected4.addInFriendRequest(1);
        expected4.addOutFriendRequest(2);
        expected4.addFriend(3);
        expected4.addFriend(8);
        assertEquals(expected4.getName(), actual4.getName());
        assertEquals(expected4.getInFriendRequests(), actual4.getInFriendRequests());
        assertEquals(expected4.getOutFriendRequests(), actual4.getOutFriendRequests());
        assertEquals(expected4.getFriends(), actual4.getFriends());
        int counterGroups4 = 0;
        List<Group> actual4Groups = (List<Group>) actual4.getGroups();
        for (Group group : expected4.getGroups()) {
            assertEquals(group.getGroupId(), actual4Groups.get(counterGroups4++).getGroupId());
        }
    }

    @Test
    @Transactional
    @Rollback
    public void testBatchUpdatePhones() {
        Collection<Phone> oldPhones = transactionTemplate.execute(transactionStatus -> {
            Account account = accountDao.getAccountById(1);
            account.getPhones().removeIf(phone -> phone.getPhoneNum().equals("11"));
            account.getPhones().add(new Phone(account, "555", PhoneType.PERSONAL));
            account.getPhones().add(new Phone(account, "666", PhoneType.PERSONAL));
            accountDao.updateAccount(account);
            return account.getPhones();
        });

        transactionTemplate.execute(transactionStatus -> {
            Account updated = accountDao.getAccountById(1);
            Iterator<Phone> updatedIterator = updated.getPhones().iterator();
            Iterator<Phone> expectedIterator = oldPhones.iterator();
            while (updatedIterator.hasNext()) {
                assertEquals(updatedIterator.next().getPhoneNum(), expectedIterator.next().getPhoneNum());
            }
            return true;
        });

    }

    @Test
    public void testGetAccountByMail() {
        transactionTemplate.execute(transactionStatus -> {
            Account actual = accountDao.getAccountByMail("mail0");
            Account expected = generateCommonAccount(1);
            assertEquals(actual.getEmail(), expected.getEmail());
            Account actual2 = accountDao.getAccountByMail("no");
            assertNull(actual2);
            return true;
        });
    }

    @Test
    @Transactional
    @Rollback
    public void testCreateAccount() {
        String name = "cName";
        String surname = "cSurname";
        String patronymic = "cPatronymic";
        LocalDate birthDate = LocalDate.parse("2000-11-12");
        String mail = "cMail";
        Long icq = (long) 999;
        String skype = "cSkype";
        String addInfo = "a";
        String country = "RU";
        LocalDate registrationDate = LocalDate.parse("2011-11-11");

        Account insertedAccount = new Account.Builder(null, name, surname, mail, registrationDate)
                .birthDate(birthDate).icq(icq).skype(skype).additionalInfo(addInfo).country(country)
                .patronymic(patronymic).build();

        transactionTemplate.execute(transactionStatus -> {
            accountDao.createAccount(insertedAccount);
            return true;
        });

        transactionTemplate.execute(transactionStatus -> {
            Account expected = accountDao.getAccountById(insertedAccount.getId());
            Account actual = new Account.Builder(22, name, surname, mail, registrationDate)
                    .birthDate(birthDate).icq(icq).skype(skype).additionalInfo(addInfo).country(country)
                    .patronymic(patronymic).build();
            assertEquals(expected.getId(), actual.getId());
            assertEquals(expected.getName(), actual.getName());
            assertEquals(expected.getAdditionalInfo(), actual.getAdditionalInfo());
            assertEquals(expected.getFriends(), actual.getFriends());
            assertEquals(expected.getOutFriendRequests(), actual.getOutFriendRequests());
            assertEquals(expected.getInFriendRequests(), actual.getInFriendRequests());
            assertEquals(expected.getPhones().size(), actual.getPhones().size());
            assertEquals(expected.getGroups().size(), actual.getGroups().size());
            return true;
        });
    }

    @Test
    public void testUpdateAccount() {
        Account detachedAccountUpdatedName = transactionTemplate.execute(transactionStatus -> {
            Account old = accountDao.getAccountById(15);
            String newName = "new name";
            return new Account.Builder(old.getId(), newName, old.getSurname(), old.getEmail(),
                    old.getRegistrationDate()).patronymic(old.getMiddleName()).birthDate(old.getBirthDate())
                    .icq(old.getIcq()).skype(old.getSkype()).additionalInfo(old.getAdditionalInfo())
                    .country(old.getCountry().getAlpha2()).build();
        });

        transactionTemplate.execute(transactionStatus -> {
            Account justUpdatedAccount = accountDao.updateAccount(detachedAccountUpdatedName);
            assertEquals(justUpdatedAccount.getName(), detachedAccountUpdatedName.getName());
            assertEquals(accountDao.getAccountById(15).getName(), detachedAccountUpdatedName.getName());
            return true;
        });
    }

    @Test
    @Transactional
    public void testDeleteAccount() {
        accountDao.deleteAccount(accountDao.getAccountById(1));
        assertNull(accountDao.getAccountById(1));
    }

    @Test
    @Transactional
    public void testCreateFriendRequest() {
        int idLow = 10;
        accountDao.createFriendRequest(idLow, 16);
        accountDao.createFriendRequest(idLow, 17);
        Account lowToHighActual = accountDao.getAccountById(idLow);
        Account lowToHighExpected = generateCommonAccount(idLow);
        lowToHighExpected.addOutFriendRequest(17);
        lowToHighExpected.addOutFriendRequest(16);
        assertEquals(lowToHighExpected.getOutFriendRequests(), lowToHighActual.getOutFriendRequests());
        assertEquals(lowToHighExpected.getInFriendRequests(), lowToHighActual.getInFriendRequests());
        assertEquals(lowToHighExpected.getFriends(), lowToHighActual.getFriends());

        Account requestAcceptorActual = accountDao.getAccountById(16);
        Account requestAcceptorExpected = generateCommonAccount(16);
        requestAcceptorExpected.addInFriendRequest(idLow);
        assertEquals(requestAcceptorExpected.getInFriendRequests(), requestAcceptorActual.getInFriendRequests());

        int idHigh = 12;
        accountDao.createFriendRequest(idHigh, 11);
        accountDao.createFriendRequest(idHigh, 9);
        Account highToLowActual = accountDao.getAccountById(idHigh);
        Account highToLowExpected = generateCommonAccount(idHigh);
        highToLowExpected.addOutFriendRequest(9);
        highToLowExpected.addOutFriendRequest(11);
        assertEquals(highToLowActual.getInFriendRequests(), highToLowExpected.getInFriendRequests());
        assertEquals(highToLowActual.getOutFriendRequests(), highToLowExpected.getOutFriendRequests());
        assertEquals(highToLowActual.getFriends(), highToLowExpected.getFriends());
    }

    @Test
    @Transactional
    public void testRelationAlreadyPresentOnRequestExceptionLowHigh() {
        try {
            accountDao.createFriendRequest(1, 2);
            fail();
        } catch (DaoException e) {
            assertEquals(e.getCause().getMessage(), "relation between 1 and 2 already exists");
        }
    }

    @Test
    @Transactional
    public void testRelationAlreadyPresentOnRequestExceptionHighLow() {
        try {
            accountDao.createFriendRequest(2, 1);
            fail();
        } catch (DaoException e) {
            assertEquals(e.getCause().getMessage(), "relation between 2 and 1 already exists");
        }
    }

    @Test
    @Transactional
    public void testAcceptFriendRequest() {
        accountDao.acceptFriendRequest(1, 2);
        accountDao.acceptFriendRequest(1, 3);
        Account actual = accountDao.getAccountById(1);
        Account expected = generateCommonAccount(1);
        expected.addFriend(2);
        expected.addFriend(3);
        expected.addOutFriendRequest(4);
        expected.addOutFriendRequest(5);
        assertEquals(expected.getFriends(), actual.getFriends());

        Account actual2 = accountDao.getAccountById(2);
        Account expected2 = generateCommonAccount(2);
        expected2.addFriend(1);
        assertEquals(expected2.getFriends(), actual2.getFriends());
    }

    @Test
    public void testAcceptNonExistingFriendRequestException() {
        try {
            accountDao.acceptFriendRequest(1, 10);
            fail();
        } catch (DaoException e) {
            assertEquals(e.getCause().getMessage(), "friend request from 1 to 10 does not exist: can't accept");
        }
    }

    @Test
    @Transactional
    public void testRejectFriendRequest() {
        accountDao.rejectFriendRequest(1, 2);
        accountDao.rejectFriendRequest(1, 3);
        accountDao.rejectFriendRequest(1, 4);
        Account actual = accountDao.getAccountById(1);
        Account expected = generateCommonAccount(1);
        expected.addOutFriendRequest(5);
        assertEquals(expected.getOutFriendRequests(), actual.getOutFriendRequests());

        Account actual2 = accountDao.getAccountById(2);
        Account expected2 = generateCommonAccount(2);
        expected2.addInFriendRequest(4);
        expected2.addInFriendRequest(5);
        expected2.addInFriendRequest(6);
        assertEquals(expected2.getInFriendRequests(), actual2.getInFriendRequests());

        accountDao.rejectFriendRequest(7, 3);
        Account actual3 = accountDao.getAccountById(7);
        Account expected3 = generateCommonAccount(7);
        assertEquals(expected3.getOutFriendRequests(), actual3.getOutFriendRequests());
    }

    @Test
    public void testRejectNonExistingFriendRequestException() {
        try {
            accountDao.rejectFriendRequest(1, 10);
            fail();
        } catch (DaoException e) {
            assertEquals(e.getCause().getMessage(), "friend request from 1 to 10 does not exist: can't reject");
        }
    }

    @Test
    @Transactional
    public void testDeleteFriend() {
        assertTrue(accountDao.deleteFriend(3, 4));
        assertTrue(accountDao.deleteFriend(5, 3));
        Account account3 = accountDao.getAccountById(3);
        Account account5 = accountDao.getAccountById(5);
        assertEquals(account3.getFriends(), new ArrayList<>());
        assertEquals(account5.getFriends(), new ArrayList<>());
    }

    @Test
    public void testCheckEmailUnique() {
        assertTrue(accountDao.isEmailUnique("unique1234"));
        assertFalse(accountDao.isEmailUnique("mail1"));
    }

    @Test
    @Transactional
    public void testGetAccountsByName() {
        Collection<Account> actual = accountDao.getAccountsByName("a");
        Collection<Account> expected = new ArrayList<>();
        for (int i = 1; i < 21; i++) {
            expected.add(generateCommonAccount(i));
        }
        Account special = new Account.Builder(21, "some", "special", "specialMail", LocalDate.of(2002, 12, 12))
                .patronymic("person").birthDate(LocalDate.of(1999, 2, 3))
                .additionalInfo("specialInfo").icq(999L).skype("skype").country("US").hasAvatar(true)
                .city("Washington").build();
        expected.add(special);
        assertEquals(expected.size(), actual.size());

        Account actualSpecial = accountDao.getAccountsByName("some").iterator().next();
        assertEquals(actualSpecial.getName(), special.getName());
    }

    @Test
    @Transactional
    public void testIsAccountAdmin() {
        assertTrue(accountDao.isAccountAdmin(1));
        assertTrue(accountDao.isAccountAdmin(2));
        assertTrue(accountDao.isAccountAdmin(3));
        assertFalse(accountDao.isAccountAdmin(4));
        assertFalse(accountDao.isAccountAdmin(99));
    }

    @Test
    public void testGetAdminList() {
        Collection<Account> adminList = accountDao.getAdminList();
        List<Account> expected = Arrays
                .asList(new Account(1), new Account(2), new Account(3));
        Iterator<Account> actualIterator = adminList.iterator();
        Iterator<Account> expectedIterator = expected.iterator();
        assertEquals(expected.size(), adminList.size());
        while (actualIterator.hasNext()) {
            assertEquals(actualIterator.next().getId(), expectedIterator.next().getId());
        }
    }

    @Test
    public void testAccountListByIdList() {
        Collection<Account> actual = accountDao.getAccountListByIdList(Arrays.asList(1, 2, 5, 7));
        Collection<Account> expected = Arrays
                .asList(new Account(1), new Account(2), new Account(5), new Account(7));
        Iterator<Account> actualIterator = actual.iterator();
        Iterator<Account> expectedIterator = expected.iterator();
        assertEquals(expected.size(), actual.size());
        while (expectedIterator.hasNext()) {
            assertEquals(expectedIterator.next().getId(), actualIterator.next().getId());
        }
    }
}
