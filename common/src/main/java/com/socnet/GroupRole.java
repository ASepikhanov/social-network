package com.socnet;

import java.util.Arrays;
import java.util.Optional;

public enum GroupRole {

    SUBSCRIBER(0),
    MODERATOR(1),
    ADMIN(2),
    PENDING(3),
    NOT_IN_GROUP(4);

    private int code;

    GroupRole(int code) {
        this.code = code;
    }

    public static GroupRole getRole(int code) {
        Optional<GroupRole> role = Arrays.stream(GroupRole.values())
                .filter(r -> r.getCode() == code)
                .findFirst();
        if (role.isPresent()) {
            return role.get();
        } else {
            throw new IllegalArgumentException("Invalid code");
        }
    }

    public int getCode() {
        return code;
    }
}
