package com.socnet;

import java.util.Arrays;
import java.util.Optional;

public enum PhoneType {

    PERSONAL(0),
    WORKING(1);

    private final int code;

    PhoneType(int code) {
        this.code = code;
    }

    public static PhoneType getType(int code) {
        Optional<PhoneType> type = Arrays.stream(PhoneType.values())
                .filter(t -> t.getCode() == code)
                .findFirst();
        if (type.isPresent()) {
            return type.get();
        } else {
            throw new IllegalArgumentException("Invalid code");
        }
    }

    public int getCode() {
        return code;
    }
}
