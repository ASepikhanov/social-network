package com.socnet;

import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import javax.persistence.*;

import org.hibernate.annotations.WhereJoinTable;

@Entity
@Table(name = "groups")
public class Group {

    @Column(name = "name")
    private String name;

    @Column(name = "id_group")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer groupId;

    @Column(name = "description")
    private String description;

    @Transient
    private Map<Integer, GroupRole> subscriberRoles = new HashMap<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "accounts_in_groups",
            joinColumns = @JoinColumn(name = "id_group"),
            inverseJoinColumns = @JoinColumn(name = "id_account"))
    @WhereJoinTable(clause = "role = 3")
    private Collection<Account> pendingRequests = new ArrayList<>();

    @ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
    private Collection<Account> subscribers = new ArrayList<>();

    @Column(name = "creation_date", updatable = false)
    private LocalDate creationDate;

    @Column(name = "avatar")
    private Boolean avatar;

    public Group() {
    }

    public Group(Integer id) {
        this.groupId = id;
    }

    public Group(Integer groupId, String name, String description, LocalDate creationDate) {
        this.name = name;
        this.groupId = groupId;
        this.description = description;
        this.creationDate = creationDate;
    }

    public Collection<Account> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Collection<Account> subscribers) {
        this.subscribers = subscribers;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAvatar() {
        return avatar;
    }

    public void setAvatar(Boolean avatar) {
        this.avatar = avatar;
    }

    public Boolean hasAvatar() {
        return avatar == null ? Boolean.FALSE : avatar;
    }

    public void addSubscriberRole(int id, GroupRole role) {
        subscriberRoles.put(id, role);
    }

    public void addSubscriber(Account account) {
        subscribers.add(account);
    }

    public void removeSubscriber(Integer subscriberId) {
        subscribers.removeIf(s -> s.getId().equals(subscriberId));
    }

    public Map<Integer, GroupRole> getSubscriberRoles() {
        return subscriberRoles;
    }

    public void setSubscriberRoles(Map<Integer, GroupRole> subscriberRoles) {
        this.subscriberRoles = subscriberRoles;
    }

    public Map<Integer, GroupRole> getApproovedSubscribers() {
        Map<Integer, GroupRole> approoved = new ConcurrentHashMap<>();
        for (Map.Entry entry : subscriberRoles.entrySet()) {
            if (entry.getValue() != GroupRole.PENDING) {
                approoved.put((Integer) entry.getKey(), (GroupRole) entry.getValue());
            }
        }
        return approoved;
    }

    public Collection<Account> getPendingRequests() {
        return pendingRequests;
    }

    public Group setPendingRequests(Collection<Account> pendingRequests) {
        this.pendingRequests = pendingRequests;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return Objects.equals(name, group.name)
                && Objects.equals(groupId, group.groupId)
                && Objects.equals(description, group.description)
                && Objects.equals(subscribers, group.subscribers)
                && Objects.equals(creationDate, group.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, groupId, description, subscribers, creationDate);
    }
}
