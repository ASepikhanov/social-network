package com.socnet;

import com.neovisionaries.i18n.CountryCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "accounts")
public class Account {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "icq")
    private Long icq;

    @Column(name = "skype")
    private String skype;

    @Column(name = "additional_info")
    private String additionalInfo;

    @Column(name = "country")
    private CountryCode country;

    @Column(name = "city")
    private String city;

    @Column(name = "registration_date", updatable = false)
    private LocalDate registrationDate;

    @Column(name = "avatar")
    private Boolean avatar;

    @Column(name = "password_hash", updatable = false)
    private String passwordHash;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "id_account", nullable = false)
    private Collection<Phone> phones = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "accounts_in_groups",
            joinColumns = {@JoinColumn(name = "id_account")},
            inverseJoinColumns = {@JoinColumn(name = "id_group")}
    )
    private Collection<Group> groups = new ArrayList<>();

    @Transient
    private Collection<Integer> friends = new ArrayList<>();
    @Transient
    private Collection<Integer> outFriendRequests = new ArrayList<>();
    @Transient
    private Collection<Integer> inFriendRequests = new ArrayList<>();

    public Account() {
    }

    private Account(Builder builder) {
        id = builder.id;
        name = builder.name;
        surname = builder.surname;
        middleName = builder.patronymic;
        birthDate = builder.birthDate;
        email = builder.email;
        icq = builder.icq;
        skype = builder.skype;
        additionalInfo = builder.additionalInfo;
        country = builder.country;
        city = builder.city;
        registrationDate = builder.registrationDate;
        avatar = builder.avatar;
        passwordHash = builder.password;
    }

    public Account(Integer id) {
        this.id = id;
        this.registrationDate = LocalDate.now();
    }

    public void setCountry(String country) {
        this.country = CountryCode.getByCode(country);
    }

    public void setAvatar(Boolean avatar) {
        this.avatar = avatar;
    }

    public Collection<Integer> getFriends() {
        return friends;
    }

    public void setFriends(Collection<Integer> friends) {
        this.friends = friends;
    }

    public Collection<Integer> getOutFriendRequests() {
        return outFriendRequests;
    }

    public void setOutFriendRequests(Collection<Integer> outFriendRequests) {
        this.outFriendRequests = outFriendRequests;
    }

    public Collection<Integer> getInFriendRequests() {
        return inFriendRequests;
    }

    public void setInFriendRequests(Collection<Integer> inFriendRequests) {
        this.inFriendRequests = inFriendRequests;
    }

    public Collection<Phone> getPhones() {
        return phones;
    }

    public void setPhones(Collection<Phone> phones) {
        this.phones = phones;
    }

    public void addFriend(int friendId) {
        friends.add(friendId);
    }

    public void deleteFriend(int friendIdToDelete) {
        friends.removeIf(friendId -> friendId.equals(friendIdToDelete));
    }

    public void addGroup(Group group) {
        groups.add(group);
    }

    public void addOutFriendRequest(int id) {
        outFriendRequests.add(id);
    }

    public void deleteOutFriendRequest(int idTo) {
        outFriendRequests.removeIf(currId -> currId.equals(idTo));
    }

    public void addInFriendRequest(int id) {
        inFriendRequests.add(id);
    }

    public void deleteInFriendRequest(int idFrom) {
        inFriendRequests.removeIf(currId -> currId.equals(idFrom));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate == null ? null : LocalDate.parse(birthDate);
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getIcq() {
        return icq;
    }

    public void setIcq(Long icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public CountryCode getCountry() {
        return country;
    }

    public void setCountry(CountryCode country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Boolean hasAvatar() {
        return avatar == null ? Boolean.FALSE : avatar;
    }

    public Collection<Group> getGroups() {
        return groups;
    }

    public void setGroups(Collection<Group> groups) {
        this.groups = groups;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(name, account.name) &&
                Objects.equals(surname, account.surname) &&
                Objects.equals(middleName, account.middleName) &&
                Objects.equals(birthDate, account.birthDate) &&
                Objects.equals(email, account.email) &&
                Objects.equals(icq, account.icq) &&
                Objects.equals(skype, account.skype) &&
                Objects.equals(additionalInfo, account.additionalInfo) &&
                country == account.country &&
                Objects.equals(city, account.city) &&
                Objects.equals(registrationDate, account.registrationDate) &&
                Objects.equals(avatar, account.avatar) &&
                Objects.equals(friends, account.friends) &&
                Objects.equals(outFriendRequests, account.outFriendRequests) &&
                Objects.equals(inFriendRequests, account.inFriendRequests);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, surname, middleName, birthDate, email, icq, skype, additionalInfo,
                country, city, registrationDate, avatar, phones, groups, friends, outFriendRequests, inFriendRequests);
    }

    public static class Builder {
        //required
        private final Integer id;
        private final String name;
        private final String surname;
        private final String email;
        private final LocalDate registrationDate;

        //optional
        private String patronymic;
        private LocalDate birthDate;
        private Long icq;
        private String skype;
        private String additionalInfo;
        private String city;
        private CountryCode country;
        private Boolean avatar;
        private String password;

        public Builder(Integer id, String name, String surname, String email, LocalDate registrationDate) {
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.registrationDate = registrationDate;
        }

        public Builder patronymic(String patronymic) {
            this.patronymic = patronymic;
            return this;
        }

        public Builder birthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public Builder icq(Long icq) {
            this.icq = icq;
            return this;
        }

        public Builder skype(String skype) {
            this.skype = skype;
            return this;
        }

        public Builder additionalInfo(String additionalInfo) {
            this.additionalInfo = additionalInfo;
            return this;
        }

        public Builder country(String country) {
            this.country = CountryCode.getByCode(country);
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder hasAvatar(Boolean avatar) {
            this.avatar = avatar;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }
}
