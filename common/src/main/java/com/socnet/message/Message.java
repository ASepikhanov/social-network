package com.socnet.message;

import com.socnet.Account;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "messages")
public class Message implements Comparable {

    @Column(name = "text")
    private String text;

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "destination_entity")
    @Enumerated(EnumType.ORDINAL)
    private DestinationEntity destinationEntity;

    @Column(name = "destination_id")
    private Integer destinationId;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "author")
    private Account author;

    @Column(name = "write_time")
    private LocalDateTime writeTime;

    @Column(name = "image")
    private Boolean image;

    public Message() {
    }

    public Message(Builder builder) {
        this.text = builder.text;
        this.id = builder.id;
        this.destinationEntity = builder.destinationEntity;
        this.destinationId = builder.destinationId;
        this.author = builder.author;
        this.writeTime = builder.writeTime;
        this.image = builder.image;
    }

    public Message(Integer id, DestinationEntity destinationEntity, Integer destinationId,
                   Account author, LocalDateTime writeTime) {
        this.id = id;
        this.destinationEntity = destinationEntity;
        this.destinationId = destinationId;
        this.author = author;
        this.writeTime = writeTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DestinationEntity getDestinationEntity() {
        return destinationEntity;
    }

    public void setDestinationEntity(DestinationEntity destinationEntity) {
        this.destinationEntity = destinationEntity;
    }

    public Integer getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Integer destinationId) {
        this.destinationId = destinationId;
    }

    public LocalDateTime getWriteTime() {
        return writeTime;
    }

    public void setWriteTime(LocalDateTime writeTime) {
        this.writeTime = writeTime;
    }

    public Boolean getImage() {
        return image;
    }

    public void setImage(Boolean image) {
        this.image = image;
    }

    public Boolean hasImage() {
        return image == null ? Boolean.FALSE : image;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    @Override
    public int compareTo(Object o) {
        Message other = (Message) o;
        return other.id - this.id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Message message = (Message) o;
        return Objects.equals(text, message.text)
                && Objects.equals(id, message.id)
                && destinationEntity == message.destinationEntity
                && Objects.equals(destinationId, message.destinationId)
                && Objects.equals(writeTime, message.writeTime)
                && Objects.equals(image, message.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, id, destinationEntity, destinationId, author, writeTime, image);
    }

    @Override
    public String toString() {
        return "Message{"
                + "text='" + text + '\''
                + ", id=" + id
                + ", destinationEntity=" + destinationEntity
                + ", destinationId=" + destinationId
                + ", writeTime=" + writeTime
                + ", image=" + image
                + '}';
    }

    public static class Builder {
        private String text;
        private Integer id;
        private DestinationEntity destinationEntity;
        private Integer destinationId;
        private Account author;
        private LocalDateTime writeTime;
        private Boolean image;

        public Builder text(String text) {
            this.text = text;
            return this;
        }

        public Builder id(Integer id) {
            this.id = id;
            return this;
        }

        public Builder destinationEntity(DestinationEntity destinationEntity) {
            this.destinationEntity = destinationEntity;
            return this;
        }

        public Builder destinationId(Integer destinationId) {
            this.destinationId = destinationId;
            return this;
        }

        public Builder author(Account author) {
            this.author = author;
            return this;
        }

        public Builder writeTime(LocalDateTime writeTime) {
            this.writeTime = writeTime;
            return this;
        }

        public Builder setImage(Boolean image) {
            this.image = image;
            return this;
        }

        public Message build() {
            return new Message(this);
        }
    }
}
