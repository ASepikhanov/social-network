package com.socnet.message;

import java.util.Arrays;
import java.util.Optional;

public enum DestinationEntity {

    ACCOUNT(0),
    GROUP(1),
    PRIVATE_MESSAGE(2);

    private int code;

    DestinationEntity(int code) {
        this.code = code;
    }

    public static DestinationEntity getEntity(int code) {
        Optional<DestinationEntity> entity = Arrays.stream(DestinationEntity.values())
                .filter(e -> e.getCode() == code)
                .findFirst();
        if (entity.isPresent()) {
            return entity.get();
        } else {
            throw new IllegalArgumentException("Invalid code");
        }
    }

    private int getCode() {
        return code;
    }
}
