package com.socnet;

import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "phones")
public class Phone {

    @ManyToOne
    @JoinColumn(name = "id_account", nullable = false, updatable = false, insertable = false)
    private Account account;

    @Id
    @Column(name = "phone", nullable = false)
    private String phoneNum;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "phone_type", nullable = false)
    private PhoneType type;

    public Phone() {
    }

    public Phone(Account account, String phoneNum, int type) {
        this.account = account;
        this.phoneNum = phoneNum;
        this.type = type == 0 ? PhoneType.PERSONAL : PhoneType.WORKING;
    }

    public Phone(Account account, String phoneNum, PhoneType type) {
        this.account = account;
        this.phoneNum = phoneNum;
        this.type = type;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public PhoneType getType() {
        return type;
    }

    public void setType(PhoneType type) {
        this.type = type;
    }

    public void setType(Integer code) {
        this.type = PhoneType.getType(code);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Phone phone = (Phone) o;
        return Objects.equals(account.getId(), phone.account.getId())
                && Objects.equals(phoneNum, phone.phoneNum)
                && type == phone.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, phoneNum, type);
    }

    @Override
    public String toString() {
        return "Phone{"
                + "account=" + account
                + ", phoneNum='" + phoneNum + '\''
                + ", type=" + type
                + '}';
    }
}
